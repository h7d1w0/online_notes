
## RAII classes


### Cust class implementation and cost
| class | constructors | call | constructor called | cost(string calls with malloc without SSO) | remark |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- 
| Cust1 | c1,-c2,-c3,-c4    | c1 | Cust1 c("Joe", "Fix", 42) | 2CR + 2CP |  | 
| Cust1 | c1, c2, c3, c4    | c2 | Cust1 c("Joe", "Fix", 42) | 2CR | extra 2 pointer cp |
| Cust1 | c1, c2, c3, c4    | c3 | std::string s = "Joe"; Cust1 c(s, "Fix", 42) | 1CR + 1CP | |
| Cust1 | c1, c2, c3,-c4    | c3 | std::string s = "Joe"; Cust1 c(std::move(s), "Fix", 42) | 1CR + 1CP | |
| Cust1 | c1,-c2,-c3, c4    | c4 | Cust1 c("Joe", "Fix", 42) | 2CR +     2MV |  | 
| Cust1 | c1,-c2,-c3, c4    | c3 | std::string s = "Joe"; Cust1 c(std::move(s), "Fix", 42) | 1CR +     2MV | |

| class | constructor call | member initialization | cost |
| ----------- | ----------- | ----------- | ----------- | 

```C++
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Cust1 {
    private:
        std::string first;
        std::string last;
        int id;
    public:
        //c1
        Cust1(const std::string &f, const std::string &l = "", int i = 0)
            :first(f), last(l), id(i){
                std::cout << "Cust1 constructor 1 " << std::endl;
        }
        // c2
        // Cust1(const char *f, const char *l = "", int i = 0)
        //     :first(f), last(l), id(i){
        //         std::cout << "Cust1 constructor 2 " << std::endl;
        // }
        //c3
        // Cust1(const std::string &f, const char *l = "", int i = 0)
        //     :first(f), last(l), id(i){
        //         std::cout << "Cust1 constructor 3 " << std::endl;
        // }        
        //c4
        Cust1(const std::string &&f, const std::string &&l = "", int i = 0)
            :first(std::move(f)), last(std::move(l)), id(i){
                std::cout << "Cust1 constructor 4 " << std::endl;
        }
        void print(){
            std::cout << first << " ----- " << last << std::endl;
        }

};


class Cust2 {
    private:
        std::string first;
        std::string last;
        int id;
    public:
        //c1
        Cust2(const std::string f, const std::string l = "", int i = 0)
            :first(std::move(f)), last(std::move(l)), id(i){
                std::cout << "Cust2 constructor 1" << std::endl;
        }
        void print(){
            std::cout << first << " ----- " << last << std::endl;
        }

};

class Cust3 {
    private:
        std::string first;
        std::string last;
        int id;
    public:
        //c1
        template<typename S1, typename S2 = std::string,
                //typename = std::enable_if_t<!std::is_same_v<S1, Cust3>> // type collapsed to Cust3& when being provided with Cust3
                //typename = std::enable_if_t<!std::is_same_v<S1, Cust3&>> // not work with the cases: Cust3 = Cust3(derived object)
                //typename = std::enable_if_t<!std::is_convertible_v<S1, Cust3>> // not work at all, if convertible need constructor to check, no constructor yet
                typename = std::enable_if_t<std::is_convertible_v<S1, std::string>>
        >
        //or requires std::is_convertible_v<S1, std::string> // c++20

        Cust3(S1 &&f, S2 &&l = "", int i = 0)
            :first(std::forward<S1>(f)), last(std::forward<S2>(l)), id(i){
                std::cout << "Cust3 constructor 1" << std::endl;
        }
        void print(){
            std::cout << first << " ----- " << last << std::endl;
        }
};


int main()
{
    Cust3 c("Joe", "Fix", 42);
    c.print();

    std::string s = "Joe";
    Cust3 c1(s, "Fix", 42);

    Cust3 c2(std::move(s), "Fix", 42);

}

```

SSO: Short String Optimation

### resource management class
```C++
#include <iostream>
#include <string>

class MyData {
    private:
    std::string* p;
    public:
    void printVal(){
        std::cout << *p << std::endl;
    }
    void printAddr(){
        std::cout << p << std::endl;
    }

    MyData(): p(new std::string("default")) {
    }

    MyData(const MyData& md){
        std::string* temp = new std::string(*(md.p));
        p = std::exchange(temp, nullptr);
    }

    MyData& operator=(const MyData& md){
        if (this == &md) return *this;
        MyData temp(md);

        // Implementation - begin
        // This implementation has memory leak problem, since the memory originally 
        // pointed by p is not released
        // p = std::exchange(temp.p, nullptr);
        // Implementation - end
        temp.swap(*this);
        return *this;
    }

    MyData(MyData&& md) noexcept{
        p = std::exchange(md.p, nullptr);
    }

    MyData& operator=(MyData&& md){
        // Implementation - begin
        // Is this implementation OK?
        // md.swap(*this);
        // Implementation - end
        MyData temp(std::move(md));
        temp.swap(*this);
        return *this;
    }

    void swap(MyData& md){
        std::swap(p, md.p);
    }

    friend void swap(MyData& md1, MyData& md2) noexcept {
        md1.swap(md2);
    }

    ~MyData(){
        delete p;
    }

};

MyData getVal(){
    MyData md;
    std::cout << "inside getVal()" << std::endl;
    md.printAddr();
    std::cout << "inside getVal() end" << std::endl;
    return md;
}


int main()
{
    MyData md;
    // MyData
    md = getVal();
    std::cout <<  "From main---------: "  << std::endl;
    // md.printVal();
    md.printAddr();
}

```


## algorithm
### split string by delimiter
```C++
string delimiter = ">=";
auto delSize = delimiter.length();
string s("01234>=567>=89");
auto wordStart = s.cbegin();

for (auto i = s.cbegin(); i != s.cend(); ++i){
    if(equal(delimiter.cbegin(), delimiter.cend(), i)){
        for(auto j = wordStart; j < wordEnd; ++j){
            cout << *j;
        }
        cout << endl;
        if(i + delSize < s.cend())
            wordStart = wordEnd = i + delSize;
        else break;
    } else {
        wordEnd = i;
    }
}
```


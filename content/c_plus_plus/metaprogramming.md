


## Unevaluated operands
sizeof, typeid, decltype, noexcept


## Partial Specializations
Class-Template Partial Specializations
Partial Specializations are not for function templates
> We can partially specialize only a class template. We cannot partially specialize a function template.
Because a partial specialization is a template, we start, as usual, by defining the
template parameters. Like any other specialization, a partial specialization has the
same name as the template it specializes. The specialization’s template parameter list
includes an entry for each template parameter whose type is not completely fixed by
this partial specialization. After the class name, we specify arguments for the template
parameters we are specializing. These arguments are listed inside angle brackets
following the template name. The arguments correspond positionally to the parameters
in the original template.

## 
An invalid simple-template-id is a compile-time error, unless it names a function template specialization (in which case SFINAE may apply) cpp_reference
It is not possible to partially or explicitly specialize an alias template. cpp_reference
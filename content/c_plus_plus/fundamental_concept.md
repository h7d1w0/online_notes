---
title: "C++ fundamental concepts"
date: 2021-12-29T10:05:05-05:00
draft: false

categories: ["programming"]
tags: ["C++", "fundamental", "concept", "basic"]
---

Notes on the C++ fundamental concepts and basic terminology.
<!--more-->

# Fundamental concept
- Built-in types such as integers, characters, and so forth
- Variables, which let us give names to the objects we use
- Expressions and statements to manipulate values of these types
- Control structures, such as if or while, that allow us to conditionally or repeatedly execute a set of actions
- Functions that let us define callable units of computation
- Libraries that provide extended types and operations
- Funtionality that users can define their own types(classes) and operations


# Terminology sorted by name
The first block quote is from the book C++ Primer (Fifth Edtion) by Stanley B. Lippman Josée Lajoie Barbara E. Moo.

## &&
### for types: && declares raw rvalue references
### for template parameters: && declares universal/forwarding references
based on Basics: Move Semantics - Nicolai Josuttis - CppCon 2021, this is the mistake that && was selected as the syntax.

## array
std::array: a fixed size stack based container.  Having the size part of the type information gives more optimization opportunity.  Since C++11, based on boost:array, by Nocolai Jusottis.

## auto
let the compiler figure out the type

## class
> Facility for defining our own data structures together with associated operations. A group of data and their related operations

## class type
> A type defined by a class. The name of the type is the class name.

## constexpr function 
is a function that can be used in a constant expression.  A constexpr function is defined like any other function but must meet certain restrictions: The return type and the type of each parameter in a must be a literal type, and the function body must contain exactly one return statement.  constexpr functions are implicitly inline.

## decltype
returns the type of its operand. The compiler analyzes the expression to determine its type but does not evaluate the expression

## explicit
Suppressing Implicit Conversions Defined by Constructors.  Explicit Constructors Can Be Used Only for Direct Initialization.  A static_cast to perform an explicit, rather than an implicit, conversion.

## expression 
> The smallest unit of computation. An expression consists of one or more operands and usually one or more operators. Expressions are evaluated to produce a result.

## reference
>  Once we have defined a reference, there is no way to make that reference refer to a different object.

## include 
> Directive that makes code in a header available to a program.

Preprocessor replaces the include directive with the code of the included file.

## inline Functions
A function specified as inline (usually) is expanded “in line” at each call.  A inline function call (probably) would be expanded during compilation, which removes the run-time overhead of making a function call.  In general, the inline mechanism is meant to optimize small, straight-line functions that are called frequently. Many compilers will not inline a recursive function.

## internal linkage
To-do

## initialize / initialization
> Give an object a value at the same time that it is created.

## main 
> Function called by the operating system to execute a C++ program. Each program must have one and only one function named main.

main function must be in the top level global scope

## namespace 
> Mechanism for putting names defined by a library into a single place. Namespaces help avoid inadvertent name clashes. The names defined by the C++ library are in the namespace std.

A mechanism to put names together into a named group to avoid name collision.

## smart pointer 
Pointer types that manage dynamic objects.  A smart pointer acts like a regular pointer with the important exception that it automatically deletes the object to which it points.  shared_ptr, unique_ptr, and weak_ptr are defined in the memory header.  Objects that use smart pointers can rely on the default definitions for the members that copy, assign, and destroy class objects.

## using declaration
using declaration lets us use a name from a namespace without qualifying the name with a namespace_name:: prefix. A using declaration has the form using namespace::name

Headers Should Not Include using Declarations

## value initialization 
> Initialization in which built-in types are initialized to zero and class types are initialized by the class’s default constructor. Objects of a class type can be value initialized only if the class has a default constructor. Used to initialize a container’s elements when a size, but not an element initializer, is specified.  Elements are initialized as a copy of this compiler-generated value.

## variable
> A named object.
> A variable provides us with named storage that our programs can manipulate. Each variable in C++ has a type.



# C++ language feature details
## aggregate class
A class is an aggregate if
- All of its data members are public
- It does not define any constructors
- It has no in-class initializers
- It has no base classes or virtual functions
- initialize the data members of an aggregate class by providing a braced list of member initializers:
    ```C++
    // val1.ival = 0; val1.s = string("Anna")
    Data val1 = { 0, "Anna" };
    ```

## call operator
> We execute a function through the call operator, which is a pair of parentheses. The call operator takes an expression that is a function or points to a function.

## class
The fundamental ideas behind classes are data abstraction and encapsulation.
- encapsulation:  Separation of implementation from interface; encapsulation hidesthe implementation details of a type. In C++,encapsulation is enforced by putting the implementation in the private part of a class.
- Data abstraction: is a programming (and design) technique that relies on the separation of interface and implementation. Programming technique that focuses on the interface to a type. Data abstraction lets programmers ignore the details of how a type is represented and think instead about the operations that the type can perform.
- Both data abstraction and encapsulation are to separate the interface from the implementation.  Data abstraction is more from the perspective of the consuming of the classes while encapsulation is more on designing and creating the classes.

- auxiliary functions: such functions define operations that are conceptually part of the interface of the class, they are not part of the class itself.
- constructor: is to initialize the data members of a class object. A constructor is run whenever an object of a class type is created.
- constructors may not be declared as const. When we create a const object of a class type, the object does not assume its “constness” until after the constructor completes the object’s initialization. Thus, constructors can write to const objects during their construction.
- default constructor, The default constructor is one that takes no arguments.
    - synthesized default constructor, if our class does not explicitly define any constructors, the compiler will implicitly define the default constructor.  For most classes, this synthesized constructor initializes each data member of the class as follows:
        - If there is an in-class initializer (§ 2.6.1, p. 73), use it to initialize the member.
        - Otherwise, default-initialize (§ 2.2.1, p. 43) the member.
    - objects of builtin or compound type (such as arrays and pointers) that are defined inside a block have undefined value when they are default initialized.  Class members are defined inside a block(class block).  If classes that have members of built-in or compound type do not initialize those members inside the class (don't have in-class initializers) then we need define their own version of the default constructor.  Otherwise, users could create objects with members that have undefined value.
    - sometimes the compiler is unable to synthesize one.
    - = default: ask the compiler to generate the constructor.
    - constructor initializer list: When a member is omitted from the constructor initializer list, it is implicitly initialized using the same process as is used by the synthesized default constructor.
    - It is usually best for a constructor to use an in-class initializer if one exists and gives the member the correct value.
    - Even though the constructor initializer list is empty, the members of this object are still initialized before the constructor body is executed.
    - Members that do not appear in the constructor initializer list are initialized by the corresponding in-class initializer (if there is one) or are default initialized.

- Copy, Assignment, and Destruction
    - Copy: Objects are copied in several contexts, such as when we initialize a variable or when we pass or return an object by value.
    - Objects are assigned when we use the assignment operator. 
    - Objects are destroyed when they cease to exist
- If we do not define these operations, the compiler will synthesize them for us.
- unlike ordinary members, members that define types must appear before they are used. As a result, type members usually appear at the beginning of the class.
- mutable Data Members: A mutable data member is never const, even when it is a member of a const object. Accordingly, a const member function may change a mutable member.

- Members that are const, references or of a class type that does not have a default constructor must be initialized.
- Members are initialized in the order in which they appear in the class definition.  The order in which initializers appear in the constructor initializer list does not change the order of initialization.

- Initializers for Data Members of Class Type
    - in-class initializers must use either the = form of initialization or the direct form of initialization using curly braces.
    - When we provide an in-class initializer, we must do so following an = sign or inside braces.
- A const member function that returns *this as a reference should have a return type that is a reference to const.

- The nonconst version will not be viable for const objects; we can only call const member functions on a const object. We can call either version on a nonconst object, but the nonconst version will be a better match.
    - const argument works only for const member functions
    - non-const argument works for both const member functions and non-const member functions

- forward declaration
After a forward declaration and before a definition is seen, the type Screen is an incomplete type—it’s known that Screen is a class type but not known what members that type contains.  We can define pointers or references to such types, and we can declare (but not define) functions that use an incomplete type as a parameter or return type.
- data members can be specified to be of a class type only if the class has been defined.  Because a class is not defined until its class body is complete, a class cannot have data members of its own type. However, a class is considered declared (but not yet defined) as soon as its class name has been seen. Therefore, a class can have data members that are pointers or references to its own type.

- member declaration
    - extern and register storage class specifiers are not allowed
    - thread_local storage class specifier is not allowed (but it is allowed for static data members)
    - incomplete types are not allowed: in particular, a class C cannot have a non-static data member of class C, although it can have a non-static data member of type C& (reference to C) or C* (pointer to C)
    - a non-static data member cannot have the same name as the name of the class if at least one user-declared constructor is present.

## Class Scope and Name Lookup
- Class definitions are processed in two phases:
    - First, the member declarations are compiled.
    - Function bodies are compiled only after the entire class has been seen.  Member function definitions are processed after the compiler processes all of the declarations in the class.
- in a class, if a member uses a name from an outer scope and that name is a type, then the class may not subsequently redefine that name.
    > Although it is an error to redefine a type name, compilers are not required to diagnose this error. Some compilers will quietly accept such code, even though the program is in error.

## Class static members


### static Data Members

> Even if a const static data member is initialized in the class body, that member ordinarily should be defined outside the class definition.

###
A class is typically declared in a header file and a header file is typically included into many translation units. However, to avoid complicated linker rules, C++ requires that every object has a unique definition. That rule would be broken if C++ allowed in-class definition of entities that needed to be stored in memory as objects.

> Note that only static const integers can be treated as compile time constants. The compiler knows that the integer value will not change anytime and hence it can apply its own magic and apply optimizations, the compiler simply inlines such class members i.e, they are not stored in memory anymore, As the need of being stored in memory is removed, it gives such variables the exception to rule mentioned by Bjarne.
It is noteworthy to note here that even if static const integral values can have In-Class Initialization, taking address of such variables is not allowed. One can take the address of a static member if (and only if) it has an out-of-class definition.This further validates the reasoning above.

- If a static data member is of const literal type, its declaration in the class definition can specify a brace-or-equal-initializer in which every initializer-clause that is an assignment-expression is a constant expression. C++11
- A static data member of literal type can be declared in the class definition with the constexpr specifier; if so, its declaration shall specify a brace-or-equal-initializer in which every initializer-clause that is an assignment-expression is a constant expression. [ Note: In both these cases, the member may appear in constant expressions. —end note ] The member shall still be defined in a namespace scope if it is used in the program and the namespace scope definition shall not contain an initializer. C++11
- Also, C++11 will allow(§12.6.2.8) a non-static data member to be initialized where it is declared(in its class). This will mean much easy user semantics.

## CRTP
curiously recurring template pattern CRTP.  We have to teach the base class about the name of the derived class, because the name of the derived class appears in the signature of some member functions of the base class.  The way to do that is to make the base class a template, a template on the name of the derived class.


## dangling pointer. 
A dangling pointer is one that refers to memory that once held an object but no longer does so.  A pointer after delete operation becomes a dangling pointer.

## decltype
- returns the type of its operand. The compiler analyzes the expression to determine its type but does not evaluate the expression
- When the expression to which we apply decltype is a variable, decltype returns the type of that variable, including top-level const and references:
- the dereference operator is an example of an expression for which decltype returns a reference
- When we apply decltype to a variable without any parentheses, we get the type of that variable. If we wrap the variable’s name in one or more sets of parentheses, the compiler will evaluate the operand as an expression. A variable is an expression that can be the left-hand side of an assignment. As a result, decltype on such an expression yields a reference

## friend
Classes and nonmember functions need not have been declared before they are used in a friend declaration. When a name first appears in a friend declaration, that name is implicitly assumed to be part of the surrounding scope. However, the friend itself is not actually declared in that scope. It is important to understand that a friend declaration affects access but is not a declaration in an ordinary sense.

## function
> A function is a block of code with a name.

## initialize / initialization
- Uninitialized variable Variable that is not given an initial value.  Uninitialized variables are a rich source of bugs.
- Variables of class type for which no initial value is specified are initialized as specified by the class definition. 
- Variables of built-in type defined inside a function are uninitialized unless explicitly initialized.
- Initialization is not assignment. Initialization happens when a variable is given a value when it is created. Assignment obliterates an object’s current value and replaces that value with a new one.
- we can supply an in-class initializer for a data member
- Members without an initializer are default initialized
- In-class initializers must either be enclosed inside curly braces or follow an = sign. We may not specify an in-class initializer inside parentheses.


### Five Forms of initializaion
#### List initialization
- If there is an initializer-list constructor for the class type obj, it will always be preferred over other other constructors for brace-init-initializers (list initialization), in your case obj s{"value"};  What this means is that if you have a constructor that takes std::initializer_list<T> as its first parameter and other parameters are defaulted, then it is preferred.  **std::vector<T> and other STL containers have such initializer-list constructors.**
- Otherwise, Overload resolution kicks in and it falls back to any available constructor as selected by the overload resolution process;
- Otherwise, if the class has no user defined constructors and it's an aggregate type, it initializes the class members directly.


- The compiler will not let us list initialize variables of built-in type if the initializer might lead to the loss of information.
    ```C++
    int units_sold = {0};
    int units_sold{0};
    long double ld = 3.1415926536;
    int a{ld}, b = {ld}; // error: narrowing conversion required
    ```

#### Direct initialization
```C++
int units_sold = 0; 
int units_sold(0);
long double ld = 3.1415926536;
int c(ld), d = ld; // ok: but value will be truncated
```

#### Copy initialization

#### Value initialization

#### Aggregate Initialization (only for aggregate types)

### Default initializaion
When defining a variable without an initializer:
| Teype | Location | Default Initialized | Default Value |
| ----------- | ----------- | ----------- | ----------- |
| Built-in | Outside Function | Yes | Zero
| Built-in | In Function | Uninitialized | Undefined| 

Each class controls how we initialize objects of that class type. In particular, it is up to the class whether we can define objects of that type without an initializer.

### initialization form
- default initialization(initialized by default constructor)
    ```C++
    string *ps = new string; // initialized to empty string
    int *pi = new int; // pi points to an uninitialized int
    ```
- direct initialization
    - traditional construction (using parentheses)
    - under the C++11 standard, use list initialization (with curly braces)
    ```C++
    int *pi = new int(1024); // object to which pi points has value 1024
    string *ps = new string(10, '9'); // *ps is "9999999999"
    // vector with ten elements with values from 0 to 9
    vector<int> *pv = new vector<int>{0,1,2,3,4,5,6,7,8,9};
    ```
- value initialize (following the type name with a pair of empty parentheses)
    ```C++
    string *ps = new string(); // value initialized to the empty string
    int *pi2 = new int(); // value initialized to 0; *pi2 is 0
    ```
- mixed examples
```C++
string *ps1 = new string; // default initialized to the empty string
string *ps = new string(); // value initialized to the empty string
string *ps2 = new string(10, '9'); // direct initialization, *ps is "9999999999"
string *ps3 = new string{"abc"}; // direct initialization, *ps is "9999999999"

int *pi1 = new int; // default initialized; *pi1 is undefined
int *pi2 = new int(); // value initialized to 0; *pi2 is 0
int *pi3 = new int(1024); // direct initialization, object to which pi points has value 1024
int *pi4 = new int{1024}; // direct initialization, object to which pi points has value 1024
```


## Cast
- does non-const is casted to const for function parameter

## Class Control
### copy control
- a copy constructor is a constructor if its first parameter is a reference to the class type and any additional parameters have default values. 
- we can define the copy constructor to take a reference to nonconst, but most of time it is const.
- The copy constructor is used implicitly in several circumstances. Hence, the copy constructor usually should not be explicit.
- a copy constructor is synthesized even if we define other constructors, but not define a copy constructor.
> Members of class type are copied by the copy constructor for that class; members of built-in type are
copied directly. Although we cannot directly copy an array, the synthesized copy constructor copies members of array type by copying each element.  Elements of class type are copied by using the elements’ copy constructor.
- copy initialization usage
    1. Pass an object as an argument to a parameter of nonreference type
    2. Return an object from a function that has a nonreference return type
    3. Brace initialize the elements in an array or the members of an aggregate class
    4. Some class types also use copy initialization for the objects they allocate. For example, the library containers copy initialize their elements when we initialize the container.

### copy assignment control
- the assignment operator is a function named operator=
- must be defined as member functions
- The copy-assignment operator takes an argument of the same type as the class
- To be consistent with assignment for the built-in types, assignment operators usually return a reference to their left-hand operand.
- generally required by the class that will be put into a container as an element, since the library generally requires that types stored in a container have assignment operators that return a reference to the left-hand operand.

### move
- The compiler synthesizes the move constructor and move assignment only if a class does not define any of its own copy-control(copy, copy-assignment, destruct) members and only if all the data members can be moved constructed and move assigned, respectively.
- Move semantics can only be meaningfully implemented with dynamically allocated memory or unique resources.

### Destructor
- destructors do whatever work is needed to free the resources used by an object and destroy the nonstatic data members of the object.
- destructor takes no parameters, it cannot be overloaded
- the function body is executed first and then the members are destroyed. Members are destroyed in reverse order from the order in which they were initialized.
- Members of class type are destroyed by running the member’s own destructor. The built-in types do not have
destructors, so nothing is done to destroy members of built-in type.
    > The implicit destruction of a member of built-in pointer type does not delete the object to which that pointer points.
- smart pointers (§ 12.1.1, p. 452) are class types and have destructors. As a result, unlike ordinary pointers, members that are smart pointers are automatically destroyed during the destruction phase
- The destructor of an object allocated by new does not run when a reference or a pointer to an object goes out of scope.  Need run delete explicitly to claim the resources.

### The Rule of Three/Five
- Classes That Need Destructors Need Copy and Assignment.  Needing either the copy constructor or the copy-assignment operator does not (necessarily) indicate the need for a destructor.
- Classes That Need Copy Need Assignment, and Vice Versa
- Ordinarily, classes that manage resources that do not reside in the class must define the copy-control members.

## Concurrency and Parallalism
https://www.youtube.com/watch?v=F6Ipn7gCOsY&ab_channel=CppCon
### mutex
### unique_lock(c++11)
### lock_guard(c++11)
### once_flag and call_once (c++11)
### condition_variable (c++11)
### static local variable initialization is thread safe automatically (Threadsafe static initialization) (c++11)
### scope_lock(c++17) rarely used
### shared_mutex (c++17) reader/writer lock, many readers and one writer
### shared_lock (c++17) reader/writer lock, many readers and one writer
### counting_semaphore (c++20) bag of poker chips
### latch (c++20)
### barrier<> (c++20)

## Const
- By Default, const Objects Are Local to a File
- To define a single instance of a const variable cross multiple files, we use the keyword extern on both its definition and declaration
-  a reference to const object must be defined as a const reference
- a reference to const can be initialized from any expression that can be converted (§ 2.1.2, p. 35) to the type of the reference. In particular, we can bind a reference to const to a nonconst object, a literal, or a more general expression
```C++
double dval = 3.14; 
const int &ri = dval;
const int temp = dval; // create a temporary const int from the double const int &ri = temp; // bind ri to that temporary
```
- pointer to const and reference to const as pointers or references "that think they point or refer to const."
- top-level const to indicate that the pointer itself is a const
- low-level const to indicate that the pointer can point to a const object
- there is no low-level const reference

## Constant Expressions and constexpr
> A constant expression is an expression whose value cannot change and that can be evaluated at compile time.

> Whether a given object (or expression) is a constant expression depends on the types and the initializers.

- Variables declared as constexpr are implicitly const and must be initialized by constant expressions
- ask the compiler to verify that a variable is a constant expression by declaring the variable in a constexpr declaration

## Declaration vs. Initialization
- Declarations introduce (or re-introduce) names into the C++ program. Each kind of entity is declared differently. Definitions are declarations that are sufficient to use the entity identified by the name.
- A declaration makes a name known to the program. A file that wants to use a name defined elsewhere includes a declaration for that name.
- Variables must be defined exactly once but can be declared many times.
- A definition creates the associated entity.
- A variable declaration specifies the type and name of a variable.
- A variable definition is a declaration.  A definition also allocates storage and may provide the variable with an initial value.
- To obtain a declaration that is not also a definition, add the extern keyword and may not provide an explicit initializer
- extern that has an initializer is a definition, and initializer on a variable defined as extern overrides the extern
- It is an error to provide an initializer on an extern inside a function.
- A function declaration is just like a function definition except that a declaration has no function body.
- can't initialize a plain reference from a literal

    ```C++
    extern int i; // declares but does not define i 
    int j; // declares and defines j
    extern double pi = 3.1416; // definition
    ```

> Separate compilation: separate compilation lets us split our programs into several files, each of which can be compiled independently.  **To support separate compilation, C++ distinguishes between declarations and definitions.**

The declaration makes it possible for the compiler to compile a file seprately.  Later at the linking stage, the linker must find exact one definition for the declared entity from all the separately compiled object files.

### Class Declaration and Defination
- For classes used in only one file that aren’t generally reusable, define them directly in the single .cpp file they’re used in.
- For classes used in multiple files, or intended for general reuse, define them in a .h file that has the same name as the class.
- Trivial member functions (trivial constructors or destructors, access functions, etc…) can be defined inside the class.
- Non-trivial member functions should be defined in a .cpp file that has the same name as the class.
- Using the extern specifier with type declarations is illegal. An extern declaration cannot appear in class scope.


## Containers
- the library generally requires that types stored in a container have assignment operators that return a reference to the left-hand operand.
### Sequential Containers

 - Performance
| Type | Add/Remove in the Middle | Add/Remove at Front | Add/Remove at End | Random Access | Memroy Overhead | Size Operation |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| Vector | Slow | Slow | Fast | Fast | Small | Fast, O(1) |
| string | Slow | Slow | Fast | Fast | Small | Fast, O(1) |
| deque | Slow | Fast | Fast | Fast | ? | Fast, O(1) |
| list | Fast | Fast | Fast | Slow | Big | Fast, O(1) |
| forward_list | Fast | Fast | Fast | Slow | Big | N/A |
| array | N/A | N/A | N/A | Fast | Small | Fast, O(1) |

- If the program needs to insert elements in the middle of the container only while reading input, and subsequently needs random access to the elements:
    * First, decide whether you actually need to add elements in the middle of a container. It is often easier to append to a vector and then call the library sort function to reorder the container when you’re done with input.
    * If you must insert into the middle, consider using a list for the input phase. Once the input is complete, copy the list into a vector.
> If you’re not sure which container to use, write your code so that it uses only operations common to both vectors and lists: Use iterators, not subscripts, and avoid random access to elements. That way it will be easy to use either a vector or a list as necessary.

- Defining and Initializing a Container
- When we initialize a container as a copy of another container, the container type and element type of both containers must be identical.

- Operations

| Type | push_back /pop_back | push_front /pop_front | insert | Random Access | Memroy Overhead | Size Operation |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| Vector | Y | N | Y | Fast | Small | Fast, O(1) |
| string | Y | N | Y | Fast | Small | Fast, O(1) |
| deque | Y | Y | Y | Fast | ? | Fast, O(1) |
| list | Y | Y | Y | Slow | Big | Fast, O(1) |
| forward_list | N | Y | N | Slow | Big | N/A |
| array | N | N | N/A | N | Small | Fast, O(1) |

- The emplace functions construct elements in the container. The arguments to
these functions must match a constructor for the element type.

## dynamic array
It is important to remember that what we call a dynamic array does not have an array type.  we cannot call begin or end on a dynamic array.
- When a unique_prt that points to an dynamic array destroys the pointer it manages, it will automatically use delete[].
- shared_ptrs provide no direct support for managing a dynamic array. If we want to use a shared_ptr to manage a dynamic array, we must provide our own deleter: [](int *p) { delete[] p;}.
- There is no subscript operator for shared_ptrs, and the smart pointer types do not support pointer arithmetic. As a result, to access the elements in the array, we must use get to obtain a built-in pointer, which we can then use in normal ways.

## EBCO / ECO / Empty Base Class Optimization
stateless class
[[no_unique_address]]
- only non-static data declaration in class definitions add something to the size of the class objects
- things that don't affect the class objects's layout or size
    - static data members
    - type members including nested classes
    - non-virtual member functiosn
- putting the empty base class first on the derived class list helps optimization
- standard layout types, since C++11
- static_assert, compile time check, useful for hardware related programming. alignas(4)

## Entity
- Entity: The entities of a C++ program are values, objects, references, structured bindings (since C++17), functions, enumerators, types, class members, templates, template specializations, namespaces, and parameter packs. Preprocessor macros are not C++ entities.
These are things we create.  These are things that in some direct way correspond to something executable images that is either data or executables.
- Name: is the use of an identifier(several forms are defined) that denotes an entity(label).  Every name that denotes an entity is introduced by a declaration.  
- Declaration: An declaration introduces(re-introduces) one or more names into a translation unit.
- Definition: a definition is a declaration that fully defines the entity being introduced.
- Variable: a variable is an entity introduced by the declaration of an object (or of a reference other than a non-static data member).




## Expression
- Every expression in C++ is either an rvalue (pronounced “are-value”) or an lvalue (pronounced “ell-value”).
- In C++, an lvalue expression yields an object or a function, but some lvalue may not the lefthand operand of an assignment (such as const objects).  Moreover, some expressions yield objects but return them as rvalues, not lvalues.
- When we use an object as an rvalue, we use the object’s value (its contents). When we use an object as an lvalue, we use the object’s identity (its location in memory).
    - Assignment requires a (nonconst) lvalue as its left-hand operand and yields its left-hand operand as an lvalue.
    - The address-of operator requires an lvalue operand and returns a pointer to its operand as an rvalue.
    - The built-in dereference and subscript operators and the iterator dereference and string and vector subscript operators all yield lvalues.
    - The built-in and iterator increment and decrement operators require lvalue operands and the prefix versions (which are the ones we have used so far) also yield lvalues.
    - When we apply decltype to an expression (other than a variable), the result is a referencetype if the expression yields an lvalue. As an example, assume p is an int*. Because dereference yields an lvalue, decltype(*p) is int&.
    - Assignment is an expression operator.  Assignment is right associative and assignment returns its left-hand operand.

> An expression with two or more operators is a compound expression. Evaluating a compound expression involves grouping the operands to the operators. Precedence and associativity determine how the operands are grouped. That is, they determine which parts of the expression are the operands for each of the operators in the expression. Programmers can override these rules by parenthesizing compound expressions to force a particular grouping.

> Precedence specifies how the operands are grouped. It says nothing about the order in which the operands are evaluated. In most cases, the order is largely unspecified.

## Function
> A function call does two things: It initializes the function’s parameters from the corresponding arguments, and it transfers control to that function. Execution of the calling function is suspended and execution of the called function begins.

> The value returned by the function is used to initialize the result of the call expression.

> can't create overloaded functions that use top-level consts / non consts as the difference

> it is a bad idea to declare a function locally.

> A function’s type is determined by its return type and the types of its parameters. The function’s name is not part of its type.

- function return
    - return type is a value: Values are returned in exactly the same way as variables and parameters are initialized: The return value is used to initialize a temporary at the call site, and that temporary is the result of the function call.
    - return type is built-in reference or pointer: Never Return a Reference or Pointer to a Local Object
    - Because we cannot copy an array, a function cannot return an array. However, a function can return a pointer or a reference to an array

- overloading
    - A parameter that has a top-level const is indistinguishable from one without a top-level const.  top-level consts in either the parameter or the argument are ignored.
    - if the parameter is non-reference (or non-pointer) type, overloading based on const-ness does not make sence - since the argument is passed by value, the function can't modify the argument any way, which means the const-ness makes no difference to the argument.  And also the parameter will be out of scope and becomes nonexist when the function ends.
    - we can overload based on whether the parameter is a reference (or pointer) to the const or nonconst version of a given type; such consts are low-level
    - there is no top-level const reference


- Default arguments
    - Default arguments ordinarily should be specified with the function declaration in an appropriate header.
    - Local variables may not be used as a default argument.
    - Although it is normal practice to declare a function once inside a header, it is legal to redeclare a function multiple times However, each parameter can have its default specified only once in a given scope. Thus, any subsequent declaration can add a default only for a parameter that has not previously had a default specified. As usual, defaults can be specified only if all parameters to the right already have defaults.


## Function matching (overload resolution)
- overloading has no special properties with respect to scope: As usual, if we declare a name in an inner scope, that name hides uses of that name declared in an outer scope. Names do not overload across scopes. Function names and viable names are all names, which hide each other.  When saying function names, we say only names without return type and parameter list.
- In C++, name lookup happens before type checking.

- function matching steps
    1. Determining the Candidate Functions
        - A candidate function is a function with the same name as the called function and for which a declaration is visible at the point of the call
        - the matching in this step is by name only
    2. Determining the Viable Functions
        - candidate functions those functions that can be called with the arguments in the given call
        - To be viable, a function must have the same number of parameters as there are arguments in the call, and the type of each argument must match—or be convertible to—the type of its corresponding parameter.
        - When a function has default arguments (§ 6.5.1, p. 236), a call may appear to have fewer arguments than it actually does.
    3. Choose the best match viable function
        - An exact match is better than a match that requires a conversion.
        - The match for each argument is no worse than the match required by any other viable function
        - There is at least one argument for which the match is better than the match provided by any other viable function
    > Casts should not be needed to call an overloaded function. The need for a cast suggests that the parameter sets are designed poorly.

- We cannot bind a plain reference(parameter) to a const object(argument)).

## Function Pointers
- Just as with arrays, we cannot define parameters of function type but can have a parameter that is a pointer to function. As with arrays, we can write a parameter that looks like a function type, but it will be treated as a pointer.
- A function can't return a function type, but it can return a function pointer.

## Header File
> In C++, the contents of a module consist of structure type (struct) declarations, class declarations, global variables, and functions. The functions themselves are normally defined in a source file (a “.cpp” file). Each source (.cpp) file has a header file (a “.h” file) associated with it that **provides the declarations needed by other modules to make use of this module**. The idea is that other modules can access the functionality in module X simply by #including the “X.h” header file, and the linker will do the rest. The code in X.cpp needs to be **compiled only the first time or if it is changed**; the rest of the time, the linker will link X’s code into the final executable without needing to recompile it, which enables the Unix make utility and IDEs to work very efficiently. Usually, the main module does not have a header file, since it normally uses functionality in the other modules, rather than providing functionality to them.
C++ Header File Guidelines
David Kieras, EECS Dept., University of Michigan
Revised for EECS 381, April 2015

> The trick is reduce the amount of “coupling” between components by minimizing the number of header files that a
module’s header file itself #includes. On very large projects (where C++ is often used), minimizing coupling can make a huge difference in “build time” as well as simplifying the code organization and debugging. 

guidelines to set up your header and source files for the greatest clarity and compilation convenience:
1. Each module with its .h and .cpp file should correspond to a clear piece of functionality
2. Always use “include guards” in a header file.
3. All of the declarations needed to use a module must appear in its header file, and this file is always used to access the module.  The header file contains the public interface for the module.
4. No namespace using statements are allowed at the top level in a header file.  Usually you should be using fully qualified names in a header file, such as “std::ostream.
5. The header file contains only declarations, templates, and inline function definitions, and is included by the .cpp file for the module.  
    - Put structure and class declarations, function prototypes, and global variable extern declarations.
    - for templates, unless you are using explicit instantiations (rare), the compiler must have the full definitions available in order to instantiate the template.
    - full definitions available for ordinary (nonmember) functions that need to be inlined
6. Set up global variables for a module with an extern declaration in the header file, and a defining declaration in the .cpp file.
7. Keep a module’s internal declarations out of the header file.
8. Put declarations in the narrowest scope possible in the header file.
9. Every header file A.h should #include every other header file that A.h requires to compile correctly, but no more.
10. If an incomplete declaration of a type X will do, use it instead of #including its header X.h.
    > . If another struct or class type X appears only as a pointer or reference type in the contents of a header file, or as a parameter type or returned value in a function declaration, then you should not #include X.h, but just place an incomplete declaration of X (also called a "forward" declaration) near the beginning of the header file, as in:
    class X;
    See the handout Incomplete Declarations for more discussion of this powerful and valuable technique.
11. The content of a header file should compile correctly by itself.
12. The A.cpp file should first #include its A.h file, and then any other headers required for its code.
13. Explicitly #include the headers for all Standard Library facilities used in a .cpp file.
14. Never #include a .cpp file for any reason!


## shared_ptr
- The management of the control block including the reference count is thread safe, but the access to the control resource is not.
- use atomic operations for shared_ptr in C++11
- atomic_shared_ptr and atomic_weak_ptr in C++20

## inheritance
- It can be thought that the scope of a derived class is nested inside the scope of its base class.  And the derived class has both base verion functions and overriden version of functions from the derived class.  The overriden virtual functions are called based on the dynamic type, while the verriden non-virtual functions are called by static type.
    > Aside from overriding inherited virtual functions, a derived class usually should not reuse names defined in its base class.

    > The base class is initialized first, and then the members of the derived class are initialized in the order in which they are declared in the class.  The initialization process on the object of a inheritance tree works like stack - the most derived object calls on its direct base and then this direct base calls on its direct base until the top base, then the top base initializes its members and runs its constructor body.  Upon the completion, the top base's direct derived class starts its initialization and constructor body...

- calls to nonvirtual functions are bound at compile time.
- Virtuals are resolved at run time only if the call is made through a reference or pointer. Only in these cases is it possible for an object’s dynamic type to differ from its static type.  
- Overrides for statics would be substantially chaotic, should not be used.
- final to forbid inheritance for a function or for a class
- Virtual functions that have default arguments should use the same argument values in the base and derived classes.  Changes on the overriden function in derived class have no effect, the function still uses the default value from the base class.
- No Conversion between Objects
    > The automatic derived-to-base conversion applies only for conversions to a reference or pointer type. There is no such conversion from a derived-class type to the baseclass type. Nevertheless, it is often possible to convert an object of a derived class to its base-class type. However, such conversions may not behave as we might want.  When we initialize, we’re calling a constructor; when we assign, we’re calling an assignment operator. These members normally have a parameter that is a reference to the const version of the class type.  Because these members take references, the derived-to-base conversion lets us pass a derived object to a base-class copy/move operation. These operations are not virtual. When we pass a derived object to a base-class constructor, the constructor that is run is defined in the base class. That constructor knows only about the members of the base class itself. Similarly, if we assign a derived object to a base object, the assignment operator that is run is the one defined in the base class. That operator also knows only about the members of the base class itself.
- Derived object can be assigned to base object, only the base part of the derived object are assigned to the base object.
- pure virtual function: a function that has no body, but "have = 0;" instead.
- Classes with Pure Virtuals Are Abstract Base Classes.
- The derivation access specifier has no effect on whether members (and friends) of a derived class may access the members of its own direct base class. Access to the members of a base class is controlled by the access specifiers in the base class itself.
- The purpose of the derivation access specifier is to control the access that users of the derived class—including other classes derived from the derived class—have to the members inherited from Base.
- The derivation access specifier used by a derived class also controls access from classes that inherit from that derived class.
- Just as friendship is not transitive, friendship is also not inherited. Friends of the base have no special access to members of its derived classes, and friends of a derived class have no special access to the base class.
- Name Lookup Happens at Compile Time, and the static type determines which names are visible and then can be used.  As Usual, Name Lookup Happens before Type Checking.  Functions declared in an inner scope do not overload functions declared in an outer scope. As a result, functions defined in a derived
class do not overload members defined in its base class(es).  If a base class function is hidden by a derived class function.  The base class function cannot be called through the derived class object.
- If a derived class wants to make all the overloaded versions available through its type, then it must override all of them or none of them.  Or use "using function_name" to bring all the base class functions in, and then redefine some of them in the derived class.
- Virtual Destructors Turn Off Synthesized Move.

### virtual inheritance
- non-virtual inheritance, the compiler knows how to access to the base class directly, and the acces is fast.  In the virtual inheritance, the derived class's virtual pointer that points to the "other stuff".  The "other stuff" holds the information on where to find the base class, so the access to base class is slowed down.
- virtual inheritance can be used without performance penalty when base classes contain no non-static data members, such as interface classes.

```
// non-virtual inheritance: the base class stuff always starts from offset 0
base class stuff
vptr
derived class members
```
```
// virtual inheritance:
Other parts of the object
vptr
derived class members
vptr ---------------------> other stuff---|
derived class members                     |
Other parts of the object                 |
base class stuff                      <---|
```




## initialization
- direct initialization, we are asking the compiler to use ordinary function matching to select the constructor that best matches the arguments we provide.
- copy initialization, we are asking the compiler to copy the right-hand operand into the object being created, converting that operand if necessary.  Copy initialization ordinarily uses the copy constructor. However, if a class has a move constructor, then copy initialization sometimes uses the move constructor instead of the copy constructor.

## globe scope
- Only declarations and definitions can appear at global scope. Of course, definitions can include expressions. int a = 5; defines a global variable, initialized by an expression.
- you can't just have a random statement/expression at global scope, like a = 5;. That is, expressions can be part of definitions, but an expression is not a definition.
- C++ requires that all global variables are initialized before main begins. So the compiler will have to invoke code pre-main. Which is perfectly legal.
- Every C++ compilation and execution system has some mechanism for invoking code before and after main. Globals have to be initialized, and object constructors may need to be called to do that initialization. After main completes, global variables have to be destroyed, which means destructors need to be called.

## linkage
- Linkage: An identifier’s linkage determines whether other declarations of that name refer to the same object or not, while scope defines where a single declaration can be seen and used.
- translation unit: When you write an implementation file (.cpp, .cxx, etc) your compiler generates a translation unit. This is the source file from your implementation plus all the headers you #included in it.
- No linkage: Local variables have no linkage, which means that each declaration refers to a unique object.
- Internal linkage refers to everything only in scope of a translation unit.
- External linkage refers to things that exist beyond a particular translation unit. In other words, accessible through the whole program, which is the combination of all translation units (or object files).
- control the linkage: You can explicitly control the linkage of a symbol by using the extern and static keywords
- default linkage:
the default linkage is extern (external linkage) for non-const symbols and static (internal linkage) for const symbols
- one-definition rule (ODR): we noted that the one-definition rule says that an object or function can’t have more than one definition, either within a file or a program.
- inline variables or functions mean multiple definitions allowed.

> The use of file static declarations is deprecated by the C++ standard.  File statics should be avoided and unnamed namespaces used instead.

However, it’s worth noting that internal objects (and functions) that are defined in different files are considered to be independent entities (even if their names and types are identical), so there is no violation of the one-definition rule. Each internal object only has one definition.
```
// In namespace scope or global scope.
int i; // extern by default
const int ci; // static by default
extern const int eci; // explicitly extern
static int si; // explicitly static

// The same goes for functions (but there are no const functions).
int f(); // extern by default
static int sf(); // explicitly static 
```
- Note that instead of using static (internal linkage), it is better to use anonymous namespaces into which you can also put classes. Though they allow extern linkage, anonymous namespaces are unreachable from other translation units, making linkage effectively static.
```
namespace {
  int i; // extern by default but unreachable from other translation units
  class C; // extern by default but unreachable from other translation units
}
```

- The keyword static plays a double role. When used in the definitions of global variables, it specifies internal linkage. When used in the definitions of the local variables, it specifies that the lifetime of the variable is going to be the duration of the program instead of being the duration of the function.

## memory types
### Static memory 
- local static objects
- class static data members
- variables defined outside any function
- Objects allocated in static are automatically created and destroyed by the compiler.  Static objects are allocated before they are used, and they are destroyed when the program ends.

### Stack memory
- nonstatic objects defined inside functions
- Objects allocated in stack memory are automatically created and destroyed by the compiler.  Stack objects exist only while the block in which they are defined is executing.

### heap (free store)
- In addition to static or stack memory, every program also has a pool of memory that it can use, which is referred to as the heap or free store.
- Objects that are dynamically allocated use heap memory.  That is, for objects that the program allocates at run time.
- Programs tend to use dynamic memory for one of three purposes:
    1. They don’t know how many objects they’ll need
    2. They don’t know the precise type of the objects they need
    3. They want to share data between several objects

## new and delete
- new and delete work on pointers that must point to a dynamically allocated object or be null
- Deleting a pointer to memory that was not allocated by new, or deleting the same pointer value more than once, is undefined.
- new operator allocates heap memory, and new expression constructs an object of the type on the free store and returns a pointer to that object.
- By default, dynamically allocated objects are default initialized (§ 2.2.1, p. 43), which means that objects of built-in or compound type have undefined value; objects of class type are initialized by their default constructor.
- if new is unable to allocate the requested storage, it throws an exception of type bad_alloc
- placement new: pass additional arguments to new such as new (nothrow).
- it is possible that a built-in pointer that points to a dynamically allocated object goes out of scope and not accessable, while the memory to which p points is not freed!

## Polymorphism in C++
- The fact that the static and dynamic types of references and pointers through inheritance can differ is the cornerstone of how C++ supports polymorphism.

## protected Members
- Like private, protected members are inaccessible to users of the class.
- Like public, protected members are accessible to members and friends of classes derived from this class.
- A derived class member or friend may access the protected members of the base class only through a derived object. The derived class has no special access to the protected members of base-class objects.

## RAII
RESOURCE ACQUISITION IS INITIALIZATION (RAII) 

## rvalue reference
- An rvalue reference is obtained by using && rather than &. As we’ll see, rvalue references have the important property that they may be bound only to an object that is about to be destroyed.
- we cannot directly bind an rvalue reference to an lvalue.  A variable is an expression with one
operand and no operator and variable expressions are lvalues. Hence we cannot bind an rvalue reference to a variable defined as an rvalue reference type.


```C++
int i = 42;
int &r = i; // ok: r refers to i
int &&rr = i; // error: cannot bind an rvalue reference to an lvalue
C++ Primer, Fifth Edition
int &r2 = i * 42; // error: i * 42 is an rvalue
const int &r3 = i * 42; // ok: we can bind a reference to const to an rvalue
int &&rr2 = i * 42; // ok: bind rr2 to the result of the multiplication
```

- Lvalues have persistent state, whereas rvalues are either literals or temporary objects created in the course of evaluating expressions.  Because rvalue references can only be bound to temporaries, we know that
    1. The referred-to object is about to be destroyed
    2. There can be no other users of that object
- Rvalues Are Moved, Lvalues Are Copied, but Rvalues Are Copied If There Is No Move Constructor.

## SFINAE
Substitution Failure If Not A Error, formally known as explicit overload set management

## sizeof
> The sizeof operator returns the size, in bytes, of an expression or a type name. The operator is right associative. The result of sizeof is a constant expression of type size_t. The operator takes one of two forms:
    - sizeof (type)
    - sizeof expr

- sizeof an array is the size of the entire array. It is equivalent to taking the sizeof the element type times the number of elements in the array. Note that sizeof does not convert the array to a pointer.
- sizeof a string or a vector returns only the size of the fixed part of these types; it does not return the size used by the object’s elements.

## smart pointers
- By default, a pointer used to initialize a smart pointer must point to dynamic memory because, by default, smart pointers use delete to free the associated object.
- We can bind smart pointers to pointers to other kinds of resources. However, to do so, we must supply our own operation to use in place of delete.
- a function is exited, whether through normal processing or due to an exception, all the local objects are destroyed.  Smart pointers will be handled properly.
- Because a unique_ptr owns the object to which it points, unique_ptr does not support ordinary copy or assignment.  We can only copy or assign a unique_ptr that is about to be destroyed. The most common example is when we return a unique_ptr from a function


## string
### string initialization
```C++
string s1; // default initialization; s1 is the empty string
strings2 = s1; // s2 isacopyof s1
string s5 = "hiya"; // copy initialization
string s6("hiya"); // direct initialization
string s7(10, 'c'); // direct initialization; s7 is cccccccccc
```

### string::size_type
- The type size_type is one of these companion types to make it possible to use the library types in a machine- independent manner. 
- Any variable used to store the result from the string size operation should be of type string::size_type.   Compiler provides the appropriate type by using auto or decltype

### Comparing strings
- operators: ==, !=, <, >, <=, =>
- comparating according to the characters in the string

## Translation unit (compilaition unit)
**Whatever show in a C++ file must appear in the translation unit.**

A translation unit is the basic unit of compilation in C++. It consists of the contents of a single source file, plus the contents of any header files directly or indirectly included by it, minus those lines that were ignored using conditional preprocessing statements.

A single translation unit can be compiled into an object file, library, or executable program.
The notion of a translation unit is most often mentioned in the contexts of the One Definition Rule, and templates.

Object file, in typical cases, is the result of the compilation unit being compiled.

Executable file is the result of linking the object file(s) of a project, together with the runtime library function.

Note that this is just practice, there is nothing in the C or C++ standards about executable files or object files. It's up to the compiler implementation to solve those things in whatever fashion they like.

## Trait
Think of a trait as a small object whose main purpose is to carry information used by another object or algorithm to determine "policy" or "implementation details". - Bjarne Stroustrup

## Type Conversions
Type conversions happen automatically when we use an object of one type where an object of another type is expected.

The compiler applies these type conversions:
- **both unsigned and int values in an arithmetic expression, the int value ordinarily is converted to unsigned.**
- **Regardless of whether one or both operands are unsigned, if we subtract a value from an unsigned, we must be sure that the result cannot be negative**
- nonbool arithmetic types -> a bool object, the result is false if the value is 0 and true otherwise.
- bool -> one of the other arithmetic types, the resulting value is 1 if the bool is true and 0 if the bool is false.
- floating-point value -> an object of integral type, the value is truncated. The value that is stored is the part before the decimal point.
- an integral value -> an object of floating-point type, the fractional part is zero. Precision may be lost if the integer has more bits than the floating-point object can accommodate.
- an out-of-range value -> an object of unsigned type, the result is the remainder of the value modulo the number of values the target type can hold. For example, an 8-bit unsigned char can hold values from 0 through 255, inclusive. If we assign a value outside this range, the compiler assigns the remainder of that value modulo 256. Therefore, assigning –1 to an 8-bit unsigned char gives that object the value 255.
- an out-of-range value -> an object of signed type, the result is undefined. The program might appear to work, it might crash, or it might produce garbage values.


## Using directives vs using declaration
using directive using namespace std; tells the compiler to import all of the names from the std namespace into the current scope (in some cases, of function main()). 
The using declaration using std::cout; tells the compiler that we’re going to be using the object cout from the std namespace. So whenever it sees cout, it will assume that we mean std::cout
> A header that has a using directive or declaration at its top-level scope injects names into every file that includes the header. Ordinarily, headers should define only the names that are part of its interface, not names used in its own implementation. As a result, header files should not contain using directives or using declarations except inside functions or namespaces


## variable storage
### storage duration types
- automation storage duration
    - objects storage are allocated at the begining of the enclosing block and deallocated at the end of the block
    - allied to all local objects that are not declared with thread_local, static or extern
- dynamic storage duration
    - object storage are allocated and deallocated by program using functions that perform dynamic memory allocation
    - objects are created with "new" and destroyed with "delete"
- static storage duration
    - Object storage is allocatated at the begining of the program and deallocated at the end of the program
    - all objects declared in namespace scope including global namespace
    - all objects declared with static or extern
    - only one instance of an object with static duration in the program
- thread storage duration
- object storage is allocated at the begining of the thread creating the object starts and deallocated when the thread ends
    - allied only to the object declared with thread_local
    - each thread has its own instance of the object with thread duration

First, one should discuss storage in terms of storage duration, the way C++ standard does: "stack" refers to automatic storage duration, while "heap" refers to dynamic storage duration. Both "stack" and "heap" are allocation strategies, commonly used to implement objects with their respective storage durations.

Global variables have static storage duration. They are stored in an area that is separate from both "heap" and "stack". Global constant objects are usually stored in "code" segment, while non-constant global objects are stored in the "data" segment.

- The scope of a name is the part of the program’s text in which that name is visible.
- The lifetime of an object is the time during the program’s execution that the object exists.
- Automatic Objects: Objects that exist only while a block is executing are known as automatic objects.  Both the scope and lifetime affect the automatic objects.  After execution exits a block, the values of the automatic objects created in that block are undefined.
- Parameters are automatic objects and ordinary local variables are automatic objects.
- local static objec: Each local static object is initialized before the first time execution passes through the object’s definition. Local statics are not destroyed when a function ends; they are destroyed when the program terminates.
_ local static has no explicit initializer, it is value initialized, meaning that local statics of built-in type are initialized to zero.


## variable life time
- Global objects are allocated at program start-up and destroyed when the program ends.
- Local, automatic objects are created and destroyed when the block in which they are defined is entered and exited. 
- Local static objects are allocated before their first use and are destroyed when the program ends.

- In addition to supporting automatic and static objects, C++ lets us allocate objects dynamically. Dynamically allocated objects have a lifetime that is independent of where they are created; they exist until they are explicitly freed.

## variadic paramter
```
{ 0, ( (void) bar(std::forward<Args>(args)), 0) ... };
  |       |       |                        |     |
  |       |       |                        |     --- pack expand the whole thing 
  |       |       |                        |   
  |       |       --perfect forwarding     --- comma operator
  |       |
  |       -- cast to void to ensure that regardless of bar()'s return type
  |          the built-in comma operator is used rather than an overloaded one
  |
  ---ensure that the array has at least one element so that we don't try to make an
     illegal 0-length array when args is empty
```


## vector
### declaration, define and initialize
- vector is a class template, which need instantiation for compiler to create a class.  Compiler need full definitions available in order to instantiate the template.
- header file need definition for vector, a definition is a declaration too.
- initialization
    - vector<int> ivec; // initially empty **defaul initialization**
    - vector<int> ivec2(ivec); // copy elements of ivec into ivec2 **copy initialization**
    - vector<int> ivec3 = ivec; // copy elements of ivec into ivec3 **copy initialization**
    - vector<string> articles = {"a", "an", "the"}; **list initialization**
    - vector<string> v1{"a", "an", "the"}; // **list initialization**
    - vector<string> v2("a", "an", "the"); // error
    - vector<int> ivec(10, -1); // ten int elements, each initialized to -1
    - vector<string> svec(10, "hi!"); // ten strings; each element is "hi!"
    - vector<int> ivec(10); // ten elements, each initialized to 0 **Value Initialization**
    - vector<string> svec(10); // ten elements, each an empty string **Value Initialization**
- value intialization
    - Library types: / user defined types: The value of the element initializer depends
    - built-in types: the element initializer has a value of ZERO

### tips
- Subscripting Does Not Add Elements, for v[i] = val to work, v[i] must exist.
- Subscript Only Elements that are Known to Exist!


## virtual table (vtbl, vtable)
a table shared by all the objects initiated from the same class.  And each object has a virtual pointer that points to this virtual table.  Virtual table contains all the virtual functions in addition to other stuff.


# already read
book:  C++ Move Semantics - The Complete Guide: First Edition

Back to Basics: Exceptions - Klaus Iglberger - CppCon 2020
https://www.youtube.com/watch?v=0ojB8c0xUd8

Fixing two-phase initialization  Andreas Weis

RAII and the rule of zero  Arthur O'Dwyer  CppCon 2019


# to read
std::mem_fn()
std::ref()
std::for_each()
CTRE - compile time Regular expression
compiler-explorer
without string optimization, string variable get allocated the memory for its value on heap even when the string variable is created in stack (https://www.youtube.com/watch?v=Bt3zcJZIalk)
a variable after move, is in a valid but unspecified state, which means the it is still the same type of variable, but is value is not guaranteed. (eg. a string variable after move, its size is not know, but it is still a string variable, and it can be printed and size() can be called, but there is no guarantee what is going to be printed out and the size could be any unsigned number.)

aliasing constructor of shared+ptr

unique_ptr can be implicitly converted into a shared_ptr, the other way is not possible.
weak_ptr can't be deferenced and it can only be used to get a shared_ptr.  weak_ptr is a ticket to the shared_ptr.
if an object is its own ticket, use enable_shared_from_this.

structure bindings C++17
std::exchange()
concepts
std::enable_if
std::is_floating_point
std::is_integral
std::string_view
std::puts
https://godbolt.org/z/hptUXj
SFINAE'd out
clang-tidy
ASAN
dynamic polymorphism with code injection and metaclasses (SY BRAND) video



# best practices

## Google code style guidance
- CppCon 2014: Titus Winters "The Philosophy of Google's C++ Code"
- 6 .Avoid tricky and hard-to-maintain constructs
    - Most code should avoid the tricky stuff. Waivers may be granted if justified.
    - Avoid macros (non-obvious, complicated)
    - Template metaprogramming (complicated, often non-obvious)
    - Non-public inheritance (suprising)
    - Multiple implementation inheritance (hard to maintain)

## shared_ptr / unique_ptr
pass pointer by value, not by reference.  Specificcally when using shared_ptr, unique_ptr.
shared_ptr and uniqueP_ptr should be used at implemetation details and should not appear in the interface.
std::enable_shared_from_this, is used to make an object be aware of itself being managed by a shared_ptr.  This is to avoid the situation that the same T* is managed by two different shared_ptr which don't know the existence of the other.
when construct the shared_ptr / unique_ptr, it should be considered as transfer the ownership of the raw pointer to the shared_ptr / unique_ptr.  After the ownership transfer, the use of the raw pointer should be stopped.

avoid naming return values: to get copy elision. C++17

fold expressions C++17

(void)std::initializer_list<int>{ (result += t, 0)...}

## class design
business-logic class: strive for Rule of Zero, with swap defined to allow optimizations.
Resource-management class: Rule of Three or Rule of Five.
To build a trivial class, to avoid all troubles, using pass-by_value and std::move() is a good solution. It is not the most run-efficient version, but is a overall good solution.

## string literal
string literal can initialize const char *, but not char *.  void func(const char *p) can be called with func("abc").
const char* can be converted to string, while string can't be converted into const char*;

Template paramters for call argument with default values are not deduced.  Need give default argument value to template parameter.

## 
RAII type + default constructors = Dangerous!
A default constructor is a constructor which can be called with no arguments (either defined with an empty parameter list, or with default arguments provided for every parameter). A type with a public default constructor is DefaultConstructible.


## sanitize
-Wshadow (very noisy, don't use ofter)
-Wshadow-compatible
-Wshadow-compatible-local
-clang-tidy
-ASAN
-fsanitize=address
-fsanitize-address-use-after_scope

## std::move(shared_ptr) vs assignment
Performance matters, no difference is on the sematics-wise.  "Move" is pilfering the pointers to controlled object and counter.  Assignment is to increase and then immediately decrease the counter, which is atomic.  The operations on atomic entity are much slower than the pilfering.



## Associative Containers and Algorithms
For associative containers, using their member functions is much faster than using the generic algorithm functions.




movies:
https://www.youtube.com/watch?v=SRCK9Z6cFMc&ab_channel=%E4%BC%AF%E4%BC%A6movie

2022-02-08
This is for building wxWidgets-3.0.5 on MacOs Monterey Version 12.2

# Build wxWidgets
## fixed gzlib.c lseek c99 error

add below in /Users/sgao/uExe/wxWidgets-3.0.5/src/zlib/gzguts.h as the first line
#include <unistd.h>

## Build multiple file static library
### configure command
../configure --with-osx_cocoa --enable-unicode --disable-shared --with-libjpeg=builtin --with-libpng=builtin --with-libtiff=builtin --with-regex=builtin --with-zlib=builtin --with-expat=builtin --with-macosx-version-min=12.2 --enable-universal_binary=x86_64  --enable-macosx-arch=x86_x64

make

## Build single file static library
### configure command
../configure --enable-monolithic --with-osx_cocoa --enable-unicode --disable-shared --with-libjpeg=builtin --with-libpng=builtin --with-libtiff=builtin --with-regex=builtin --with-zlib=builtin --with-expat=builtin --with-macosx-version-min=12.0 --enable-universal_binary=x86_64  --enable-macosx-arch=x86_x64

make


## Build multiple file dynamic shared library
../configure --with-osx_cocoa --enable-unicode  --with-libjpeg=builtin --with-libpng=builtin --with-libtiff=builtin --with-regex=builtin --with-zlib=builtin --with-expat=builtin --with-macosx-version-min=12.2 --enable-universal_binary=x86_64  --enable-macosx-arch=x86_x64

make

## Build single file shared library
../configure --enable-monolithic --with-osx_cocoa --enable-unicode  --with-libjpeg=builtin --with-libpng=builtin --with-libtiff=builtin --with-regex=builtin --with-zlib=builtin --with-expat=builtin --with-macosx-version-min=12.2 --enable-universal_binary=x86_64  --enable-macosx-arch=x86_x64

make

## Build single file with cmake
cd /Users/sgao/uExe/wxWidgets/wxWidgets-3.0.5-build-cmake
cmake  ../wxWidgets-3.0.5
it failed because there is no CMakeList.txt file.


# Use wxWidgets with CMAKE
## Solution 1
Use the build of single shared library
### get the result from these two commands
wx-config --cxxflags
wx-config --libs

### CMakeLists.txt file WITH USING DYNAMIC LIB
```
cmake_minimum_required(VERSION 3.0.0)
project(learn_words VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 17)

include(CTest)
enable_testing()


set(wxWidgets_build_root "/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/")
execute_process (
    COMMAND bash -c "${wxWidgets_build_root}wx-config --libs"
    OUTPUT_VARIABLE link_flags
)

message(${wxWidgets_build_root})

execute_process (
    COMMAND bash -c "${wxWidgets_build_root}wx-config --cxxflags"
    OUTPUT_VARIABLE compile_flags
)

add_library(wxWidgets SHARED IMPORTED)
set_target_properties(wxWidgets PROPERTIES
    IMPORTED_LOCATION "/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib/libwx_osx_cocoau-3.0.dylib"
    LINK_FLAGS "${link_flags}"
)

add_library(wxWidgets2 SHARED IMPORTED)
set_target_properties(wxWidgets2 PROPERTIES
    IMPORTED_LOCATION "/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib/libwx_osx_cocoau_gl-3.0.dylib"
    LINK_FLAGS "${link_flags}"
)



add_executable(learn_words src/main.cpp src/move.cpp)
target_include_directories(learn_words 
    PRIVATE "/Users/sgao/uExe/wxWidgets-3.0.5/include"
    PRIVATE "/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib/wx/include/osx_cocoa-unicode-3.0"
    PRIVATE "src/move.h")
set_target_properties(learn_words PROPERTIES
    COMPILE_FLAGS "${compile_flags}"
)

target_link_libraries(learn_words wxWidgets wxWidgets2)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
```

### CMakeLists.txt file WITH USING STATIC LIB
```
cmake_minimum_required(VERSION 3.0.0)
project(learn_words VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 17)
set( CMAKE_VERBOSE_MAKEFILE on )

include(CTest)
enable_testing()


set(wxWidgets_build_root "/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono")

execute_process (
    COMMAND bash -c "${wxWidgets_build_root}/wx-config --libs"
    OUTPUT_VARIABLE link_flags
)
string(STRIP "${link_flags}" link_flags)

execute_process (
    COMMAND bash -c "${wxWidgets_build_root}/wx-config --cxxflags"
    OUTPUT_VARIABLE compile_flags
)
string(STRIP "${compile_flags}" compile_flags)

add_library(wxWidgets STATIC IMPORTED)
set_target_properties(wxWidgets PROPERTIES
    IMPORTED_LOCATION "${wxWidgets_build_root}/lib/libwx_osx_cocoau-3.0.a"
)

add_library(wxWidgets2 STATIC IMPORTED)
set_target_properties(wxWidgets2 PROPERTIES
    IMPORTED_LOCATION "${wxWidgets_build_root}/lib/libwx_osx_cocoau_gl-3.0.a"
)

add_executable(learn_words src/main.cpp src/move.cpp)
target_include_directories(learn_words 
    PRIVATE "/Users/sgao/uExe/wxWidgets-3.0.5/include"
    PRIVATE "${wxWidgets_build_root}/lib/wx/include/osx_cocoa-unicode-3.0"
    PRIVATE "src"
    )
set_target_properties(learn_words PROPERTIES
    COMPILE_FLAGS "${compile_flags}"
)

target_link_libraries(learn_words 
    PRIVATE wxWidgets 
    PRIVATE wxWidgets2 
    PRIVATE "${link_flags}"
    )

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
```



### copy the result to CMakeLists.txt file 
```
cmake_minimum_required(VERSION 3.0.0)
project(learn_words VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 17)

include(CTest)
enable_testing()


add_library(wxWidgets SHARED IMPORTED)
set_target_properties(wxWidgets PROPERTIES
    IMPORTED_LOCATION "/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib/libwx_osx_cocoau-3.0.dylib"
    LINK_FLAGS "-L/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib   -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL -lwx_osx_cocoau-3.0"
)

add_library(wxWidgets2 SHARED IMPORTED)
set_target_properties(wxWidgets2 PROPERTIES
    IMPORTED_LOCATION "/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib/libwx_osx_cocoau_gl-3.0.dylib"
    LINK_FLAGS "-L/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib   -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL -lwx_osx_cocoau-3.0"
)

add_executable(learn_words src/main.cpp src/move.cpp)
target_include_directories(learn_words 
    PRIVATE "/Users/sgao/uExe/wxWidgets-3.0.5/include"
    PRIVATE "/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib/wx/include/osx_cocoa-unicode-3.0"
    PRIVATE "src/move.h")
set_target_properties(learn_words PROPERTIES
    COMPILE_FLAGS "-I/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_dynamic_mono/lib/wx/include/osx_cocoa-unicode-3.0 -I/Users/sgao/uExe/wxWidgets-3.0.5/include -D_FILE_OFFSET_BITS=64 -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D__WXOSX_COCOA__"
)

target_link_libraries(learn_words wxWidgets wxWidgets2)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
```

# wxWidgets component creations
## no need delete
> When a wxWindow is destroyed, it automatically deletes all its children. These children are all the objects that received the window as the parent-argument in their constructors.
As a consequence, if you're creating a derived class that contains child windows, you should use a pointer to the child windows instead of the objects themself as members of the main window.
- https://forums.wxwidgets.org/viewtopic.php?t=26244

## code example
```C++
Oneclass::OnSomeButtonClick(wxCommandEvent& event)
{
     SomeDialog dlg(this);
     if (dlg.ShowModal()==wxID_OK)
     {
              //do something
     }
}
```
```C++
{
     SomeDialog *dlg = new SomeDialog(this);
     if (dlg->ShowModal()==wxID_OK)
     {
           // do something
     }
     dlg->Close();
}
```
```C++
Oneclass:OnSomeButtonClick(wxCommandEvent& event)
{
     SomeDialog *dlg = new SomeDialog(NULL);
     if (dlg->ShowModal()==wxID_OK)
     {
           // do something
     }
     delete dlg;  // or dlg->Destroy()  ????
}
```
I think in practice both are fine.

The first should set up your frame on the stack and will delete it when the code goes out of scope (i.e. when that function returns).

The second is the preferable one. It sets up things on the heap. I don't think you need to do an explicit Close(). Since the window parent is "this", then it will be closed properly when the parent window is closed.

>Allocating and Deleting wxWidgets Objects
In general, classes derived from wxWindow must dynamically allocated with new and deleted with delete. If you delete a window, all of its children and descendants will be automatically deleted, so you don't need to delete these descendants explicitly.

When deleting a frame or dialog, use Destroy rather than delete so that the wxWidgets delayed deletion can take effect. This waits until idle time (when all messages have been processed) to actually delete the window, to avoid problems associated with the GUI sending events to deleted windows.

In general wxWindow-derived objects should always be allocated on the heap as wxWidgets will destroy them itself. The only, but important, exception to this rule are the modal dialogs, i.e. wxDialog objects which are shown using wxDialog::ShowModal() method. They may be allocated on the stack and, indeed, usually are local variables to ensure that they are destroyed on scope exit as wxWidgets does not destroy them unlike with all the other windows. So while it is still possible to allocate modal dialogs on the heap, you should still destroy or delete them explicitly in this case instead of relying on wxWidgets doing it.

If you decide to allocate a C++ array of objects (such as wxBitmap) that may be cleaned up by wxWidgets, make sure you delete the array explicitly before wxWidgets has a chance to do so on exit, since calling delete on array members will cause memory problems.

wxColour can be created statically: it is not automatically cleaned up and is unlikely to be shared between other objects; it is lightweight enough for copies to be made.

Beware of deleting objects such as a wxPen or wxBitmap if they are still in use. Windows is particularly sensitive to this, so make sure you make calls like wxDC::SetPen(wxNullPen) or wxDC::SelectObject(wxNullBitmap) before deleting a drawing object that may be in use. Code that doesn't do this will probably work fine on some platforms, and then fail under Windows.
-https://docs.wxwidgets.org/trunk/page_multiplatform.html#page_multiplatform_allocatingobjects
2022-02-09




----------------
(cd bombs && /Library/Developer/CommandLineTools/usr/bin/make all)
/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/bk-deps g++ -mmacosx-version-min=12.2 -c -o bombs_bombs.o -D__WXOSX_COCOA__      -I../../../demos/bombs  -I../../../demos/bombs/../../samples -Wall -Wundef -Wunused-parameter -Wno-ctor-dtor-privacy -Woverloaded-virtual -Wno-deprecated-declarations -D_FILE_OFFSET_BITS=64 -I/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib/wx/include/osx_cocoa-unicode-static-3.0 -I../../../include -D__ASSERT_MACROS_DEFINE_VERSIONS_WITHOUT_UNDERSCORES=1 -DWX_PRECOMP -O2 -fno-strict-aliasing -arch x86_64 -fno-common  ../../../demos/bombs/bombs.cpp
/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/bk-deps g++ -mmacosx-version-min=12.2 -c -o bombs_bombs1.o -D__WXOSX_COCOA__      -I../../../demos/bombs  -I../../../demos/bombs/../../samples -Wall -Wundef -Wunused-parameter -Wno-ctor-dtor-privacy -Woverloaded-virtual -Wno-deprecated-declarations -D_FILE_OFFSET_BITS=64 -I/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib/wx/include/osx_cocoa-unicode-static-3.0 -I../../../include -D__ASSERT_MACROS_DEFINE_VERSIONS_WITHOUT_UNDERSCORES=1 -DWX_PRECOMP -O2 -fno-strict-aliasing -arch x86_64 -fno-common  ../../../demos/bombs/bombs1.cpp
/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/bk-deps g++ -mmacosx-version-min=12.2 -c -o bombs_game.o -D__WXOSX_COCOA__      -I../../../demos/bombs  -I../../../demos/bombs/../../samples -Wall -Wundef -Wunused-parameter -Wno-ctor-dtor-privacy -Woverloaded-virtual -Wno-deprecated-declarations -D_FILE_OFFSET_BITS=64 -I/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib/wx/include/osx_cocoa-unicode-static-3.0 -I../../../include -D__ASSERT_MACROS_DEFINE_VERSIONS_WITHOUT_UNDERSCORES=1 -DWX_PRECOMP -O2 -fno-strict-aliasing -arch x86_64 -fno-common  ../../../demos/bombs/game.cpp
g++ -mmacosx-version-min=12.2 -o bombs  bombs_bombs.o bombs_bombs1.o bombs_game.o     -L/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib   -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL       -lwx_osx_cocoau-3.0 -lwxscintilla-3.0 -lwxtiff-3.0 -lwxjpeg-3.0 -lwxpng-3.0   -lwxzlib-3.0 -lwxregexu-3.0 -lwxexpat-3.0 -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL  -lpthread -liconv -llzma    -lpthread -liconv -llzma
mkdir -p bombs.app/Contents
mkdir -p bombs.app/Contents/MacOS
mkdir -p bombs.app/Contents/Resources
sed -e "s/IDENTIFIER/`echo ../../../demos/bombs | sed -e 's,\.\./,,g' | sed -e 's,/,.,g'`/" \
	-e "s/EXECUTABLE/bombs/" \
	-e "s/VERSION/3.0.5/" \
	../../../src/osx/carbon/Info.plist.in >bombs.app/Contents/Info.plist


-L/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib   -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL /Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib/libwx_osx_cocoau-3.0.a -lwxregexu-3.0 -lwxexpat-3.0 -lwxtiff-3.0 -lwxjpeg-3.0 -lwxpng-3.0 -lwxzlib-3.0 -lpthread -liconv -llzma 

-- below is from the build command for demos
-L/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib   -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL       -lwx_osx_cocoau-3.0 -lwxscintilla-3.0 -lwxtiff-3.0 -lwxjpeg-3.0 -lwxpng-3.0   -lwxzlib-3.0 -lwxregexu-3.0 -lwxexpat-3.0 -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL  -lpthread -liconv -llzma    -lpthread -liconv -llzma
mkdir -p bombs.app/Contents


/Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/move.cpp.o  
/Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/main.cpp.o


### command created based on the demo - works
g++ -mmacosx-version-min=12.2 -o my_move  /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/move.cpp.o  /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/main.cpp.o   -L/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib   -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL       -lwx_osx_cocoau-3.0 -lwxscintilla-3.0 -lwxtiff-3.0 -lwxjpeg-3.0 -lwxpng-3.0   -lwxzlib-3.0 -lwxregexu-3.0 -lwxexpat-3.0 -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL  -lpthread -liconv -llzma    -lpthread -liconv -llzma


### command created by CMake but does not work:
/usr/bin/clang++ -g -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX12.1.sdk -Wl,-search_paths_first -Wl,-headerpad_max_install_names CMakeFiles/learn_words.dir/src/main.cpp.o CMakeFiles/learn_words.dir/src/move.cpp.o -o learn_words  /Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib/libwx_osx_cocoau-3.0.a /Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib/libwx_osx_cocoau_gl-3.0.a 

### test1 - works
/usr/bin/clang++ -g -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX12.1.sdk -Wl,-search_paths_first -Wl,-headerpad_max_install_names /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/main.cpp.o

### test2 - does not work
/usr/bin/clang++ -g -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX12.1.sdk -Wl,-search_paths_first -Wl,-headerpad_max_install_names /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/main.cpp.o /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/move.cpp.o -o learn_words  /Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib/libwx_osx_cocoau-3.0.a /Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib/libwx_osx_cocoau_gl-3.0.a 


### test3 - works
/usr/bin/clang++ -g -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX12.1.sdk -Wl,-search_paths_first -Wl,-headerpad_max_install_names /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/main.cpp.o /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/move.cpp.o -o learn_words  -L/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib   -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL       -lwx_osx_cocoau-3.0 -lwxscintilla-3.0 -lwxtiff-3.0 -lwxjpeg-3.0 -lwxpng-3.0   -lwxzlib-3.0 -lwxregexu-3.0 -lwxexpat-3.0 -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL  -lpthread -liconv -llzma    -lpthread -liconv -llzma

### test4 - works
/usr/bin/clang++ -g -isysroot /Library/Developer/CommandLineTools/SDKs/MacOSX12.1.sdk -Wl,-search_paths_first -Wl,-headerpad_max_install_names /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/main.cpp.o /Users/sgao/cpp/learn_words/build/CMakeFiles/learn_words.dir/src/move.cpp.o -o learn_words  -L/Users/sgao/uExe/wxWidgets-3.0.5/build-cocoa-debug_static_mono/lib   -arch x86_64  -framework IOKit -framework Carbon -framework Cocoa -framework AudioToolbox -framework System -framework OpenGL       -lwx_osx_cocoau-3.0 -lwxscintilla-3.0 -lwxtiff-3.0 -lwxjpeg-3.0 -lwxpng-3.0   -lwxzlib-3.0 -lwxregexu-3.0  -lwxexpat-3.0   -lpthread -liconv -llzma



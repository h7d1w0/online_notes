---
title: "C++ library creation and usage"
date: 2021-12-28T13:05:01-05:00
draft: false

categories: ["programming"]
tags: ["C++", "library", "clang++", "ar", "build", "package"]
---

Using commands to generate C++ library and use the generated library.

These test and run occurred on MacOS.  The ccommands used are clang++ and ar.
<!--more-->

# Static Library
Two projects are created: pack1 and pack2.  Pack1 is to create a static library and pack2 shows how the library created in pack1 is used.
## pack1
### Directories and files
```
pack1
|
|----build
|    |
|    |----pack1.o
|
|----include
|    |
|    |----pack1
|         |
|         |----pack1_header.hpp
|
|----lib
|    |
|    |----pack1.a
|
|----src
|    |
|    |----pack1.cpp
```
### source files
```C++
//file ../pack1/include/pack1/pack1_header.hpp
namespace mynamespace
{
    void helloWorld();
}
```
```C++
//file ../pack1/src/pack1.cpp
#include <iostream>
#include "pack1/pack1_header.hpp"

namespace mynamespace
{
    void helloWorld()
    {
        std::cout << "Hello World!" << std::endl;
    }
}
```
### command and results

```sh
cd ../pack1/build
clang++ \
    -c \
    -Wall \
    -o pack1.o \
    -I ../include \
    ../src/pack1.cpp
```
../pack1/build/pack1.o is generated.

```sh
cd ../pack1/build
ar rc ../pack1.a pack1.o
```
../pack1/pack1.a is generated.
## pack2
### Directories and files
```
pack2
|
|----build
|    |
|    |----main
|
|
|----lib
|    |
|    |----pack1
|         |
|         |----include
|         |     |
|         |     |----pack1
|         |         |
|         |         |----pack1_header.hpp
|         |
|         |----pack1.a
|
|----src
|    |
|    |----main.cpp
```
### source files
```C++
//file ../pack2/src/pack2.cpp
#include "pack1/pack1_header.hpp"
int main(void)
{
    mynamespace::helloWorld();
    return(0);
}
```

under lib, everything of pack1 are copied from pack1 project

### command and results
```sh
cd ../pack2/build
clang++ \
    -Wall \
    -o main \
    -I ../lib/pack1/include \
    ../src/main.cpp \
    ../lib/pack1/pack1.a
```
../pack2/build/main is generated.


# Dynamic Library

Dynamic libraries typically have different suffixes per OS since each OS has it’s preferred object file format. On Linux the .so suffix is common, .dylib on OSX, and .dll on Windows.

```sh
cd ../pack1/build
clang++ \
    -shared \
    -Wall \
    -I ../include \
    -fpic ../src/pack1.cpp \
    -o libpack1.dylib
```



```sh
clang++ \
    -Wall \
    -o main_dy1 \
    ../src/main.cpp \
    ../lib/pack1/libpack1.dylib
```

```sh
clang++ \
    -Wall \
    -o main_dy3 \
    ./src/main_dy2.cpp \
    ./lib/pack1/libpack2.dylib
```

# Other Notes
## libc++
This project is using Mac OS system libc++.  Other version of libc++ can be used too.  However, this must be kept in mind: replacing your systems libc++ installation could render the system non-functional.  Mac OS X will not boot without a valid copy of libc++.1.dylib in /usr/lib.otool, nm, 

## using debugger
### Mac OS
lldb main_dy3
image lookup -r -s puts
image lookup -r -s helloWorld

### Linux
ldd or readelf -d

## check dynamic linker
### Mac OS
sudo dtruss ./a.out

### Linux
LD_LIBRARY_PATH=. strace ./a.out

## include header
THe correctness of the header path in the include clause inside the C++ source file may not matter if the driver command line(eg. clang++) contains the path in the -I option.

With CMake, the following two options work well too:

- There is no target_include_directories() called while the C++ source files include statements have the correct paths
- The C++ source files include statements have incomplete paths(eg. only header file name is there but without path), while the target_include_directories() correctly contains the path of the incomplete header file
---
title: "About"
date: 2018-10-03T09:26:12-04:00
draft: ture
---

# What this website is
This website is used to record notes that contain the information from other websites and my own understanding on the subjects.  Since it gathers notes but not an academic essays, Citation Rules are not restrictly followed, though the best effort is given to show the source of the information.

# About me
A developer, data engineer.

Engineers, as practitioners of engineering, are people who invent, design, analyze, build, and test machines, systems, structures and materials to fulfill objectives and requirements while considering the limitations imposed by practicality, regulation, safety, and cost.
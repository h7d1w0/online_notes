---
title: "Java_module"
date: 2018-10-23T15:33:33-04:00
draft: true
---

https://blog.codefx.org/java/java-module-system-tutorial/

mkdir JPMS-Hello-World

mkdir -p JPMS-Hello-World/src/main/java/org/codefx/demo/jpms/

File under JPMS-Hello-World/src/main/java/org/codefx/demo/jpms/
package org.codefx.demo.jpms;
 
public class HelloModularWorld {
 
    public static void main(String[] args) {
        System.out.println("Hello, modular World!");
    }
 
}

File under JPMS-Hello-World/src/main/java/
module org.codefx.demo.jpms_hello_world {
    // this module only needs types from the base module 'java.base';
    // because every Java module needs 'java.base', it is not necessary
    // to explicitly require it - I do it nonetheless for demo purposes
    requires java.base;
    // this export makes little sense for the application,
    // but once again, I do this for demo purposes
    exports org.codefx.demo.jpms;
}

cd JPMS-Hello-World
javac  -d target/classes  src/main/java/*.java src/main/java/org/codefx/demo/jpms/*
jar --create\
    --file target/jpms-hello-world.jar\
    --main-class org.codefx.demo.jpms.HelloModularWorld\
    -C target/classes .

java --module-path target/jpms-hello-world.jar --module org.codefx.demo.jpms_hello_world


jar --create\
    --file target/jpms-hello-world.jar\
    -C target/classes .
java --module-path target/jpms-hello-world.jar --module org.codefx.demo.jpms_hello_world/org.codefx.demo.jpms.HelloModularWorld
java --module-path target/jpms-hello-world.jar --module org.codefx.demo.jpms_hello_world/org.codefx.demo.jpms.HelloModularWorld2


java --module-path target/example-1.0-SNAPSHOT.jar --module org.codefx.demo.jpms_hello_world/org.codefx.demo.jpms.HelloModularWorld

Modules
Modules are like JARs with additional characteristics

descriptor module-info.class, which is compiled from a module declaration module-info.java

The basic building block of the JPMS are modules (surprise!). Like JARs, they are a container for types and resources; but unlike JARs, they have additional characteristics – these are the most fundamental ones:

a name, preferably one that is globally unique
declarations of dependencies on other modules
a clearly defined API that consists of exported packages

java --list-modules and look at an individual module with java --describe-module ${module}.


module-info.java in the project’s root, which looks as follows:

module ${module-name} {
    requires ${module-name};
    exports ${package-name};
}


It gets compiled into a module-info.class, called module descriptor, and ends up in the JAR’s root. This descriptor is the only difference between a plain JAR and a modular JAR.

${module-name}
globally unique
stable




Implied Readability
That’s pretty inconvenient, so modules like alpha that use another module’s type in their own public API should generally require that module with the transitive modifier.


Optional Dependencies
This is quite straight-forward: If you want to compile against a module’s types, but don’t want to force its presence at runtime you can mark your dependency as being optional with the static modifier:


When a module needs to be compiled against types from another module but does not want to depend on it at run time, it can use a requires static clause. If foo requires static bar, the module system behaves different at compile and run time:

At compile time, bar must be present or there will be an error. During compilation bar is readable by foo.
At run time, bar might be absent and that will cause neither error nor warning. If it is present, it is readable by foo.


Qualified Exports
Regular exports have you make the decision whether a package’s public types are accessible only within the same module or to all modules. Sometimes you need something in between, though. If you’re shipping a bunch of modules, you might end up in the situation, where you’d like to share code between those modules but not outside of it. Qualified exports to the rescue!



Open Packages And Modules
An open package is inaccessible at compile time (so you can’t write code against its types), but accessible at run time (so reflection works). More than just being accessible, it allows reflective access to non-public types and members (this is called deem reflection). Open packages can be qualified just like exports and open modules simply open all their packages.

Services
Instead of having the main module monitor depend on monitor.observer.alpha and monitor.observer.beta, so it can create instances of AlphaServiceObserver and BetaServiceObserver, it could let the module system make that connection:

module monitor {
    requires monitor.observer;
    // monitor wants to use a service
    uses monitor.observer.ServiceObserverFactory;
    requires monitor.statistics;
    requires monitor.persistence;
    requires monitor.rest;
}
 
module monitor.observer.alpha {
    requires monitor.observer;
    // alpha provides a service implementation
    provides monitor.observer.ServiceObserverFactory
        with monitor.observer.alpha.AlphaServiceObserverFactory;
}
 
module monitor.observer.beta {
    requires monitor.observer;
    // beta provides a service implementation
    provides monitor.observer.ServiceObserverFactory
        with monitor.observer.beta.BetaServiceObserverFactory;
}
This way, monitor can do the following to get an instance of each provided observer factory:

List<ServiceObserverFactory> observerFactories = ServiceLoader
    .load(ServiceObserverFactory.class).stream()
    .map(Provider::get)
    .collect(toList());
It uses the ServiceLoader API, which exists since Java 6, to inform the module system that it needs all implementations of ServiceObserverFactory. The JPMS will then track down all modules in the readability graph that provide that service, create an instance of each and return them.

There are two particularly interesting consequences:

the module consuming the service does not have to require the modules providing it
the application can be configured by selecting which modules are placed on the module path
Services are a wonderful way to decouple modules and its awesome that the module system gives this mostly ignored concept a second life and puts it into a prominent place.



Illegal Access To Internal APIs
The most obvious and sustainable fix for dependencies on internal APIs is to get rid of them. 
If that can’t be done for whatever reason, the next best thing is to acknowledge the dependencies and inform the module system that you need to access it. To that end you can use two command line options:

The option --add-exports module/package=$readingmodule exports $package of $module to $readingmodule. Code in $readingmodule can hence access all public types in $package but other modules can not. When setting $readingmodule to ALL-UNNAMED, all code from the class path can access that package. During a migration to Java 11, you will always use that placeholder (you will have to change it when you modularize). The option is available for the java and javac commands.
This covers access to public members of public types but reflection can do more than that: With the generous use of setAccessible(true) it allows interaction with non-public classes, fields, constructors, and methods (sometimes called deep reflection), which even in exported packages are still encapsulated. The java option --add-opens uses the same syntax as --add-exports and opens the package to deep reflection, meaning all of its types and their members are accessible regardless of their visibility modifiers.
You obviously need --add-exports to appease the compiler but using --add-exports and --add-opens for the run time has advantages as well:

the run time’s permissive behavior will change in future Java releases, so you have to do that work at some point anyway
--add-opens makes the warnings for illegal reflective access go away
as I will show in a minute, you can make sure no new dependencies crop up by making the run time actually enforce strong encapsulation

# Linux / MacOS
$ find -name "*.java" > sources.txt
$ javac @sources.txt


:: Windows
> dir /s /B *.java > sources.txt
> javac @sources.txt



docker exec -it e985fdaf50d9 /bin/bash


curl http://www-us.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz -o apache-maven-3.5.4-bin.tar.gz

/root/apache-maven-3.5.4/bin
---
title: "Java Jackson - create json String file"
date: 2018-10-09T11:43:11-04:00
draft: false

categories: ["programming"]
tags: ["json", "json string", "jackson", "ObjectMapper", "JsonGenerator"]
---

* Notes:
  + Combining ObjectMapper with JsonGenerator to append multiple lines to one file
  + mapper.writeValue(jsonGenerator, page); jsonGenerator.writeRaw('\n');

Using JsonGenerator and ObjectMapper from com.fasterxml.jackson to create json string file.
<!--more-->
# com.fasterxml.jackson.databind.ObjectMapper
(Based on https://fasterxml.github.io/jackson-databind/javadoc/2.7/com/fasterxml/jackson/databind/ObjectMapper.html)

* Reading and writing JSON, either to and from basic POJOs (Plain Old Java Objects), or to and from a general-purpose JSON Tree Model (JsonNode).  It handles many Java objects - String, long, Map... - automatically.
* acts as a factory for more advanced ObjectReader and ObjectWriter classes. Mapper (and ObjectReaders, ObjectWriters it constructs) will use instances of JsonParser and JsonGenerator for implementing actual reading/writing of JSON. 
* Most read and write methods are exposed through this class, some of the functionality is only exposed via ObjectReader and ObjectWriter: specifically, reading/writing of longer sequences of values is only available through ObjectReader.readValues(InputStream) and ObjectWriter.writeValues(OutputStream).


# Code


```java
/*
  The code has been tested with Java 8

*/
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Test4 {

    public static void main(String[] args) {
        File csvFile = new File("/tmp/test");

        FileOutputStream fileOut = null;
        ObjectMapper mapper = new ObjectMapper();
        JsonGenerator jsonGenerator = null;

        try {
            fileOut = new FileOutputStream(csvFile.getAbsoluteFile());
            jsonGenerator = mapper.getFactory().createGenerator(fileOut);
            jsonGenerator.setPrettyPrinter(new MinimalPrettyPrinter(""));
            Page page = new Page();

            int resultSize = 100;

            for (int i = 0; i < resultSize; i++) {
                Long lineItemId = new Random().nextLong();
                String lineItemName = generateRandomString(15);

                page.setId(lineItemId);
                page.setTitle(lineItemName);
                page.setTags(new String[]{"java", "programming", "static"});
                
                HashMap<String, String> customAttributes = new HashMap<String, String>();
                customAttributes.put("key1_" + generateRandomString(3), generateRandomString(7));
                customAttributes.put("key2_" + generateRandomString(3), generateRandomString(7));
                page.setCustomAttributes(customAttributes);

                mapper.writeValue(jsonGenerator, page);
                jsonGenerator.writeRaw('\n');

            }
        } catch (RemoteException ex) {
            Logger.getLogger(Test4.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Test4.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (jsonGenerator != null) {
                try {
                    jsonGenerator.close();
                } catch (IOException ex) {
                    Logger.getLogger(Test4.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (IOException ex) {
                    Logger.getLogger(Test4.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    private static String generateRandomString(int stringLength) {
        byte[] array = new byte[stringLength];
        int leftLimit = 97;
        int rightLimit = 122;

        for (int i = 0; i < stringLength; i++){
            float val = new Random().nextFloat();
            array[i] = (byte) (leftLimit + (int) (val * (rightLimit - leftLimit)));
        }
        return new String(array, StandardCharsets.UTF_8);
    }

    private static class Page {

        private long id;
        private String title;
        private String[] tags;
        private Map<String, String> customAttributes;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String[] getTags() {
            return tags;
        }

        public void setTags(String[] tags) {
            this.tags = tags;
        }

        public Map<String, String> getCustomAttributes() {
            return customAttributes;
        }

        public void setCustomAttributes(Map<String, String> customAttributes) {
            this.customAttributes = customAttributes;
        }
    }
}
```

Result sample:
{"id":1839076225488268786,"title":"igyowtusbebjhoc","tags":["java","programming","static"],"customAttributes":{"key2_eae":"xsgeael","key1_rss":"uoxnpmq"}}
{"id":1395382647228341042,"title":"orrpbikgqjwncrp","tags":["java","programming","static"],"customAttributes":{"key1_yhu":"qdtwnga","key2_irf":"lbptucm"}}


```java

package com.tgam.databrickstest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sgao
 */
public class Main {

    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final String USER_CREDENTIALS = "............";

    public static void main(String[] args) throws JsonProcessingException {
        try {
            HttpRequestFactory requestFactory
                    = HTTP_TRANSPORT.createRequestFactory((HttpRequest request) -> {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    });

            AwsAttributesConfig awsAttributesConfig = new AwsAttributesConfig();
            NewClusterConfig newClusterConfig
                    = new NewClusterConfig("4.1.x-scala2.11", "m4.xlarge", "m4.xlarge", 2, awsAttributesConfig);

            HashMap<String, String> base_parameters = new HashMap();
            base_parameters.put("YEAR", "2019");
            base_parameters.put("MONTH", "4");
            base_parameters.put("DAY", "1");
            base_parameters.put("NUMBEROFDAYS", "1");
            NotebookTaskConfig notebookTaskConfig
                    = new NotebookTaskConfig("/Users/SGao@globeandmail.com/cleon/cleon_postDownload_process", base_parameters);
            RunSubmitConfig runSubmitConfig = new RunSubmitConfig("Test", newClusterConfig, notebookTaskConfig);
            
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.valueToTree(runSubmitConfig);
            Map<String, Object> result = mapper.convertValue(node, Map.class);
            
            HttpContent content = new JsonHttpContent(JSON_FACTORY, result);

            GenericUrl url = new GenericUrl();
            url.setScheme("https");
            url.setHost("globeandmail-dev.cloud.databricks.com/");
            List<String> pathPars = new ArrayList();
            pathPars.add("api");
            pathPars.add("2.0");
            pathPars.add("jobs");
            pathPars.add("runs");
            pathPars.add("submit");
            url.setPathParts(pathPars);
            HttpRequest request = null;
            try {
                request = requestFactory.buildPostRequest(url, content);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(USER_CREDENTIALS.getBytes()));
            HttpHeaders headers = request.getHeaders();
            headers.setAuthorization(basicAuth);
            headers.setContentType("Content-Type: application/json");
            HttpResponse rs = request.execute();
            OutputStream out = new FileOutputStream("/tmp/out");
//            content.writeTo(out);
//            rs.download(out);
//            System.out.println(rs.getContentEncoding());
            System.out.println(url.build());
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
```

```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tgam.databrickstest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sgao
 */
public class Main {

    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final String USER_CREDENTIALS = "SGao@globeandmail.com:?(YJ6Y~MK5R6$osC";

    public static void main(String[] args) throws JsonProcessingException {
        try {
            HttpRequestFactory requestFactory
                    = HTTP_TRANSPORT.createRequestFactory((HttpRequest request) -> {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    });

            ObjectMapper mapper = new ObjectMapper();
            JsonNode baseParameters = mapper.createObjectNode();
            ((ObjectNode) baseParameters).put("YEAR", "2019");
            ((ObjectNode) baseParameters).put("MONTH", "4");
            ((ObjectNode) baseParameters).put("DAY", "1");
            ((ObjectNode) baseParameters).put("NUMBEROFDAYS", "1");

            Map<String, Object> runSubmitRequestBody = RunSubmitRequestBodyFactory.build()
                    .setNotebookPath("/Users/SGao@globeandmail.com/cleon/cleon_postDownload_process")
                    .setSparkVersion("4.1.x-scala2.11")
                    .setDriverNodeType("m4.xlarge")
                    .setNodeType("m4.xlarge")
                    .setNumWorkers(1)
                    .setNotebookBaseParameters(baseParameters)
                    .create();
            HttpContent content = new JsonHttpContent(JSON_FACTORY, runSubmitRequestBody);

            GenericUrl url = new GenericUrl();
            url.setScheme("https");
            url.setHost("globeandmail-dev.cloud.databricks.com/");
            List<String> pathPars = new ArrayList();
            pathPars.add("api");
            pathPars.add("2.0");
            pathPars.add("jobs");
            pathPars.add("runs");
            pathPars.add("submit");
            url.setPathParts(pathPars);
            HttpRequest request = null;
            try {
                request = requestFactory.buildPostRequest(url, content);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(USER_CREDENTIALS.getBytes()));
            HttpHeaders headers = request.getHeaders();
            headers.setAuthorization(basicAuth);
            headers.setContentType("Content-Type: application/json");
            HttpResponse rs = request.execute();
            OutputStream out = new FileOutputStream("/tmp/out");
//            content.writeTo(out);
            rs.download(out);
//            System.out.println(rs.getContentEncoding());
            System.out.println(url.build());
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
/*
'{
  "run_name": "yeildex",
  "new_cluster": {
    "spark_version": "4.1.x-scala2.11",
    "driver_node_type_id": "m4.xlarge",
    "node_type_id": "c4.4xlarge",
    "num_workers": 4,

    "aws_attributes": {
        "availability": "ON_DEMAND",
        "ebs_volume_count":1,
        "ebs_volume_type": "THROUGHPUT_OPTIMIZED_HDD",
        "ebs_volume_size":500
      }
  },
  "notebook_task": {
    "notebook_path": "/Users/SGao@globeandmail.com/ScheduledJobs/yieldex_parameters",
    "base_parameters": {
        "YEAR": "2019",
        "MONTH": "1",
        "DAY": "1",
        "NUMBEROFDAYS": "5"
    }
  }
}'
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tgam.databrickstest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Map;

/**
 *
 * @author sgao
 */
public class RunSubmitRequestBodyFactory {
    
    private RunSubmitRequestBodyFactory(){
        
    }
    
    static RunSubmitRequestBody build(){
        return new RunSubmitRequestBody();
    }
    
    static class RunSubmitRequestBody {

        final private JsonNode requestBody;
        final private JsonNode notebookTask;
        final private JsonNode newCluster;
        final private ObjectMapper mapper;

        RunSubmitRequestBody() {
            mapper = new ObjectMapper();
            this.requestBody = mapper.createObjectNode();
            this.notebookTask = mapper.createObjectNode();
            this.newCluster = mapper.createObjectNode();
        }

        RunSubmitRequestBody setSparkVersion(String sparkVersion) {
            ((ObjectNode) newCluster).put("spark_version", sparkVersion);
            return this;
        }

        RunSubmitRequestBody setDriverNodeType(String driverNodeType) {
            ((ObjectNode) newCluster).put("driver_node_type_id", driverNodeType);
            return this;
        }

        RunSubmitRequestBody setNodeType(String nodeType) {
            ((ObjectNode) newCluster).put("node_type_id", nodeType);
            return this;
        }

        RunSubmitRequestBody setNumWorkers(int num) {
            ((ObjectNode) newCluster).put("num_workers", num);
            return this;
        }

        RunSubmitRequestBody setNotebookPath(String notebookPath) {
            ((ObjectNode) notebookTask).put("notebook_path", notebookPath);
            return this;
        }

        RunSubmitRequestBody setNotebookBaseParameters(JsonNode baseParameters) {
            ((ObjectNode) notebookTask).set("base_parameters", baseParameters);
            return this;
        }

        Map<String, Object> create() {
            JsonNode awsAttributes = mapper.createObjectNode();
            ((ObjectNode) awsAttributes).put("availability", "ON_DEMAND");
            ((ObjectNode) awsAttributes).put("ebs_volume_count", 1);
            ((ObjectNode) awsAttributes).put("ebs_volume_type", "THROUGHPUT_OPTIMIZED_HDD");
            ((ObjectNode) awsAttributes).put("ebs_volume_size", 500);

            ((ObjectNode) newCluster).set("aws_attributes", awsAttributes);
            ((ObjectNode) requestBody).set("new_cluster", newCluster);
            ((ObjectNode) requestBody).set("notebook_task", notebookTask);
            
            Map<String, Object> result = mapper.convertValue(requestBody, Map.class);
            return result;

        }
    }
}
```
---
title: "Java_jax_client"
date: 2018-12-07T09:00:01-05:00
draft: true
---

This is the note on the work to create a web service client to download fattail report.

Begin from the fattail documentation: http://adbook03.fattail.com/abn/ws/AdBookConnect.svc

WSDL locations are included here:

You have created a service.

To test this service, you will need to create a client and use it to call the service. You can do this using the svcutil.exe tool from the command line with the following syntax:

svcutil.exe http://adbook03.fattail.com/ABN/ws/adbookconnect.svc?wsdl
You can also access the service description as a single file:

http://adbook03.fattail.com/ABN/ws/adbookconnect.svc?singleWsdl
This will generate a configuration file and a code file that contains the client class. Add the two files to your client application and use the generated client class to call the Service. For example:


Using the java tool wsimport tool to generate Java source files and compile those source files to byte code.
The wsimport tool is used to parse an existing Web Services Description Language (WSDL) file and generate required files (JAX-WS portable artifacts) for web service client to access the published web services. This wsimport tool is available in the $JDK/bin folder.

(this does not work well: wsimport -d . -p com.tgam.thirdparty.fattail -keep http://adbook01.fattail.com/ABN/ws/adbookconnect.svc?wsdl)

cd /Users/sgao/uExe/fattail_20210109
wsimport -keep -verbose http://adbook03.fattail.com/abn/ws/AdBookConnect.svc?singleWsdl
jar cvf fattail-20210109.jar .

javadoc -protected -sourcepath /Users/sgao/uExe/fattail_20210109/ com.fattail.api -d /Users/sgao/uExe/fattail/docs20210109 /Users/sgao/uExe/fattail_20210109/com/fattail/api/*.java

javadoc -protected -sourcepath /Users/dshadab/wsdl com.fattail.api -d /Users/dshadab/fattail_docs20210109 /Users/dshadab/wsdl/com/fattail/api/*.java


Examine the Java source code:
AdBookConnect.java:

```java
@WebServiceClient(name = "AdBookConnect", targetNamespace = "http://www.FatTail.com/api", wsdlLocation = "http://adbook01.fattail.com/ABN/ws/adbookconnect.svc?wsdl")
public class AdBookConnect
    extends Service
```
AdBookConnect is the Service Client Creator Class.

IAdBookConnect.java:
```java
@WebService(name = "IAdBookConnect", targetNamespace = "http://www.FatTail.com/api")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface IAdBookConnect 
```
IAdBookConnect is the Service Interface that calls all the methods.




Package the files into a jar
jar cvf fattail-v23-20181240.jar  -C *

To create a javadoc
javadoc -protected -sourcepath /Users/sgao/uExe/fattail/ com.tgam.thirdparty.fattail -d /Users/sgao/uExe/fattail/docs  /Users/sgao/uExe/fattail/com/tgam/thirdparty/fattail/*.java


Start to development:

Set up on NetBeans IDE:
process-classes org.codehaus.mojo:exec-maven-plugin:1.3.2:exec


exec.args=-classpath %classpath:.:${project.build.directory}/lib/* ${packageClassName}
exec.executable=java
Env.AWS_DEFAULT_REGION=us-east-1

The main code:
```java
import com.tgam.thirdparty.fattail.AdBookConnect;
import com.tgam.thirdparty.fattail.AdUnit;
import com.tgam.thirdparty.fattail.ArrayOfAdUnit;
import com.tgam.thirdparty.fattail.IAdBookConnect;
import com.tgam.thirdparty.fattail.IAdBookConnectGetAdUnitListDataAccessFaultFaultFaultMessage;
import java.net.MalformedURLException;
import java.util.List;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.ws.BindingProvider;

public class FattailReport {

    public static void main(String[] args) {

        try {
            URL wsdlLocation;
            wsdlLocation = new URL("http://adbook03.fattail.com/ABN/ws/adbookconnect.svc?singleWsdl");
            AdBookConnect adBookConnect = new AdBookConnect(wsdlLocation);

            //The HandlerResolver below contains a HandlerChain which conains only one Handler.
            //The function of the Handler is to add creadentials to the SOAP outbound message header, since 
            //by default the generated SOAP message has an empty header.
            adBookConnect.setHandlerResolver(new FattailSOAPHeaderHandlerResolver());
            IAdBookConnect iAdBookConnect = adBookConnect.getBasicHttpBindingIAdBookConnect();

          //The code blowed is to add the credential to the HTTP header which is not useful in this case.
          //Therefore, the code are commentted out.
//        BindingProvider prov = (BindingProvider) iAdBookConnect;
//        prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "DRoss@globeandmail.com");
//        prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "tMkN2aqUTp3ZirdZ");

            ArrayOfAdUnit arrayOfAdUnit = iAdBookConnect.getAdUnitList();
            List<AdUnit> adunits = arrayOfAdUnit.getAdUnit();
            adunits.stream().forEach((r) -> System.out.println(r));
            System.out.println("hello world");
        } catch (IAdBookConnectGetAdUnitListDataAccessFaultFaultFaultMessage | MalformedURLException ex) {
            Logger.getLogger(FattailReport.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
```

HandlerResolver code
```java
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

/**
 *
 * @author sgao
 */
public class FattailSOAPHeaderHandlerResolver implements HandlerResolver {

    @Override
    public List<Handler> getHandlerChain(PortInfo portInfo) {
        List<Handler> handlerChain = new ArrayList<>();
        FattailSOAPHeaderHandler hh = new FattailSOAPHeaderHandler();
        handlerChain.add(hh);
        return handlerChain;
    }

}
```

```java
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author sgao
 */
public class FattailSOAPHeaderHandler implements SOAPHandler<SOAPMessageContext> {

    //handleMessage is called for both inbound and outbound message.
    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        Boolean isRequest = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        //if this is a request, true for outbound messages, false for inbound
        if (isRequest) {

            try {
                SOAPMessage soapMsg = context.getMessage();
                SOAPEnvelope soapEnv = soapMsg.getSOAPPart().getEnvelope();
                SOAPHeader soapHeader = soapEnv.getHeader();

                //if no header, add one
                if (soapHeader == null) {
                    soapHeader = soapEnv.addHeader();
                }
                /*
                <s:header>
                    <wsse:security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">                           
                        <wsse:usernametoken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"> 
                            <wsse:username>TestUser</wsse:username>
                            <wsse:password type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">TestPassword</wsse:password>
                        </wsse:usernametoken>
                    </wsse:security>
                </s:header>
                 */
                //The following code is to create header elements described in the comment above.
                SOAPElement security
                        = soapHeader.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

                SOAPElement usernameToken
                        = security.addChildElement("UsernameToken", "wsse");
                usernameToken.addAttribute(new QName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
                security.setAttribute("S:mustUnderstand", "0");

                SOAPElement username
                        = usernameToken.addChildElement("Username", "wsse");
                username.addTextNode("<The user login id, same as the GUI login id>");

                SOAPElement password
                        = usernameToken.addChildElement("Password", "wsse");
                password.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
                password.addTextNode("<The user password, same as the GUI login password>");

                //Print out the outbound SOAP message to System.out
                soapMsg.writeTo(System.out);

            } catch (SOAPException | IOException e) {
                System.err.println(e);
            }
        }
        //continue other handler chain
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        System.out.println("Client : handleFault()......");
        return true;
    }

    @Override
    public void close(MessageContext context) {
        System.out.println("Client : close()......");
    }

    /**
     * Gets the header blocks that can be processed by this Handler instance.
     * Returns:
     * Set of QNames of header blocks processed by this handler instance. QName is the qualified name of the outermost element of the Header block.
     **/
    //getHeaders is only called once.
    //The following code is just to say, this handler can handle the "wsse:Security" element, which is tagged as mustUnderstand="1" in the 
    //SOAP response header.  Without the following code, the program exits with exception saying "Security" can not be understood.
    @Override
    public Set<QName> getHeaders() {
        final QName securityHeader = new QName(
                "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
                "Security",
                "wsse");

        final Set<QName> headers = new HashSet<>();
        headers.add(securityHeader);

        // notify the runtime that this is handled
        return headers;
    }
}

```

The following command will dump out the inbound and outbound http headers and loads(SOAP xml message). 
```
java -Dcom.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump=true  -Dcom.sun.xml.internal.ws.transport.http.HttpAdapter.dump=true -cp /Users/sgao/Netbeans/web_client/target/lib/*:/Users/sgao/Netbeans/web_client/target/web_client-1.0-SNAPSHOT.jar com.tgam.web_client.FattailReport
```
---
title: "Java_jaxb_map"
date: 2019-08-16T09:26:52-05:00
draft: true
---

#Solution 1

```java
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement
public class Test {

	@XmlTransient
    private Map<String, Double> mapProperty;

    public Test() {
        mapProperty = new HashMap<String, Double>();
    }

    @XmlJavaTypeAdapter(MapAdapter.class)
    @XmlPath(".")
    public Map<String, Double> getMapProperty() {
        return mapProperty;
    }

    public void setMapProperty(Map<String, Double> map) {
        this.mapProperty = map;
    }

}
```

```java
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MapAdapter extends XmlAdapter<AdaptedMap, Map<String, Double>> {

    @Override
    public AdaptedMap marshal(Map<String, Double> map) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();
        Element rootElement = document.createElement("map");
        document.appendChild(rootElement);

        for(Entry<String,Double> entry : map.entrySet()) {
            Element mapElement = document.createElement(entry.getKey());
            mapElement.setTextContent(entry.getValue().toString());
            rootElement.appendChild(mapElement);
        }

        AdaptedMap adaptedMap = new AdaptedMap();
        adaptedMap.setValue(document);
        return adaptedMap;
    }

    @Override
    public Map<String, Double> unmarshal(AdaptedMap adaptedMap) throws Exception {
        Map<String, Double> map = new HashMap<String, Double>();
        Element rootElement = (Element) adaptedMap.getValue();
        NodeList childNodes = rootElement.getChildNodes();
        for(int x=0,size=childNodes.getLength(); x<size; x++) {
            Node childNode = childNodes.item(x);
            if(childNode.getNodeType() == Node.ELEMENT_NODE) {
                map.put(childNode.getLocalName(), Double.valueOf(childNode.getTextContent()));
            }
        }
        return map;
    }

}
```

```java
import javax.xml.bind.annotation.XmlAnyElement;

public class AdaptedMap {
	@XmlAnyElement
    private Object value;

    
    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
```
```java
public class App 
{
    public static void main( String[] args ) throws JAXBException
    {
        System.out.println( "Hello World1!" );
        
        Map<String, Double> t4OthInfo = new HashMap<>();
        t4OthInfo.put("brbr_hrdrssr_amt", 3333333.33);
        Test test = new Test();
        test.setMapProperty(t4OthInfo);
        
		JAXBContext jaxbContext = JAXBContextFactory.createContext(new Class[] {T4Slip.class, Test.class}, null);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

//		jaxbMarshaller.marshal(t4Sumamry, file);
		jaxbMarshaller.marshal(test, System.out);

        
    }
}
```

```xml
Hello World1!
<?xml version="1.0" encoding="UTF-8"?>
<test>
   <mapProperty>
      <map>
         <brbr_hrdrssr_amt>3333333.33</brbr_hrdrssr_amt>
      </map>
   </mapProperty>
</test>
```


#Solution 2
```java
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement
public class Test {

	@XmlTransient
    private Map<String, Double> mapProperty;

    public Test() {
        mapProperty = new HashMap<String, Double>();
    }

    @XmlJavaTypeAdapter(MapAdapter2.class)
    public Map<String, Double> getMapProperty() {
        return mapProperty;
    }

    public void setMapProperty(Map<String, Double> map) {
        this.mapProperty = map;
    }

}
```

```java
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.naming.OperationNotSupportedException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MapAdapter2 extends XmlAdapter<MapWrapper, Map<String, Object>> {
    @Override
    public MapWrapper marshal(Map<String, Object> m) throws Exception {
        MapWrapper wrapper = new MapWrapper();
        List elements = new ArrayList();
        for (Map.Entry<String, Object> property : m.entrySet()) {

            if (property.getValue() instanceof Map)
                elements.add(new JAXBElement<MapWrapper>(new QName(getCleanLabel(property.getKey())),
                        MapWrapper.class, marshal((Map) property.getValue())));
            else
                elements.add(new JAXBElement<String>(new QName(getCleanLabel(property.getKey())),
                        String.class, property.getValue().toString()));
        }
        wrapper.elements = elements;
        return wrapper;
    }

    @Override
    public Map<String, Object> unmarshal(MapWrapper v) throws Exception {
        // TODO
        throw new OperationNotSupportedException();
    }

    // Return a XML-safe attribute.  Might want to add camel case support
    private String getCleanLabel(String attributeLabel) {
        attributeLabel = attributeLabel.replaceAll("[()]", "").replaceAll("[^\\w\\s]", "_").replaceAll(" ", "_");
        return attributeLabel;
    }
}
```

```java
import java.util.List;

import javax.xml.bind.annotation.XmlAnyElement;

public class MapWrapper {
    @XmlAnyElement
    List elements;
}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<test>
   <mapProperty>
      <brbr_hrdrssr_amt>3333333.33</brbr_hrdrssr_amt>
   </mapProperty>
</test>
```
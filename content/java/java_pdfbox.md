---
title: "Java_PDFBox"
date: 2019-08-13T15:32:23-05:00
draft: true
---

```java

package com.tgam.test.pdftest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
   	PDDocument document = new PDDocument();
   	document.save("/tmp/pdftest/test1.pdf");
       for (int i=0; i<10; i++) {
           //Creating a blank page 
           PDPage blankPage = new PDPage();

           //Adding the blank page to the document
           document.addPage( blankPage );
        } 
   	document.close();


//        File t4f = new File("/Users/sgao/timesheet_201807.pdf");
//        File pdfFile = new File("/Users/sgao/Documents/t4-fill-18e.pdf");
        File pdfFile = new File("/tmp/pdftest/newT4.pdf");
        PDDocument pdfDoc = PDDocument.load(pdfFile);
        pdfDoc.setAllSecurityToBeRemoved(true);
        
        
     // look at all the document information
        PDDocumentInformation info = pdfDoc.getDocumentInformation();
        


        PDDocumentCatalog docCatalog = pdfDoc.getDocumentCatalog();
        System.out.println(docCatalog);
        PDAcroForm acroForm = docCatalog.getAcroForm();
        //This will get all the field in the AcroForm
        //The FullyQualifiedName is the string value of the key /T in the PDF 
        // getFields -- get the document top level field only
       Iterator<PDField> pdFields = acroForm.getFieldIterator();
       int i = 0;
       while(pdFields.hasNext()) {
       	PDField field = pdFields.next();
       	System.out.println(field.getFullyQualifiedName());
       	System.out.println(i++);
       }
        //This is to extract value and set value
        PDField field = acroForm.getField("form1[0].Page1[0].Slip1[0].Box14[0].Slip1Box14[0]");
        System.out.println(field.getValueAsString());
        field.setValue("12345");
        System.out.println(field.getValueAsString());
        

        System.out.println(acroForm);
        pdfDoc.close();
        
    }
}

```
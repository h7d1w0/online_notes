---
title: "Java_webservice"
date: 2018-12-03T13:11:13-05:00
draft: true
---


<From https://www.ibm.com/developerworks/library/ws-whichwsdl/>
# Binding styles


A WSDL document describes a web service. A WSDL binding describes how the service is bound to a messaging protocol, particularly the SOAP messaging protocol. A WSDL SOAP binding can be either a Remote Procedure Call (RPC) style binding or a document style binding. A SOAP binding can also have an encoded use or a literal use.
RPC/document style merely dictates how to translate a WSDL binding to a SOAP message and the terms encoded and literal are only meaningful for the WSDL-to-SOAP mapping.

Five binding styles:
RPC/encoded
RPC/literal
Document/encoded: Nobody follows this style. It is not WS-I compliant.
Document/literal
Document/literal wrapped pattern 
The difference among the five styles resides in the WSDL and SOAP message.


# How to choose a style
Document/literal wrapped pattern is very good, but lacks formal specification that defines this style.  In addition, If you have overloaded operations, you cannot use the document/literal wrapped style.

```
public void myMethod(int x, float y);
<strong>public void myMethod(int x);</strong>
```

WSDL allows overloaded operations. But when you add the wrapped pattern to WSDL, you require an element to have the same name as the operation, and you cannot have two elements with the same name in XML. So you must use the document/literal, non-wrapped style or one of the RPC styles.

Since the document/literal, non-wrapped style doesn't provide the operation name, there are cases where you'll need to use one of the RPC styles. 

```
public void myMethod(int x, float y);
public void myMethod(int x);
<strong>public void someOtherMethod(int x, float y);</strong>
```

When server receives the SOAP message below, 
<soap:envelope>
    <soap:body>
        <xElement>5</xElement>
        <yElement>5.0</yElement>
    </soap:body>
</soap:envelope>
All you know for sure is that it's not myMethod(int x) because the message has two parameters and this method requires one. It could be either of the other two methods. With the document/literal style, you have no way to know which one.
RPC/literal contains the method name in the SOAP, so with this message it's fairly easy for a server to decide which method to dispatch to. 


Reasons to use RPC/encoded
The primary reason to prefer the RPC/encoded style is for data graphs.  The standard way to send data graphs is to use the href tag, which is part of the RPC/encoded style.

The RPC/encoded binary tree
<A>
    <name>A</name>
    <left href="12345"/>
    <right href="12345"/>
</A>
<B id="12345">
    <name>B</name>
    <left xsi:nil="true"/>
    <right xsi:nil="true"/>
</B>


The literal binary tree
<A>
    <name>A</name>
    <left>
        <name>B</name>
        <left xsi:nil="true"/>
        <right xsi:nil="true"/>
    </left>
    <right>
        <name>B</name>
        <left xsi:nil="true"/>
        <right xsi:nil="true"/>
    </right>
</A>
There will be two Bs.


# Style detail
RPC/encoded vs. RPC/literal
WSDL is almost the same. The difference is that the use in the binding is encoded vs. literal.
The operation name appears in the message.

In SOAP message, The type encoding info is eliminated for RPC/literal.
RPC/literal is WS-I compliant, while although WSDL of RPC/encoded is legal WSDL, RPC/encoded is not WS-I compliant.

Document/literal
WSDL contains <types>.
There is no type encoding info in SOAP message.  SOAP message body can be verified, but with the operation name eliminated.  WS-I only allows one child of the soap:body in a SOAP message. In Document/literal soap:body could have more than one children.


Document/literal wrapped pattern
SOAP message looks remarkably like the RPC/literal SOAP message, but there's a subtle difference. In the RPC/literal SOAP message, the <myMethod> child of <soap:body> was the name of the operation. In the document/literal wrapped SOAP message, the <myMethod> clause is the name of the wrapper element which the single input message's part refers to. It just so happens that one of the characteristics of the wrapped pattern is that the name of the input element is the same as the name of the operation. This pattern is a sly way of putting the operation name back into the SOAP message.
These are the WSDL basic characteristics of the document/literal wrapped pattern:
The input message has a single part.
The part is an element.
The element has the same name as the operation.
The element's complex type has no attributes.

If you have overloaded operations, you cannot use the document/literal wrapped style.
WSDL allows overloaded operations. But when you add the wrapped pattern to WSDL, you require an element to have the same name as the operation, and you cannot have two elements with the same name in XML. So you must use the document/literal, non-wrapped style(with no operation name, but the number of parts can differentiate which method to depatch to) or one of the RPC styles.


In SOAP message: 
There is no type encoding info.
Everything that appears in the soap:body is defined by the schema, so you can easily validate this message.
Once again, you have the method name in the SOAP message.
Document/literal is WS-I compliant, and the wrapped pattern meets the WS-I restriction that the SOAP message's soap:body has only one child.


## RPC/encoded WSDL and SOAP message for myMethod
<message name="myMethodRequest">
    <part name="x" type="xsd:int"/>
    <part name="y" type="xsd:float"/>
</message>
<message name="empty"/>
 
<portType name="PT">
    <operation name="myMethod">
        <input message="myMethodRequest"/>
        <output message="empty"/>
    </operation>
</portType>

<binding .../>  


<soap:envelope>
    <soap:body>
        <myMethod>
            <x xsi:type="xsd:int">5</x>
            <y xsi:type="xsd:float">5.0</y>
        </myMethod>
    </soap:body>
</soap:envelope>


## RPC/literal WSDL for myMethod
<message name="myMethodRequest">
    <part name="x" type="xsd:int"/>
    <part name="y" type="xsd:float"/>
</message>
<message name="empty"/>
 
<portType name="PT">
    <operation name="myMethod">
        <input message="myMethodRequest"/>
        <output message="empty"/>
    </operation>
</portType>
 
<binding .../>  
<!-- I won't bother with the details, just assume it's RPC/<strong>literal</strong>. -->


<soap:envelope>
    <soap:body>
        <myMethod>
            <x>5</x>
            <y>5.0</y>
        </myMethod>
    </soap:body>
</soap:envelope>


## Document/literal WSDL for myMethod
<strong><types>
    <schema>
        <element name="xElement" type="xsd:int"/>
        <element name="yElement" type="xsd:float"/>
    </schema>
</types></strong>
 
<message name="myMethodRequest">
    <part name="x" <strong>element="xElement"</strong>/>
    <part name="y" <strong>element="yElement"</strong>/>
</message>
<message name="empty"/>
 
<portType name="PT">
    <operation name="myMethod">
        <input message="myMethodRequest"/>
        <output message="empty"/>
    </operation>
</portType>
 
<binding .../>  


<soap:envelope>
    <soap:body>
        <xElement>5</xElement>
        <yElement>5.0</yElement>
    </soap:body>
</soap:envelope>


## Document/literal wrapped
<types>
    <schema>
        <strong><element name="myMethod">
            <complexType>
                <sequence>
                    <element name="x" type="xsd:int"/>
                    <element name="y" type="xsd:float"/>
                </sequence>
            </complexType>
        </element>
        <element name="myMethodResponse">
            <complexType/>
        </element></strong>
    </schema>
</types>
<message name="myMethodRequest">
    <part name="<strong>parameters</strong>" element="<strong>myMethod</strong>"/>
</message>
<strong><message name="empty">
    <part name="parameters" element="myMethodResponse"/>
</message></strong>
 
<portType name="PT">
    <operation name="myMethod">
        <input message="myMethodRequest"/>
        <output message="empty"/>
    </operation>
</portType>
 
<binding .../>  


<soap:envelope>
    <soap:body>
        <myMethod>
            <x>5</x>
            <y>5.0</y>
        </myMethod>
    </soap:body>
</soap:envelope>




----------------------
There are two types of web services:

SOAP stands for Simple Object Access Protocol. SOAP is an XML based industry standard protocol for designing and developing web services. Since it’s XML based, it’s platform and language independent. So our server can be based on JAVA and client can be on .NET, PHP etc. and vice versa.

WSDL stands for Web Service Description Language. WSDL is an XML based document that provides technical details about the web service. Some of the useful information in WSDL document are: method name, port types, service end point, binding, method parameters etc.

UDDI is acronym for Universal Description, Discovery and Integration. UDDI is a directory of web services where client applications can lookup for web services. Web Services can register to the UDDI server and make them available to client applications.

JAX-WS stands for Java API for XML Web Services. JAX-WS is XML based Java API to build web services server and client application. It’s part of standard Java API, so we don’t need to include anything else which working with it.


SOAP Web Services
Restful Web Services

A WSDL document defines services as collections of network endpoints, or ports. In WSDL, the abstract definition of endpoints and messages is separated from their concrete network deployment or data format bindings. This allows the reuse of abstract definitions: messages, which are abstract descriptions of the data being exchanged, and port types which are abstract collections of operations. The concrete protocol and data format specifications for a particular port type constitutes a reusable binding. A port is defined by associating a network address with a reusable binding, and a collection of ports define a service. Hence, a WSDL document uses the following elements in the definition of network services:

Types– a container for data type definitions using some type system (such as XSD).
Message– an abstract, typed definition of the data being communicated.
Operation– an abstract description of an action supported by the service.
Port Type–an abstract set of operations supported by one or more endpoints.
Binding– a concrete protocol and data format specification for a particular port type.
Port– a single endpoint defined as a combination of a binding and a network address.
Service– a collection of related endpoints.


The endpoint is a connection point where HTML files or active server pages are exposed. Endpoints provide information needed to address a Web service endpoint. The endpoint provides a reference or specification that is used to define a group or family of message addressing properties and give end-to-end message characteristics, such as references for the source and destination of endpoints, and the identity of messages to allow for uniform addressing of "independent" messages. The endpoint can be a PC, PDA, or point-of-sale terminal.

This is de "old terminology", use directally the WSDL2 "endepoint" definition (WSDL2 translated "port" to "endpoint").:w

--------
There are two communication style models that are used to translate a WSDL binding to a SOAP message body. They are: Document & RPC

Furthermore, there are two encoding use models that are used to translate a WSDL binding to a SOAP message. They are: literal, and encoded

When using a literal use model, the body contents should conform to a user-defined XML-schema(XSD) structure. The advantage is two-fold. For one, you can validate the message body with the user-defined XML-schema, moreover, you can also transform the message using a transformation language like XSLT.

With a (SOAP) encoded use model, the message has to use XSD datatypes, but the structure of the message need not conform to any user-defined XML schema. This makes it difficult to validate the message body or use XSLT based transformations on the message body.

The combination of the different style and use models give us four different ways to translate a WSDL binding to a SOAP message.

Document/literal
Document/encoded
RPC/literal
RPC/encoded


Once the artifacts are received, in both styles of communication, I invoke the method on the port. Now, this does not differ in RPC style and Document style. So what is the difference and where is that difference visible?

Conclusion

As you would have noticed in the two SOAP response messages that it is possible to validate the SOAP response message in case of DOCUMENT style but not in RPC style web services.
The basic disadvantage of using RPC style is that it doesn’t support richer data types and that of using Document style is that it brings some complexity in the form of XSD for defining the richer data types.
The choice of using one out of these depends upon the operation/method requirements and the expected clients.

-----------

Similarly, in what way SOAP over HTTP differ from XML over HTTP? After all SOAP is also XML document with SOAP namespace. So what is the difference here?
Why do we need a standard like SOAP? By exchanging XML documents over HTTP, two programs can exchange rich, structured information without the introduction of an additional standard such as SOAP to explicitly describe a message envelope format and a way to encode structured content.

SOAP provides a standard so that developers do not have to invent a custom XML message format for every service they want to make available. Given the signature of the service method to be invoked, the SOAP specification prescribes an unambiguous XML message format. Any developer familiar with the SOAP specification, working in any programming language, can formulate a correct SOAP XML request for a particular service and understand the response from the service by obtaining the following service details.

Service name
Method names implemented by the service
Method signature of each method
Address of the service implementation (expressed as a URI)
Using SOAP streamlines the process for exposing an existing software component as a Web service since the method signature of the service identifies the XML document structure used for both the request and the response.

----------

In WSDL definition, bindings contain operations, here comes style for each operation.

Document : In WSDL file, it specifies types details either having inline Or imports XSD document, which describes the structure(i.e. schema) of the complex data types being exchanged by those service methods which makes loosely coupled. Document style is default.

Advantage:
Using this Document style, we can validate SOAP messages against predefined schema. It supports xml datatypes and patterns.
loosely coupled.
Disadvantage: It is a little bit hard to get understand.
In WSDL types element looks as follows:

<types>
 <xsd:schema>
  <xsd:import schemaLocation="http://localhost:9999/ws/hello?xsd=1" namespace="http://ws.peter.com/"/>
 </xsd:schema>
</types>

The schema is importing from external reference.

RPC :In WSDL file, it does not creates types schema, within message elements it defines name and type attributes which makes tightly coupled.

<types/>  
<message name="getHelloWorldAsString">  
<part name="arg0" type="xsd:string"/>  
</message>  
<message name="getHelloWorldAsStringResponse">  
<part name="return" type="xsd:string"/>  
</message>  
Advantage: Easy to understand.
Disadvantage:
we can not validate SOAP messages.
tightly coupled
RPC : No types in WSDL
Document: Types section would be available in WSDL

-----------
28
down vote
A web service endpoint is the URL that another program would use to communicate with your program. To see the WSDL you add ?wsdl to the web service endpoint URL.

Web services are for program-to-program interaction, while web pages are for program-to-human interaction.

So: Endpoint is: http://www.blah.com/myproject/webservice/webmethod

Therefore, WSDL is: http://www.blah.com/myproject/webservice/webmethod?wsdl

To expand further on the elements of a WSDL, I always find it helpful to compare them to code:

A WSDL has 2 portions (physical & abstract).

Physical Portion:

Definitions - variables - ex: myVar, x, y, etc.

Types - data types - ex: int, double, String, myObjectType

Operations - methods/functions - ex: myMethod(), myFunction(), etc.

Messages - method/function input parameters & return types

ex: public myObjectType myMethod(String myVar)
Porttypes - classes (i.e. they are a container for operations) - ex: MyClass{}, etc.

Abstract Portion:

Binding - these connect to the porttypes and define the chosen protocol for communicating with this web service. - a protocol is a form of communication (so text/SMS, vs. phone vs. email, etc.).

Service - this lists the address where another program can find your web service (i.e. your endpoint).

share

To trace the soap request:
java -Dcom.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump=true  -Dcom.sun.xml.internal.ws.transport.http.HttpAdapter.dump=true wsclient/HelloClient




 left out the details of the binding tags in his examples for RPC-literal and RPC-encoded.
Unfortunately, that is where most of the differences lie.
At any rate, this is a good opportunity to get Eclipse and create some sample WSDL files, so you can see the differences for yourself.


Java JAX

wsimport -d . -p wsclient -keep http://localhost:1212/hello?wsdl
wsimport -d . -p com.tgam.thirdparty.fattail -keep http://adbook03.fattail.com/ABN/ws/adbookconnect.svc?wsdl

The -p arg tells the tool to put the generated classes into a specific package. 
Executing this command will result in generating two classes. The first class:
Service Interface: which is annoted with @WebService and many methods annoted @WebMethod, this is the interface to call the operations.
Service Client Creator Class: annoted with @WebServiceClient and extends javax.xml.ws.Service, this is the place to generate the instance of Service Interface.


--------------
tomcat
download and unzip
startup.sh
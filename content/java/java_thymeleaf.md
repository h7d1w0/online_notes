---
title: "Java_thymeleaf"
date: 2019-02-25T10:51:59-04:00
draft: true
---

The framework of java.time comes with Java 8.  It makes date/time manipulation much easier.  This post gives some code examples on how developer use java.time to perform related date/time related tasks.
<!--more-->

# Concept

Thymeleaf is a Java XML/XHTML/HTML5 Template Engine that can work both in web (Servlet-based) and non-web environments. It is better suited for serving XHTML/HTML5 at the view layer of MVC-based web applications, but it can process any XML file even in offline environments. It provides full Spring Framework integration.

The template file of Thymeleaf in essence are only a ordinary document file with the format of  XML/XHTML/HTML5. Thymeleaf Engine will read a template file and combine with Java objects to generate another document.


TemplateResolver is to tell TemplateEngine where and how to find the templates.


```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.mnc</groupId>
	<artifactId>thymeleaftest</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>thymeleaftest</name>
	<url>http://maven.apache.org</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>3.8.1</version>
			<scope>test</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.thymeleaf/thymeleaf -->
		<dependency>
			<groupId>org.thymeleaf</groupId>
			<artifactId>thymeleaf</artifactId>
			<version>3.0.11.RELEASE</version>
		</dependency>
	</dependencies>
</project>
```


```java
package com.mnc.thymeleaftest;


import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;
import java.io.StringWriter;

public class App 
{
    public static void main( String[] args )
    {
    	TemplateEngine templateEngine = new TemplateEngine();
    	StringTemplateResolver templateResolver = new StringTemplateResolver();
    	templateResolver.setTemplateMode("HTML");
    	templateEngine.setTemplateResolver(templateResolver);
    	Context context = new Context();
    	context.setVariable("var", "Hello World");
    	StringWriter stringWriter = new StringWriter();
        templateEngine.process("<!DOCTYPE html><html xmlns:th=\"http://www.thymeleaf.org\"> <body> <p th:text=\"${var}\">Welcome to our grocery store!</p> </body> </html>", context, stringWriter);
        System.out.println(stringWriter.toString());
    	System.out.println( "Hello World!" );
        
    }
}
```



```java
package com.mnc.thymeleaftest;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;

public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	TemplateEngine templateEngine = new TemplateEngine();
    	FileTemplateResolver templateResolver = new FileTemplateResolver();
    	templateResolver.setTemplateMode("html");
    	templateResolver.setPrefix("/Users/sgao/Documents/ThymeleafTest/static/");
    	templateResolver.setSuffix(".html");
    	templateEngine.setTemplateResolver(templateResolver);
    	Context context = new Context();
    	context.setVariable("var", "Hello World");
    	String fileName = "home";
    	FileWriter out = new FileWriter(new File("/Users/sgao/Documents/ThymeleafTest/dynamic/" + fileName + ".html"));
        templateEngine.process(fileName, context, out);
        StringWriter stringWriter = new StringWriter();
        templateEngine.process(fileName, context, stringWriter);
        System.out.println(stringWriter.toString());
    	System.out.println( "Hello World!" );
        
    }
}
```
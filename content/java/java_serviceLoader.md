---
title: "Java ServiceLoader"
date: 2018-10-24T14:32:55-04:00
draft: false

categories: ["programming"]
tags: ["ServiceLoader", "IoC"]
---

* Notes
  + Service configuration file should be under ../src/main/resouces/META-INFO/services
  + Plug-ins can be abscent at compile time
  + Both API and plug-ins should be one the classpath at run time 

Java Service Loader is a specific form of Inversion of Control offered by Java out of the box. It is designed to locate implementation classes of an interface on the classpath. This way allows to discover which available implementations of an interface are available on the classpath at runtime, and thus paves the way for modules designed around a clean separation between an API module — i.e. JAR, and multiple implementation modules.

Inversion of Control offered by Spring is a little more flexible.  However, Spring is a lot of heavier, and if IoC is the only purpose for choosing Spring, Java Service Loader is very good alternative.

<!--More-->

# What is and what are the use cases of Java Service Loader
* Java Service Loader was first introduced by Java 6.
* It aims to automatically discover “plugin” service implementations for JARs within the classpath, which makes user applications extensible too.

# Java Service Loader's components and how it works
* Service: is the API you want plugins to be able to implement — probably a set of interfaces or abstract classes, which provide application certain functionalities or features.
* Service Provider Interface (SPI): An interface or abstract class that acts as a proxy or an endpoint to the service.  It is the entry point of the service and its associated supporting interfaces and classes.  If the service is one interface, then there could be no a separate a service provider interface, and the service itself can servce as the SPI at the same time.  Service and SPI together are well-known in the Java Ecosystem as API.
* Service Provider: is the plugin, which you want to be able to discover & load automatically. It is a specific implementation of the SPI. The Service Provider contains one or more concrete classes that implement or extends the service type.  A Service Provider is configured and identified through a provider configuration file which we put in the resource directory META-INF/services. The file name is the fully-qualified name of the SPI and his content is the fully-qualified name of the SPI implementation.  The Service Provider is installed in the form of extensions, a jar file which we place in the application classpath, the java extension classpath or user-defined classpath.
* ServiceLoader: at the heart of the SPI is the ServiceLoader class. This has the role of discovering and loading implementations lazily. It uses the context classpath to locate providers implementations and put them in an internal cache.
* THe service API and SPI are exposed to the developer and must be available at both the compile and run time.  The availability of the Service Provider is not necessary at the compile phase.  One, more or none Service Provider can exists at run time, but all the situation must be handled properly - e.g. if there are more than one Service Provider, what is the way to choose one or how to use two or three of them.  If there is no Service Provider provided at run time, the application how to handle the situation.

# High level of the design of the example code
In in order to illustrate the concept above, there is the design

* package com.mnci.api
  * FormatManager: which is the service interface, and its only method is format() which takes a String and returns a formatted one.
  * FormatServiceProvider: Service Provider Interface (SPI), and its only method provide() is to create a FormatManager.

* package com.mnci.impl
  * FormatManagerImpl1: an implemetation of FormatManager, overrides format(), and it simply prefix and profix the original string with "impl1".
  * FormatServiceProviderImpl1: an implemetation of FormatServiceProvider, overrides provide(), returns a FormatManagerImpl1 which is one kind of FormatManager.

* package com.mnci
  * User application puts api and plug-in together and shows the usage.

* Notes
  * The package names can be anything, e.g. com.abc for the api and net.bdc.efg for the plug-in and something else for the user application.  In the real world, the package names of the three parts are very likely irrelavent, since there are usually designed and developed by separate groups.

# Code example
Test with Java 8
Maven is used as the build tool, and there will be directory structure followed the java code, which will help the beginners.
## package com.mnci.api
### Table structure
```
This is the source code structure:
.
--- api
|   --- pom.xml
|   --- src
|   |   --- main
|   |   |   --- java
|   |   |   |   --- com
|   |   |   |   |   --- mnci
|   |   |   |   |   |   --- api
|   |   |   |   |   |   |   +-- FormatManager.java
|   |   |   |   |   |   |   +-- FormatServiceProvider.java
|   |   +-- test
```
After run **mvn install** under the directory api:
```
This is the source code structure:
.
--- api
|   --- pom.xml
|   --- src
|   |   --- main
|   |   |   --- java
|   |   |   |   --- com
|   |   |   |   |   --- mnci
|   |   |   |   |   |   --- api
|   |   |   |   |   |   |   +-- FormatManager.java
|   |   |   |   |   |   |   +-- FormatServiceProvider.java
|   |   +-- test
|   --- target
|   |   --- api-1.0-SNAPSHOT
|   |   +-- classes
...
```
### pom.xml
```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.mnci.api</groupId>
  <artifactId>api</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>api</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
</project>
```
### FormatManager.java
```java
package com.mnci.api;

public interface FormatManager{
    public String format(String s);
}
```
### FormatServiceProvider.java
```java
package com.mnci.api;

public interface FormatServiceProvider{
    public FormatManager provide();
}
```
## package com.mnci.impl
### Table structure
After run **mvn package** under the directory api:
```
This is the source code structure:
.
--- provider1
|   --- pom.xml
|   --- src
|   |   --- main
|   |   |   --- java
|   |   |   |   --- com
|   |   |   |   |   --- mnci
|   |   |   |   |   |   --- impl
|   |   |   |   |   |   |   +-- FormatManagerImpl1.java
|   |   |   |   |   |   |   +-- FormatServiceProviderImpl1.java
|   |   |   --- resources
|   |   |   |   --- META-INFO
|   |   |   |   |   --- servides
|   |   |   |   |   |   --- com.mnci.api.FormatServiceProvider
|   |   +-- test
|   --- target
|   |   --- provider1-1.0-SNAPSHOT
|   |   +-- classes
...
```
### pom.xml
```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.mnci.impl</groupId>
  <artifactId>provider1</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>provider1</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>com.mnci.api</groupId>
      <artifactId>api</artifactId>
      <version>1.0-SNAPSHOT</version>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
</project>
```
### ccom.mnci.api.FormatServiceProvider
```text
com.mnci.impl.FormatServiceProviderImpl1
```
### FormatManagerImpl1.java
```java
package com.mnci.impl;

import com.mnci.api.FormatManager;

public class FormatManagerImpl1 implements FormatManager{
    @Override
    public String format(String s){
        return "Impl1 " + s + " Impl1";
    }
}
```
### FormatServiceProviderImpl1.java
```java
package com.mnci.impl;

import com.mnci.api.FormatServiceProvider;
import com.mnci.api.FormatManager;

public class FormatServiceProviderImpl1 implements FormatServiceProvider{
    @Override
    public FormatManager provide(){
        return new FormatManagerImpl1();
    }
}
```
## package com.mnci
### Table structure
After run **mvn package** under the directory api:
```
This is the source code structure:
.
--- show-info
|   --- pom.xml
|   --- src
|   |   --- main
|   |   |   --- java
|   |   |   |   --- com
|   |   |   |   |   --- mnci
|   |   |   |   |   |   +-- App.java
|   |   +-- test
|   --- target
|   |   --- show-info-1.0-SNAPSHOT
|   |   +-- classes
...
```
### pom.xml
```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.mnci</groupId>
  <artifactId>show-info</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>show-info</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>com.mnci.api</groupId>
      <artifactId>api</artifactId>
      <version>1.0-SNAPSHOT</version>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
</project>
```
### App.java
```java
package com.mnci;

import java.util.ServiceLoader;
import java.util.Iterator;
import com.mnci.api.FormatServiceProvider;
import com.mnci.api.FormatManager;

public class App 
{
    public static void main( String[] args )
    {
        ServiceLoader<FormatServiceProvider> loader = ServiceLoader.load(FormatServiceProvider.class);
  
        int i = 0;
        Iterator<FormatServiceProvider> iter = loader.iterator();
        while(iter.hasNext()){
            FormatServiceProvider item = iter.next();
            // System.out.println( item );
            FormatManager fm = item.provide();
            String fmstr = fm.format("Hello, test!");
            System.out.println( fmstr );
            i++;
        }
        System.out.println( "i=" + i );
        System.out.println( "Hello World!" );

    }
}
```
## run app
Replace $SOME_FOLDER1, $SOME_FOLDER2, and $SOME_FOLDER3 with the correct path on your computer and then run the following command in **one line**
```
java -cp $SOME_FOLDER1/api/target/api-1.0-SNAPSHOT.jar:$SOME_FOLDER2/provider1/target/provider1-1.0-SNAPSHOT.jar:$SOME_FOLDER3/show-info/target/show-info-1.0-SNAPSHOT.jar com.mnci.App
```

---
title: "Java stream - collect"
date: 2018-10-09T12:41:23-04:00
draft: false

categories: ["programming"]
tags: ["stream", "collect", "consummer", "supplier", "function", "aggregation operation", "reduction"]
---

Java Stream.collect is more efficient than Stream.reduce in many cases.  However, time and effort are needed to understand how to use it correctly, especially the form of **collect(Supplier<R> supplier, BiConsumer<R,? super T> accumulator, BiConsumer<R,R> combiner)**.  Further more, the official java tutorial contains a little thing that can confuse people.
<!--more-->

# Some Java stream terms
* Java Stream: A stream is a sequence of objects from a source.  A stream is something on the fly.  After it is consumed by some operations, it can't be reused.  If two sets of operations need to be acted on a stream to get the two results, two streams can be created based on the some source (e.g. a Java collection) and then apply each set of operations individually.
* Aggregate operations: the operations that act on a stream.
* Reduction operations: Many terminal operations (such as average, sum, min, max, and count) that are contained in the JDK contains and return one value or return a collection by combining the contents of a stream.
* Steam.collect: Steam.collect is one kind of the Stream reduction operations, and it can be flexiable and efficient. 
  + Flexibility: developers can create any class that has proper fields and appropriate methods.
  + Efficiency: unlike Stream.reduce, Steam.collect modifies an existing object instead of creating new object to keep the calculation going.
* Requirements: The same as Sream.reduce, Steam.collect requires the Steam elements and the operation defined for the elements to be associative, commutative, and distributive.  This is the point when saying "any class that has proper fields and appropriate methods" in the Flexibility above.

# Decipher Java Stream.collect
Before we start, please read the official Java tutorial first.  Here is a copy downloaded in 2018 (Java 8) - 
{{% ln "The_Java_Tutorials_Collections_AggregateOperations.pdf" %}}The Java™ Tutorials - Reduction{{% /ln %}}.  It is referred as "Java Tutorial" in this post.  There is tested code appended to this post and can be used to play around.

The main task is to decipher the code below in "Java Tutorial":
```Java
Averager averageCollect = roster.stream()
 .filter(p -> p.getGender() == Person.Sex.MALE)
 .map(Person::getAge)
 .collect(Averager::new, Averager::accept, Averager::combine);

System.out.println("Average age of male members: " +
 averageCollect.average());
```
On seeing the code above, there are some questions raised up:
* Why is Averager needed?
* What are the arguments that collect takes and how do they comply to the signature of Stream.collect?

## class Averager
Why is Averager needed?  Apparently here the average is the mathematical mean of the ages which is not distributive at least.  However, the sum of the ages and the count of the number of person are associative, commutative, and distributive.  The class Averager is "wrapper" of the "total" and "count".  After all persons' age are sumed up and all persons are counted, we can easily calculate the average of the age.

Averager will be both the final result type and immediate results' type that when the processing going on.  It means the final result of the collect will be an object of the Averager type and all the objects that hold the immediate results will be of the Averager too.  The methods defined in Averager modify the fields (total and count), which improves the efficiency instead of creating a new object every time like the Stream.reduce.

In the "Java Tutorial", Averager implements IntConsumer and override the method accept.  However, it is **NOT** necessary, and it confuses the beginners too.  The next section illustrates the reason.

## Supplier<R> supplier - the first parameter of Stream.collect
Supplier<R> is a functional interface, and R is the type of results supplied by this supplier.  For Stream.collect, R is the type of final and intermediate results when Stream.collect processes a Stream<T>.  R can be same as or different from T.  In the "Java Tutorial", T is int, and R is Averager.

For the case in the "Java Tutorial", the first parameter is Supplier<Averager> supplier, and it means an object of Supplier<Averager> should be provided for the collect call.  There are as least four ways for the developer to provide an object of the Supplier<Averager> type:
```Java
//anonymous class
Supplier<Averager> supplier = new Supplier() {
    @Override
    public Averager get() {
        return new Averager();
    }
};

//Lambda expression
Supplier<Averager> supplier2 = () -> new Averager();

//Create a class than implements Supplier<Averager>
//and then create object
Supplier<Averager> supplier3 = new Supplier3();

class Supplier3 implements Supplier<Averager> {
    @Override
    public Averager get() {
        return new Averager();
    }
}

//method reference
Averager::new
```
There should be no problem with the first three ways.  The last one which is used in the tutorial is method reference.  Explanation on method reference is out of the scope of this post.  Basically, method reference helps to get a lambda expression by refering to a method name.

## BiConsumer<R,? super T> accumulator - the second parameter of Stream.collect
The accumulator is used to take a new element from the stream and add it to the intermediate result.  In the example, it is to take a new age to "add" it to the intermediate result (an object of Averager).  We need to do two things for the "adding" - add the age to the field "total" and increase one to the field "count".

R is Averager and ? super T is int for this case.  Similar to supplier, there are more than one ways to create a accumulator and pass it to the collect operation.  You can also create a new regular class and initiate an object like we did in supplier.
```Java
//Anonymous class
BiConsumer<Averager, Integer> accumulator = new BiConsumer() {
    @Override
    public void accept(Object avg, Object i) {
        ((Averager) avg).take((int) i);
    }
};
//Lambda expression
BiConsumer<Averager, Integer> accumulator2 = (Averager avg, Integer i) -> {
    avg.take(i);
};
//method reference
Averager::accept
```
We put more attention to the method reference - Averager::accept.  The method accept in Averager is not the only option we can use.  If there is any other method either defined by Averager or by another other class, the method will be suitable to the purpose.  The requirements to make the method syntactically suitable to be passed as the accumulator is that it must have two parameters: an Averager and followed by an int.  Here are the two examples:
```Java
static class IrelevantClass {

    public static void accumulate(Averager keeper, int age) {
        keeper.total += age;
        keeper.count += 1;
    }

    public static void combineTwoAveragers(Averager keeper, Averager other) {
        keeper.total += other.total;
        keeper.count += other.count;
    }
}

static class IrelevantClass2 {

    public void accumulate(Averager keeper, int age) {
        keeper.total += age;
        keeper.count += 1;
    }

    public void combineTwoAveragers(Averager keeper, Averager other) {
        keeper.total += other.total;
        keeper.count += other.count;
    }
}
```
Averager::accept has only one parameter: int.  In this case, the specific method references rule makes use of the implicit parameter - an object of the accept method is used.

## BiConsumer<R,R> combiner - the third parameter of Stream.collect
The combiner is used to combine the intermediate results from all the partitions.  In terms of concept and ways to provide a combiner is similar to accumulator. 

# Code
```Java
/*
  The code has been tested with Java 8.
 
 */
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.IntConsumer;
import java.util.function.Supplier;


public class TestJava {

    public static void main(String[] args) {
        Person p1 = new Person("Tom", "MALE", LocalDate.parse("1975-01-09"));
        Person p2 = new Person("Liya", "FEMALE", LocalDate.parse("1995-01-09"));
        List<Person> roster = new ArrayList<>();
        roster.add(p1);
        roster.add(p2);

        //anonymous class
        Supplier<Averager> supplier = new Supplier() {
            @Override
            public Averager get() {
                return new Averager();
            }
        };

        //Lambda expression
        Supplier<Averager> supplier2 = () -> new Averager();

        //Create a class than implements Supplier<Averager>
        //and then create object
        Supplier<Averager> supplier3 = new Supplier3();


        BiConsumer<Averager, Integer> accumulator = new BiConsumer() {
            @Override
            public void accept(Object avg, Object i) {
                ((Averager) avg).take((int) i);
            }
        };

        BiConsumer<Averager, Integer> accumulator2 = (Averager avg, Integer i) -> {
            avg.take(i);
        };

        BiConsumer<Averager, Averager> combiner = new BiConsumer() {
            @Override
            public void accept(Object o1, Object o2) {
                ((Averager) o1).combine((Averager) o2);
            }
        };

        BiConsumer<Averager, Averager> combiner2 = (Averager o1, Averager o2) -> {
            o1.combine(o2);
        };

        Averager averageCollect = roster.stream()
                .filter(p -> p.getGender() == Person.Sex.MALE)
                .map(Person::getAge)
                .collect(Averager::new, Averager::accept, Averager::combine);

        Averager averageCollect1 = roster.stream()
                .map(Person::getAge)
                .collect(supplier2, accumulator2, combiner2);

         Averager averageCollect2 = roster.stream()
                .map(Person::getAge)
                .collect(supplier2, IrelevantClass::accumulate, IrelevantClass::combineTwoAveragers);       
         
        IrelevantClass2 irelevant2 = new IrelevantClass2();
        Averager averageCollect3 = roster.stream()
                .map(Person::getAge)
                .collect(supplier2, irelevant2::accumulate, irelevant2::combineTwoAveragers);
         
        System.out.println("Average age of male members: "
                + averageCollect3.average());

    }

    public static class Person {

        public Person(String name, String sex, LocalDate birthday) {
            this.name = name;
            this.gender = Sex.valueOf(sex);
            this.birthday = birthday;
        }

        public enum Sex {
            MALE, FEMALE
        }

        String name;
        LocalDate birthday;
        Sex gender;
        String emailAddress;

        public Sex getGender() {
            return gender;
        }

        public int getAge() {
            return (int) ChronoUnit.YEARS.between(birthday, LocalDate.now());
        }

        public void printPerson() {
            System.out.println(name);
        }
    }

    static class Averager implements IntConsumer {

        private int total = 0;
        private int count = 0;

        public double average() {
            return count > 0 ? ((double) total) / count : 0;
        }

        public void take(int i) {
            total += i;
            count++;
        }

        @Override
        public void accept(int i) {
            total += i;
            count++;
        }

        public void combine(Averager other) {
            total += other.total;
            count += other.count;
        }
    }
    
    static class IrelevantClass {
        
        public static void accumulate(Averager keeper, int age) {
            keeper.total += age;
            keeper.count += 1;
        }
        
        public static void combineTwoAveragers(Averager keeper, Averager other) {
            keeper.total += other.total;
            keeper.count += other.count;
        }
    }
 
    static class IrelevantClass2 {
        
        public void accumulate(Averager keeper, int age) {
            keeper.total += age;
            keeper.count += 1;
        }
        
        public void combineTwoAveragers(Averager keeper, Averager other) {
            keeper.total += other.total;
            keeper.count += other.count;
        }
    }    

    static class Supplier3 implements Supplier<Averager> {
        @Override
        public Averager get() {
            return new Averager();
        }
    }   
    
}
```
---
title: "Java_time"
date: 2018-10-16T10:51:59-04:00
draft: true
---

The framework of java.time comes with Java 8.  It makes date/time manipulation much easier.  This post gives some code examples on how developer use java.time to perform related date/time related tasks.
<!--more-->

# Concept

* Java datetime is "absolute", and we don't need think of the time-space in the way as does in special or generate relativity.
* The zero point of Java datetime is set to Java epoch of 1970-01-01T00:00:00Z.
* java.time.Instant stores a long representing epoch-seconds and an int representing nanosecond-of-second.
* The epoch-seconds are measured from the standard Java epoch of 1970-01-01T00:00:00Z (the zero point).

# Major datetime package
* java.time
* java.time.chrono
* java.time.format
* java.time.temporal
* java.time.zone

# Major classes
## java.time.Instant


java.time.Period
A date-based amount of time in the ISO-8601 calendar system, such as '2 years, 3 months and 4 days'.

java.time.Duration
A time-based amount of time, such as '34.5 seconds'.


java.time.Instant
the class stores a long representing epoch-seconds and an int representing nanosecond-of-second
The epoch-seconds are measured from the standard Java epoch of 1970-01-01T00:00:00Z
Coordinated Universal Time (abbreviated to UTC) is the primary time standard by which the world regulates clocks and time. It is within about 1 second of mean solar time at 0° longitude, and is not adjusted for daylight saving time. In some countries where English is spoken, the term Greenwich Mean Time (GMT) is often used as a synonym for UTC.(From wikipedia.com)


java.time.OffsetDateTime

java.time.ZonedDateTime


A standard set of date periods units.
java.time.temporal.ChronoUnit

LocalDate dateBefore;
LocalDate dateAfter;
long daysBetween = DAYS.between(dateBefore, dateAfter);


LocalDate today = LocalDate.now()
LocalDate yesterday = today.minusDays(1);
// Duration oneDay = Duration.between(today, yesterday); // throws an exception
Duration.between(today.atStartOfDay(), yesterday.atStartOfDay()).toDays() // another option



java.time
java.time.chrono
java.time.format
java.time.temporal
java.time.zone


```java
import java.time.*;
import java.time.format.DateTimeFormatter;

public class Test5 {
    public static void main(String[] args){
        
        LocalDateTime ldt = LocalDateTime.parse("1970-01-01T00:00:00");
        ZoneId torontoZoneId = ZoneId.of("Canada/Eastern");
        ZonedDateTime torontoDt =  ZonedDateTime.of(ldt, torontoZoneId);
        
        String        reportStartDateTime = "2018-10-01T00:00:00";
        String reportEndDateTime = LocalDateTime.parse(reportStartDateTime).plusDays(1L).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        System.out.println(reportEndDateTime);
    }
}
```


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
//Set<String> zoneIds= ZoneId.getAvailableZoneIds();
//
//for (String zone : zoneIds) {
//    System.out.println(zone);
//}
---
title: "Java_spring_security"
date: 2019-09-13T14:17:23-05:00
draft: true
---


# 
```java
import org.jboss.logging.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	Logger logger = Logger.getLogger(SecurityConfiguration.class);

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().and().antMatcher("/graphql").authorizeRequests().antMatchers(HttpMethod.POST, "/graphql")
				.hasRole("ADMIN").anyRequest().denyAll().and().antMatcher("/**").authorizeRequests().antMatchers("/**")
				.denyAll().and().csrf().disable().formLogin().disable();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(User.withUsername("user").password(passwordEncoder().encode("user")).roles("USER").build());
		manager.createUser(
				User.withUsername("admin").password(passwordEncoder().encode("admin")).roles("USER", "ADMIN").build());
		return manager;
	}

}



```

# Postman
** there may be cache on postman and it causes confusion, the work-around is to change Username every time, even before putting the correct username, first type in a wrong one there the remove the cache


# Spring Security using cookie
## Using the credential and Create a cookie file for the first log in
curl 'http://localhost:4001/graphql' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Connection: keep-alive' -H 'DNT: 1' -H 'Origin: file://' -H 'Authorization: Basic YWRtaW46YWRtaW4=' --data-binary '{"query":"{\n    bookById(id: \"book-1\"){\n        id,\n        name,\n    }\n}"}' --compressed -c xcookies.txt

## Using the cookie for the subsequent logins
curl 'http://localhost:4001/graphql' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'Connection: keep-alive' -H 'DNT: 1' -H 'Origin: file://'  --data-binary '{"query":"{\n    bookById(id: \"book-1\"){\n        id,\n        name,\n    }\n}"}' --compressed -b xcookies.txt


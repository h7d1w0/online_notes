---
title: "Java_connection_pool"
date: 2018-11-14T09:17:45-05:00
draft: true
---
what 
Connection pooling is a well-known data access pattern, whose main purpose is to reduce the overhead involved in performing database connections and read/write database operations.

why
It becomes evident that database connections are fairly expensive operations, and as such, should be reduced to a minimum in every possible use case (in edge cases, just avoided).

Here’s where connection pooling implementations come into play.

By just simply implementing a database connection container, which allows us to reuse a number of existing connections, we can effectively save the cost of performing a huge number of expensive database trips, hence boosting the overall performance of our database-driven applications.



JDBC Connection Pooling Frameworks
Let’s start this quick roundup with Apache Commons DBCP Component, a full-featured connection pooling JDBC framework:

1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
public class DBCPDataSource {
     
    private static BasicDataSource ds = new BasicDataSource();
     
    static {
        ds.setUrl("jdbc:h2:mem:test");
        ds.setUsername("user");
        ds.setPassword("password");
        ds.setMinIdle(5);
        ds.setMaxIdle(10);
        ds.setMaxOpenPreparedStatements(100);
    }
     
    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
     
    private DBCPDataSource(){ }
}
In this case, we’ve used a wrapper class with a static block to easily configure DBCP’s properties.

Here’s how to get a pooled connection with the DBCPDataSource class:

1
Connection con = DBCPDataSource.getConnection();

 HikariCP, a lightning fast JDBC connection pooling framework created by Brett Wooldridge (for the full details on how to configure and get the most out of HikariCP, please check this article):

 C3PO, a powerful JDBC4 connection and statement pooling framework developed by Steve Waldman:

---
title: "Java_spring_JDBC"
date: 2019-07-11T09:17:23-05:00
draft: true
---

# spring-jdbc vs spring-data-jdbc and what are they supporting
spring-jdbc`

The docs for spring-jdbc are basically here:

https://docs.spring.io/spring/docs/current/spring-framework-reference/data-access.html

Though it doesn't specifically point you to the Spring project spring-jdbc. This project just provides all of the Spring abstractions over the plain JDBC DataSource that you can use with the Spring Framework. For example, the Spring DataSources which nicely hook into Spring's Transaction management capabilities, like the @Transactional annotation. Also, the JdbcTemplate is part of this module, which allows you to execute SQL statements and extract objects from Resultsets without dealing with exception handling or the nasty details of properly closing statements, connections and the like.

spring-data-jdbc

spring-data-jdbc, on the other hand, provides the Spring Data abstraction over spring-jdbc. That is, you can create a Spring Data CrudRepository and a simple "entity" (not a JPA entity!) and, as Spring Data does, it will create your queries for you without you having to write native CRUD queries over JDBC, as in this example on the spring-data-examples git repo.

Rather than writing your own like so:

@Repository
public class CategoryRepository {
   public void create(Category category) {
      jdbcTemplate.execute("insert...");
   }

  // The rest of my other CRUD operations
}




Dali, the Eclipse plug-in that supplies the entity generation wizard you are using, relies on another Eclipse plug-in, DTP, to provide the database metadata necessary to perform the entity generation. As a result, this problem could be caused by either plug-in.

A simple way to (possibly) isolate the problem is to use DTP's Data Source Explorer view to see what DTP is returning in the way of metadata. Use this view to connect to your database. (You will be using the same connection profile you created in the Dali Entity Generation wizard.) Once you are connected you should be able to expand the tree to see your tables. If there are no tables, the problem is with the DTP adapter used to retrieve metadata from your database and this is a problem with DTP; if there are tables in the tree, the problem is with Dali.

Also, you should look at the Eclipse log (./.metadata/.log) to see if any sort of exception occurs when you are using the Dali wizard.
---
title: "Java_module"
date: 2019-01-07T15:33:33-04:00
draft: true
---

# Java microframeworks:
Dropwizard
Light-rest-4j (also known as Light Java)
Ninja Web Framework
Play Framework
RESTEasy
Restlet
Spark Framework
Spring Boot

#Microservices

Monolith Application.

Large Application Size
Long Release Cycles
Large Teams
Typical Challenges include

Scalability Challenges
New Technology Adoption
New Processes - Agile?
Difficult to Automation Test
Difficult ot Adapt to Modern Development Practices
Adapting to Device Explosion


Microservices
Microservice Architectures evolved as a solution to the scalability and innovotation challenges with Monolith architectures
Developing a single application as a suite of small services each running in its own process and communicating with lightweight mechanisms, often an HTTP resource API. These services are built around business capabilities and independently deployable by fully automated deployment machinery. There is a bare minimum of centralized management of these services, which may be written in different programming languages and use different data storage technologies - James Lewis and Martin Fowler

While there is no single accepted definition for microservices, for me, there are a few important characteristics:

REST - Built around RESTful Resources. Communication can be HTTP or event based.
Small Well Chosen Deployable Units - Bounded Contexts
Cloud Enabled - Dynamic Scaling


Lets look at some of the challenges:

Quick Setup needed : You cannot spend a month setting up each microservice. You should be able to create microservices quickly.
Automation : Because there are a number of smaller components instead of a monolith, you need to automate everything - Builds, Deployment, Monitoring etc.
Visibility : You now have a number of smaller components to deploy and maintain. Maybe 100 or maybe 1000 components. You should be able to monitor and identify problems automatically. You need great visibility around all the components.
Bounded Context : Deciding the boundaries of a microservice is not an easy task. Bounded Contexts from Domain Driven Design is a good starting point. Your understanding of the domain evolves over a period of time. You need to ensure that the microservice boundaries evolve.
Configuration Management : You need to maintain configurations for hundreds of components across environments. You would need a Configuration Management solution
Dynamic Scale Up and Scale Down : The advantages of microservices will only be realized if your applications can scaled up and down easily in the cloud.
Pack of Cards : If a microservice at the bottom of the call chain fails, it can have knock on effects on all other microservices. Microservices should be fault tolerant by Design.
Debugging : When there is a problem that needs investigation, you might need to look into multiple services across different components. Centralized Logging and Dashboards are essential to make it easy to debug problems.
Consistency : You cannot have a wide range of tools solving the same problem. While it is important to foster innovation, it is also important to have some decentralized governance around the languages, platforms, technology and tools used for implementing/deploying/monitoring microservices.


Solutions to Challenges with Microservice Architectures
Spring Boot
Enable building production ready applications quickly

Provide non-functional features

embedded servers (easy deployment with containers)
metrics (monitoring)
health checks (monitoring)
externalized configuration
Spring Cloud
Spring Cloud provides solutions to cloud enable your microservices. It leverages and builds on top of some of the Cloud solutions opensourced by Netflix (Netflix OSS).

Important Spring Cloud Modules
Dynamic Scale Up and Down. Using a combination of

Naming Server (Eureka)
Ribbon (Client Side Load Balancing)
Feign (Easier REST Clients)


Visibility and Monitoring with - Zipkin Distributed Tracing and  Netflix API Gateway

Configuration Management with - Spring Cloud Config Server
Fault Tolerance with - Hystrix


Rest client:
```java
import org.springframework.web.client.RestTemplate;
...
@RestController
public class CurrencyConversionController {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
  public CurrencyConversionBean convertCurrency(@PathVariable String from, @PathVariable String to,
      @PathVariable BigDecimal quantity) {

    Map<String, String> uriVariables = new HashMap<>();
    uriVariables.put("from", from);
    uriVariables.put("to", to);

    ResponseEntity<CurrencyConversionBean> responseEntity = new RestTemplate().getForEntity(
        "http://localhost:8000/currency-exchange/from/{from}/to/{to}", CurrencyConversionBean.class,
        uriVariables);

    CurrencyConversionBean response = responseEntity.getBody();

    return new CurrencyConversionBean(response.getId(), from, to, response.getConversionMultiple(), quantity,
        quantity.multiply(response.getConversionMultiple()), response.getPort());
  }
```

Feign provide a better alternative to RestTemplate to call REST API.
This is proxy:
```java
package com.in28minutes.springboot.microservice.example.currencyconversion;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="forex-service" url="localhost:8000")
public interface CurrencyExchangeServiceProxy {
  @GetMapping("/currency-exchange/from/{from}/to/{to}")
  public CurrencyConversionBean retrieveExchangeValue
    (@PathVariable("from") String from, @PathVariable("to") String to);
}
```

```java
@RestController
public class CurrencyConversionController {

  private Logger logger = LoggerFactory.getLogger(this.getClass());
  @Autowired
  private CurrencyExchangeServiceProxy proxy;
  
  @GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
  public CurrencyConversionBean convertCurrency(@PathVariable String from, @PathVariable String to,
      @PathVariable BigDecimal quantity) {

//    Map<String, String> uriVariables = new HashMap<>();
//    uriVariables.put("from", from);
//    uriVariables.put("to", to);
//    ResponseEntity<ExchangeValue> responseEntity = new RestTemplate().getForEntity(
//        "http://localhost:8000/currency-exchange/from/{from}/to/{to}", ExchangeValue.class,
//        uriVariables);
//    ExchangeValue response = responseEntity.getBody();

	  
	  ExchangeValue response = proxy.retrieveExchangeValue(from, to);

	    logger.info("{}", response);	  
	  
    return new CurrencyConversionBean(response.getId(), from, to, response.getConversionMultiple(), quantity,
        quantity.multiply(response.getConversionMultiple()), response.getPort());
  }
  
}
```


Enable Feign Clients
```java
@SpringBootApplication
@EnableFeignClients("com.in28minutes.springboot.microservice.example.currencyconversion")
@EnableDiscoveryClient
public class SpringBootMicroserviceCurrencyConversionApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringBootMicroserviceCurrencyConversionApplication.class, args);
  }
}
```


Ribbon for clinet side Load Balancing.
Enabling Ribbon
Add Ribbon Dependency to pom.xml

    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
    </dependency>
Enable RibbonClient in CurrencyExchangeServiceProxy

@FeignClient(name="forex-service")
@RibbonClient(name="forex-service")
public interface CurrencyExchangeServiceProxy {

Configure the instances in application.properties
forex-service.ribbon.listOfServers=localhost:8000,localhost:8001



Using Eureka Naming Server

Set up an Eureka server:
@SpringBootApplication
@EnableEurekaServer
public class SpringBootMicroserviceEurekaNamingServerApplication {

/spring-boot-microservice-eureka-naming-server/src/main/resources/application.properties

spring.application.name=netflix-eureka-naming-server
server.port=8761

eureka.client.register-with-eureka=false
eureka.client.fetch-registry=false

Connect FS and CCS Microservices with Eureka
Add to pom.xml

    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>

Configure Eureka URL in application.properties

eureka.client.service-url.default-zone=http://localhost:8761/eureka

Remove this configuration in application.properties
forex-service.ribbon.listOfServers=localhost:8000,localhost:8001
---
title: "Java_spring"
date: 2018-11-12T14:17:23-05:00
draft: true
---

# IoC
IoC is Inverse of Control, usually it is the developer's task to control the work flow of the computer program - initiate an object A, initiate another object B which is A's dependency and maybe B is initiated inside A.  With IoC, the control of these steps is turned over to the framework (It is the Spring Framework when using Spring.).  Developer is only to provide the configuration file or annotation in the program, and the frame is in charge to create the objects and couple the object together to make the dependency work.  Now the tasks are split into two parts: tasks taken by the framework: object lifecycle management including satisfying the dependencies and tasks taken by the developer: configuration.

## IoC container
Spring IoC taken by the Spring framework is achieved by IoC container.  The IoC container is responsible to instantiate, configure and assemble the objects.  The main tasks performed by IoC container are:

* to instantiate the application class
* to configure the object
* to assemble the dependencies between the objects

> There are two types of IoC containers. They are:
> * BeanFactory
> * ApplicationContext

> The ApplicationContext interface is built on top of the BeanFactory interface. It adds some more functionality than BeanFactory such as simple integration with Spring's AOP, message resource handling (for I18N), event propagation, application layer specific context (e.g. WebApplicationContext) for web application.  THerefore it is better to use ApplicationContext than BeanFactory.

## Configuration
IoC container need developer's instructions to work.  The tasks of the developer include:

* Objects Registration: to instruct which objects to be used by the framework
* Dependency relationship: to instruct the dependency relations among the objects

There is not so much work for developers in terms of IoC container, so the rest of this post will be focus on the configuration.

# Three types of configuration

* XML based configuration: Explicit configuration 
* Annotation Based Configuration: Implicit configuration 
* Java based configuration: Explicit configuration 

## XML based configuration:

Both the Objects Registration and Dependency injection are through the XML file.

Easy to make change frequently, hard to manage, pro to error, hard to debug

Use XML based configurations when you know you may have a need to alter the behavior of an application without the need of compiling and deploying the code all over again.

## Annotation Based Configuration:

Starting from Spring 2.5 it became possible to configure the dependency injection using annotations.  So instead of using XML to describe a bean wiring, you can move the bean configuration into the component class itself by using annotations on the relevant class, method, or field declaration.  This is an implicit configuration, which rests on three legs — annotations to mark Spring-managed beans, component-scanning to find them, and property-placeholder injection to customize values.
Annotation injection is performed before XML injection. Thus, the latter configuration will override the former for properties wired through both approaches.
<context:annotation-config/>

Major annotations:
* @Required: This annotation is applied on bean setter methods. Consider a scenario where you need to enforce a required property. The @Required annotation indicates that the affected bean must be populated at configuration time with the required property. Otherwise an exception of type BeanInitializationException is thrown.
* @Autowired: This annotation is applied on fields, setter methods, and constructors. The @Autowired annotation injects object dependency implicitly.  When you use @Autowired on fields and pass the values for the fields using the property name, Spring will automatically assign the fields with the passed values. 
You can even use @Autowired  on private properties, as shown below. (This is a very poor practice though!)

When you use @Autowired on setter methods, Spring tries to perform the by Type autowiring on the method. You are instructing Spring that it should initiate this property using setter method where you can add your custom code, like initializing any other property with this property.

Consider a scenario where you need instance of class A, but you do not store A in the field of the class. You just use A to obtain instance of B, and you are storing B in this field. In this case setter method autowiring will better suite you. You will not have class level unused fields.

When you use @Autowired on a constructor, constructor injection happens at the time of object creation. It indicates the constructor to autowire when used as a bean. One thing to note here is that only one constructor of any bean class can carry the @Autowired annotation.

NOTE: As of Spring 4.3, @Autowired  became optional on classes with a single constructor. In the above example, Spring would still inject an instance of the Person  class if you omitted the @Autowired  annotation.


* @Qualifier: This annotation is used along with @Autowired annotation. When you need more control of the dependency injection process, @Qualifier can be used. @Qualifier can be specified on individual constructor arguments or method parameters. This annotation is used to avoid confusion which occurs when you create more than one bean of the same type and want to wire only one of them with a property.

* @ComponentScan: This annotation is used with @Configuration annotation to allow Spring to know the packages to scan for annotated components. @ComponentScan is also used to specify base packages using basePackageClasses or basePackage attributes to scan. If specific packages are not defined, scanning will occur from the package of the class that declares this annotation.

* @Lazy: This annotation is used on component classes. By default all autowired dependencies are created and configured at startup. But if you want to initialize a bean lazily, you can use @Lazy annotation over the class. This means that the bean will be created and initialized only when it is first requested for. You can also use this annotation on @Configuration classes. This indicates that all @Bean methods within that @Configuration should be lazily initialized.

* @Value: This annotation is used at the field, constructor parameter, and method parameter level. The @Value annotation indicates a default value expression for the field or parameter to initialize the property with. As the @Autowired annotation tells Spring to inject object into another when it loads your application context, you can also use @Value annotation to inject values from a property file into a bean’s attribute. It supports both #{...} and ${...} placeholders.


* Spring Framework Stereotype Annotations
Annotations denoting the roles of types or methods in the overall architecture (at a conceptual, rather than implementation, level).

@Component
This annotation is used on classes to indicate a Spring component. The @Component annotation marks the Java class as a bean or say component so that the component-scanning mechanism of Spring can add into the application context.

@Controller
The @Controller  annotation is used to indicate the class is a Spring controller. This annotation can be used to identify controllers for Spring MVC or Spring WebFlux.

@Service
This annotation is used on a class. The @Service marks a Java class that performs some service, such as execute business logic, perform calculations and call external APIs. This annotation is a specialized form of the @Component annotation intended to be used in the service layer.

@Repository
This annotation is used on Java classes which directly access the database. The @Repository annotation works as marker for any class that fulfills the role of repository or Data Access Object.

This annotation has a automatic translation feature. For example, when an exception occurs in the @Repository there is a handler for that exception and there is no need to add a try catch block.

* Spring Boot Annotations
@EnableAutoConfiguration
This annotation is usually placed on the main application class. The @EnableAutoConfiguration annotation implicitly defines a base “search package”. This annotation tells Spring Boot to start adding beans based on classpath settings, other beans, and various property settings.

@SpringBootApplication
This annotation is used on the application class while setting up a Spring Boot project. The class that is annotated with the @SpringBootApplication must be kept in the base package. The one thing that the @SpringBootApplication does is a component scan. But it will scan only its sub-packages. As an example, if you put the class annotated with @SpringBootApplication in com.example then @SpringBootApplication will scan all its sub-packages, such as com.example.a, com.example.b, and com.example.a.x.

The @SpringBootApplication is a convenient annotation that adds all the following:

@Configuration
@EnableAutoConfiguration
@ComponentScan


* JSR-250 Annotations
Spring supports JSR-250 based annotations which include @Resource, @PostConstruct and @PreDestroy annotations.

Use Annotations in anything that is stable and defines the core structure of the application. Anything that would need a code change is okay to sit as an annotation.

## Java based configuration
with Spring 3.0, came Java based configuration, completed in Spring 3.1.
– type-safety check by compiling
– code completion
– refactoring support
– support for finding references in the workspace (even on jars in the classpath)
@Configuration & @Bean Annotations

* @Configuration: This annotation is used on classes which define beans. @Configuration is an analog for XML configuration file – it is configuration using Java class. Java class annotated with @Configuration is a configuration by itself and will have methods to instantiate and configure the dependencies.

* @Bean: This annotation is used at the method level. @Bean annotation works with @Configuration to create Spring beans. As mentioned earlier, @Configuration will have methods to instantiate and configure dependencies. Such methods will be annotated with @Bean. The method annotated with this annotation works as bean ID and it creates and returns the actual bean.




# constructor and setter injection
There are two types of injection: constructor and setter injection.

Benefit of setter injection over constructor injection.

* Partial dependency: can be injected using setter injection but it is not possible by constructor. Suppose there are 3 properties in a class, having 3 arg constructor and setters methods. In such case, if you want to pass information for only one property, it is possible by setter method only.
* Overriding: Setter injection overrides the constructor injection. If we use both constructor and setter injection, IOC container will use the setter injection.
* Changes: We can easily change the value by setter injection. It doesn't create a new bean instance always like constructor. So setter injection is flexible than constructor injection.



# Autowiring in XML configuration
Autowiring feature of spring framework enables you to inject the object dependency implicitly. It internally uses setter or constructor injection.
Autowiring can't be used to inject primitive and string values. It works with reference only.


There are many autowiring modes:

No.	Mode	Description
1)	no	It is the default autowiring mode. It means no autowiring bydefault.
2)	byName	The byName mode injects the object dependency according to name of the bean. In such case, property name and bean name must be same. It internally calls setter method.
3)	byType	The byType mode injects the object dependency according to type. So property name and bean name can be different. It internally calls setter method.
4)	constructor	The constructor mode injects the dependency by calling the constructor of the class. It calls the constructor having large number of parameters.
5)	autodetect	It is deprecated since Spring 3.


XML based
xml based autowire: Autowiring : using autowire attribute in <bean> tag.
autowire="byName" autowire="byType" autowire="constructor" autowire="no"


# Example of Java based configuration


```xml
    <dependency>
	    <groupId>org.springframework</groupId>
	    <artifactId>spring-context</artifactId>
	    <version>5.1.3.RELEASE</version>
	</dependency> 
```
```java
package com.mnc.test.spirng_test2;

import org.springframework.context.annotation.*;

@Configuration
public class JavaBasedConfig {
	@Bean
	public HelloWorld helloWorld(Formatter fmt) {
		return new HelloWorld(fmt);
	}
	
	@Bean
	public HelloWorld helloWorld() {
		return new HelloWorld(formatter());
	}
	
	@Bean
	public Formatter formatter() {
		return new Formatter();
	}

	@Bean
	public HelloWorld2 helloWorld2(Formatter fmttter) {
		HelloWorld2 helloWorld2 = new HelloWorld2();
		helloWorld2.setfmt(fmttter);
		return helloWorld2;
	}
}
```

```java
public class HelloWorld {
   private String message;
   private Formatter fmt;
   
   public HelloWorld(Formatter fmt) {
	   this.fmt = fmt;
   }
   public void setMessage(String message){
      this.message  = message;
   }
   public void getMessage(){
      System.out.println("Your Message : " + fmt.changeFormat(message));
   }
}
```

```java
public class HelloWorld2 {
   private String message;
   
   private Formatter fmt;

   public void setMessage(String message){
      this.message  = message;
   }
   public void getMessage(){
      System.out.println("Your Message2 : " + fmt.changeFormat(message));
   }
   
   public void setfmt(Formatter fmt){
	      this.fmt  = fmt;
	   }   
   
}
```

```java
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

public class App {
	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(JavaBasedConfig.class);

		HelloWorld helloWorld = ctx.getBean(HelloWorld.class);
		helloWorld.setMessage("Hello World!");
		helloWorld.getMessage();
		
		HelloWorld2 helloWorld2 = ctx.getBean(HelloWorld2.class);
		helloWorld2.setMessage("Hello World!");
		helloWorld2.getMessage();
		
	}
}
```
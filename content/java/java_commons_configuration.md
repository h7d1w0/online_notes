---
title: "Java_commons_configuration"
date: 2018-11-20T11:19:34-05:00
draft: true
---
Configuration Sources
PropertiesConfiguration Loads configuration values from a properties file.
XMLConfiguration Takes values from an XML document.
INIConfiguration Loads the values from a .ini file as used by Windows.
PropertyListConfiguration Loads values from an OpenStep .plist file. XMLPropertyListConfiguration is also available to read the XML variant used by Mac OS X.
JNDIConfiguration Using a key in the JNDI tree, can retrieve values as configuration properties.
BaseConfiguration An in-memory method of populating a Configuration object.
HierarchicalConfiguration An in-memory Configuration object that is able to deal with complex structured data.
SystemConfiguration A configuration using the system properties
ConfigurationConverter Takes a java.util.Properties or an org.apache.commons.collections.ExtendedProperties and converts it to a Configuration object.


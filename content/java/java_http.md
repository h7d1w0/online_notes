---
title: "Java_http"
date: 2018-11-29T10:53:38-05:00
draft: true
---



Google http client get example
```java
package com.tgam.databrickstest;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final String USER_CREDENTIALS = "............";

    public static void main(String[] args) {
        try {
            HttpRequestFactory requestFactory
                    = HTTP_TRANSPORT.createRequestFactory((HttpRequest request) -> {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    });
            
            GenericUrl url = new GenericUrl();
            url.setScheme("https");
            url.setHost("globeandmail-dev.cloud.databricks.com/");
            List<String> pathPars = new ArrayList();
            pathPars.add("api");
            pathPars.add("2.0");
            pathPars.add("clusters");
            pathPars.add("list-node-types");
            url.setPathParts(pathPars);
            HttpRequest request = null;
            try {
                request = requestFactory.buildGetRequest(url);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(USER_CREDENTIALS.getBytes()));
            HttpHeaders headers = request.getHeaders();
            headers.setAuthorization(basicAuth);
            HttpResponse rs = request.execute();
            OutputStream out = new FileOutputStream("/tmp/out");
            rs.download(out);
            System.out.println(rs.getContentEncoding());
            System.out.println(url.build());
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


```
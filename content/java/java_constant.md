---
title: "Java_jaxb"
date: 2019-01-04T09:26:52-05:00
draft: true
---

```java

import java.util.Objects;

public class FattailFields {
    final private String value;
    
    final static public FattailFields CAMPAIGN_DELIVERY_DATE = new FattailFields("(Campaign) Delivery Date");
    final static public FattailFields CAMPAIGN_AD_SERVER_CAMPAIGN_ID = new FattailFields("(Campaign) Ad Server Campaign ID");
    final static public FattailFields CAMPAIGN_CAMPAIGN_NAME = new FattailFields("(Campaign) Campaign Name");
    final static public FattailFields CAMPAIGN_CAMPAIGN_ID = new FattailFields("(Campaign) Campaign ID");
    final static public FattailFields CAMPAIGN_AD_SERVER_CLIENT_ID = new FattailFields("(Campaign) Ad Server Client ID");
    final static public FattailFields CAMPAIGN_CLIENT_NAME = new FattailFields("(Campaign) Client Name");
    final static public FattailFields DROP_AD_SERVER_DROP_ID = new FattailFields("(Drop) Ad Server Drop ID");
    final static public FattailFields DROP_TRAFFIC_NAME = new FattailFields("(Drop) Traffic Name");
    final static public FattailFields DROP_DROP_ID = new FattailFields("(Drop) Drop ID");
    final static public FattailFields DROP_POSITION_PATH = new FattailFields("(Drop) Position Path");
    final static public FattailFields CAMPAIGN_AD_SERVER_AGENCY_ID = new FattailFields("(Campaign) Ad Server Agency ID");
    final static public FattailFields CAMPAIGN_AGENCY_NAME = new FattailFields("(Campaign) Agency Name");
    final static public FattailFields DROP_AD_TYPE = new FattailFields("(Drop) Ad Type");
    final static public FattailFields DROP_PRIORITY = new FattailFields("(Drop) Priority");
    final static public FattailFields DROP_PRICE_TYPE = new FattailFields("(Drop) Price Type");
    final static public FattailFields DROP_3RD_PARTY_AD_SERVER_BUFFERED_IMPRESSIONS = new FattailFields("(Drop) 3rd Party Ad Server Buffered Impressions");
    final static public FattailFields DROP_IMPRESSIONS_SOLD = new FattailFields("(Drop) Impressions Sold");
    final static public FattailFields CAMPAIGN_CAMPAIGN_START_DATE = new FattailFields("(Campaign) Campaign Start Date");
    final static public FattailFields CAMPAIGN_CAMPAIGN_END_DATE = new FattailFields("(Campaign) Campaign End Date");
    final static public FattailFields DROP_START_DATE = new FattailFields("(Drop) Start Date");
    final static public FattailFields DROP_END_DATE = new FattailFields("(Drop) End Date");
    final static public FattailFields CAMPAIGN_IO_STATUS = new FattailFields("(Campaign) IO Status");
    final static public FattailFields CAMPAIGN_BPG_PROJECT = new FattailFields("(Campaign) BPG Project");
    final static public FattailFields CAMPAIGN_SALES_REP = new FattailFields("(Campaign) Sales Rep");
    final static public FattailFields DROP_SOLD_AMOUNT = new FattailFields("(Drop) Sold Amount");
    final static public FattailFields CAMPAIGN_TOTAL_CAMPAIGN_VALUE = new FattailFields("(Campaign) Total Campaign Value");
    final static public FattailFields DROP_UNITS = new FattailFields("(Drop) Units");
    final static public FattailFields DROP_INDEX_CURRENT = new FattailFields("(Drop) Index Current");
    final static public FattailFields DROP_PRICE = new FattailFields("(Drop) Price");
    
    FattailFields(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FattailFields other = (FattailFields) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
}
```


```java
public enum FattailFields {
    
    CAMPAIGN_DELIVERY_DATE ("(Campaign) Delivery Date"),
    CAMPAIGN_AD_SERVER_CAMPAIGN_ID ("(Campaign) Ad Server Campaign ID"),
    CAMPAIGN_CAMPAIGN_NAME ("(Campaign) Campaign Name"),
    CAMPAIGN_CAMPAIGN_ID ("(Campaign) Campaign ID"),
    CAMPAIGN_AD_SERVER_CLIENT_ID ("(Campaign) Ad Server Client ID"),
    CAMPAIGN_CLIENT_NAME ("(Campaign) Client Name"),
    DROP_AD_SERVER_DROP_ID ("(Drop) Ad Server Drop ID"),
    DROP_TRAFFIC_NAME ("(Drop) Traffic Name"),
    DROP_DROP_ID ("(Drop) Drop ID"),
    DROP_POSITION_PATH ("(Drop) Position Path"),
    CAMPAIGN_AD_SERVER_AGENCY_ID ("(Campaign) Ad Server Agency ID"),
    CAMPAIGN_AGENCY_NAME ("(Campaign) Agency Name"),
    DROP_AD_TYPE ("(Drop) Ad Type"),
    DROP_PRIORITY ("(Drop) Priority"),
    DROP_PRICE_TYPE ("(Drop) Price Type"),
    DROP_3RD_PARTY_AD_SERVER_BUFFERED_IMPRESSIONS ("(Drop) 3rd Party Ad Server Buffered Impressions"),
    DROP_IMPRESSIONS_SOLD ("(Drop) Impressions Sold"),
    CAMPAIGN_CAMPAIGN_START_DATE ("(Campaign) Campaign Start Date"),
    CAMPAIGN_CAMPAIGN_END_DATE ("(Campaign) Campaign End Date"),
    DROP_START_DATE ("(Drop) Start Date"),
    DROP_END_DATE ("(Drop) End Date"),
    CAMPAIGN_IO_STATUS ("(Campaign) IO Status"),
    CAMPAIGN_BPG_PROJECT ("(Campaign) BPG Project"),
    CAMPAIGN_SALES_REP ("(Campaign) Sales Rep"),
    DROP_SOLD_AMOUNT ("(Drop) Sold Amount"),
    CAMPAIGN_TOTAL_CAMPAIGN_VALUE ("(Campaign) Total Campaign Value"),
    DROP_UNITS ("(Drop) Units"),
    DROP_INDEX_CURRENT ("(Drop) Index Current"),
    DROP_PRICE ("(Drop) Price");
    
    private final String fieldName;
    FattailFields(String fieldName) {
        this.fieldName = fieldName;
    }

    public String fieldName() {
        return this.fieldName;
    }
}
```
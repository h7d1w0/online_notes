---
title: "Java_Restful"
date: 2019-01-07T15:33:33-04:00
draft: true
---

A REST service also has a schema in what is called a WADL – Web Application Description Language. The WADL for the above call would look like this:

```xml
<?xml version="1.0"?>
<application xmlns="http://wadl.dev.java.net/2009/02">
<doc xml:lang="en" title="http://www.catechizeme.com"/>
<resources base="http://www.catechizeme.com">
<resource path="catechisms/{CATECHISM_NAME}/daily_question.js" id="Daily_question.js">
<doc xml:lang="en" title="Daily_question.js"/>
<param xmlns:xs="http://www.w3.org/2001/XMLSchema" name="CATECHISM_NAME" style="template" type="string"/>
<method name="GET" id="Daily_question.js">
   <doc xml:lang="en" title="Daily_question.js"/>
   <request/>
   <response status="200">
     <representation mediaType="json" element="data"/>
     <representation mediaType="js; charset=utf-8" element="data"/>
   </response> 
</method>
</resource>
</resources>
</application>
```

The WADL does not have any mechanism to represent the data itself, which is what must be sent on the URI. This means that the WADL is able to document only about half of the information you need in order to interface with the service.

Take for example the parameter CATECHISM_NAME in the above sample. The WADL only tells you where in the URI the parameter belongs, and that it should be a string.
The WADL is completely optional.
Further the WADL is completely optional; in fact, it is quite rare that the WADL is supplied at all! Due to the nature of the service, in order to make any meaningful use of it, you will almost undoubtedly need additional documentation.

Arguably, the biggest drawback is the WADL – optional and lacking some necessary information. To address this deficiency, there are several frameworks available on the market that help document and produce RESTful APIs, such as Swagger, RAML, or JSON-home. Swagger has been donated to the Open API Iniative and is now called OpenAPI (OAS). Head over to Swagger.io where you can read more about this standard, the specification, and how the Swagger tools play a role.


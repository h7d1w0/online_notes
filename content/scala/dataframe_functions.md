  @SerialVersionUID(7161735266679388304L)
  class ArrayEqualToString extends UDF2[Seq[String], String, Boolean] {
    override def call(tolerantArr: Seq[String], restrictStr: String): Boolean = {
      if(tolerantArr == null || tolerantArr.size == 1 && tolerantArr(0) == ""  || tolerantArr.isEmpty) {
        true
      } else if(restrictStr == null || restrictStr == "" || restrictStr.isEmpty) {
        false
      } else {
        tolerantArr.contains(restrictStr)
      }
    }
  }

    spark.udf.register("ArrEqualToArr",new ArrayEqualToArray(), DataTypes.BooleanType)
    spark.udf.register("ArrEqualToStr",new ArrayEqualToString(), DataTypes.BooleanType)



  val dateAdd = (dateStr: String, add: Int) => {
     LocalDate.parse(dateStr).plusDays(add).format(DateTimeFormatter.ISO_LOCAL_DATE)
  }

  val dateAddUDF = udf(dateAdd)
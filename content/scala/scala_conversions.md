---
title: "Scala_conversions"
date: 2018-11-07T11:50:28-05:00
draft: true
---
    val nums = Seq("75.21-", "24", "-98.1")

    import spark.implicits._
    val numbDf = nums.toDS().toDF()

    val numbDf2 = numbDf.withColumn("num", regexp_replace('value, "([\\d\\.]+)-", "-$1"))

    numbDf2.collect.foreach(println)

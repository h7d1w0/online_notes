---
title: "Eta_expansion"
date: 2018-10-05T12:21:39-04:00
draft: false
categories: ["concept", "programming"]
tags: ["eta-conversion", "eta-expansion"]
---

# Eta-conversion in Mathematics
(Information was taken from wikipedia.com)

Lambda calculus (also written as λ-calculus) is a formal system in mathematical logic for expressing computation based on function abstraction and application using variable binding and substitution. It is a universal model of computation that can be used to simulate any Turing machine. It was first introduced by mathematician Alonzo Church in the 1930s as part of his research of the foundations of mathematics. 

η-conversion(Eta-conversion) is one of the three kinds of reduction besides α-conversion and β-reduction.

Eta-conversion expresses the idea of extensionality, which in this context is that two functions are the same if and only if they give the same result for all arguments. Eta-conversion converts between λx.(f x) and f whenever x does not appear free in f.

# Eta-conversion in Haskell
(Information was taken from https://wiki.haskell.org)

Scala's functional programming part is inspired and influnced heavily by Haskell, and it would be helpful to see the eta-conversion in Haskell and it will help on understanding the eta-expansion in Scala. 

An eta conversion (also written η-conversion) is adding or dropping of abstraction over a function. For example, the following two values are equivalent under η-conversion:
```Haskell
\x -> abs x

abs
```
Converting from the first to the second would constitute an eta reduction, and moving from the second to the first would be an eta abstraction. The term 'eta conversion' can refer to the process in either direction.

Extensive use of η-reduction can lead to Pointfree programming. It is also typically used in certain compile-time optimisations.


# Eta-conversion in Scala
(Information was taken from The Scala Language Specification Version 2.9)

Eta-expansion converts an expression of method type to an equivalent expression of function type.

Syntax:

SimpleExpr ::= SimpleExpr1 ‘_’

The expression e _ is well-formed if e is of method type or if e is a call-by-name parameter. If e is a method with parameters, e _ represents e converted to a function type by eta expansion (§6.26.5). If e is a parameterless method or call-by-name parameter of type =>T , e _ represents the function of type () => T , which evaluates e when it is applied to the empty parameterlist.

# My notes on Eta-conversion
Generally(in λ-calculus and Haskell), let's say there are two functions f and g, 

  * if g takes the same arguments and
  * what g does is to call f and nothing else

, then the process of from f getting g is eta-expansion(eta-abstraction) and if we go from g to get f, the process becomes eta-reduction.

Scala's take on eta-conversion is a little bit different from the general version of eta-conversion with two apparent attributes:

 * Scala compiler emphasizes only the eta-expansion.
 * Scala compiler uses eta-expansion mainly to convert a method to a function, which is kind of implicit "type" conversion. The function's arguments can be different from the method's arguments

The second point need more elabration: in Scala, if we have a method(either static or belonging to an object), we can create a function that calls the method only and does nothing more, then we did eta-expansion.  Since this process is so simple, sometimes Scala complier can do it automatically for the developers and it makes things look like where a function is required, there is no complaint when passing a method.

# Code sample for Eta-expansion in Scala
The code below works and print out 20s
```Scala
object EtaExpansionTest{

  def main(args: Array[String]): Unit ={
    val myInt = new MyInt();

    //Using _ which means the rest of the arguments
    //in this case it means all the arguments
    val fTimes1 = myInt.times _
    println(fTimes1(4, 5))

    //To tell compiler no guess, the developer provide all the information
    // to create a function from a method
    val fTimes2 = (num1: Int, num2: Int) => myInt.times(num1, num2)
    println(fTimes1(4, 5))

    //To create a function with different argument list from the method's
    val fDouble = (num1: Int) => myInt.times(num1, 2)
    println(fDouble(10))

    //To create a function with different argument list from the method's
    val fDouble2 = myInt.times(_: Int, 2)
    println(fDouble2(10))

    //Function f2 takes another function, and two integers as argument
    val f2 = (f: (Int, Int) => Int, num1: Int, num2: Int) => {
      f(num1, num2)
    }

    //Scala complier does the eta-expansion
    println(f2(myInt.times, 2, 10))


    //Pass function explicitly
    println(f2(fTimes1, 2, 10))
    println(f2(fTimes2, 2, 10))

  }
}

class MyInt {
  def times(num1: Int, num2: Int): Int ={
    num1 * num2
  }
}
```

When there are parameter-less and empty-parentheses methods are involved, Scala code can be confusing, since there are very much rules and they can be very sutle.  However, those are beyond this topic.
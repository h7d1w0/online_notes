---
title: "Nginx"
date: 2019-09-18T13:50:23-05:00
draft: true
---

# Run graphql server on port 4000



nginx.conf file


```conf
    server {
        listen       80 default_server;
        #listen       [::]:80 default_server;
        server_name  _ ;
        root         /www/build;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        # redirect graphql request to graphql server on 4000
        location /graphql {
          proxy_pass http://127.0.0.1:4000;
        }

        #All the routing path go to the single page
        location / {
          try_files   $uri /index.html;
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
```
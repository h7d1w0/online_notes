---
title: "Security"
date: 2019-01-15T13:50:23-05:00
draft: true
---

# certificate
A certificate contains a public key.

The certificate, in addition to containing the public key, contains additional information such as issuer, what the certificate is supposed to be used for, and other types of metadata.

Typically, a certificate is itself signed with a private key that verifies its authenticity.


# How to sign a public key:
Lets say company A has a key pair and needs to publish his public key for public usage (aka ssl on his web site). 

Company A must make a certificate request (CR) to a certification authority (CA) to get a certificate for his key pair.
The public key, but not the private key, of company A's key pair is included as part of the certificate request.
The CA then uses company A's identity information to determine whether the request meets the CA's criteria for issuing a certificate.
If the CA approves the request, it issues a certificate to company A. In brief CA signs company A's public key with his(CA's) private key, which verifies its authenticity.
So company A's public key signed with a valid CA's private key is called company A's certificate.






Generate SSL key and certificate for each Kafka broker

If host name verification is enabled, clients will verify the server's fully qualified domain name (FQDN) against one of the following two fields:
Common Name (CN)
Subject Alternative Name (SAN)

Both fields are valid, RFC-2818 recommends the use of SAN however. SAN is also more flexible, allowing for multiple DNS entries to be declared. Another advantage is that the CN can be set to a more meaningful value for authorization purposes. To add a SAN field append the following argument -ext SAN=DNS:{FQDN} to the keytool command:

keytool -keystore server.keystore.jks -alias localhost -validity {validity} -genkey -keyalg RSA -ext SAN=DNS:{FQDN}



OpenSSL is a free and open-source cryptographic library that provides several command-line tools for handling digital certificates. Some of these tools can be used to act as a certificate authority.

A certificate authority (CA) is an entity that signs digital certificates. Many websites need to let their customers know that the connection is secure, so they pay an internationally trusted CA (eg, VeriSign, DigiCert) to sign a certificate for their domain.


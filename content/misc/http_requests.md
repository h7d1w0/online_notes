---
title: "http_requests"
date: 2018-12-14T13:11:13-05:00
draft: true
---

GET
HEAD
POST
PUT
DELETE
CONNECT
OPTIONS
TRACE


 difference between PUT vs POST
 the PUT method is used to create or overwrite a resource at a particular URL that is known by the client.
 the POST method should be used to create a subordinate (or child) of the resource identified by the Request-URI.


 First off, choosing between using PUT vs POST should be based on the action’s idempotence. As Wikipedia puts it,

Idempotence is the property of certain operations in mathematics and computer science, that can be applied multiple times without changing the result beyond the initial application

With this definition, we can say that the PUT method is idempotent because no matter how many times we send the same request, the results will always be the same. On the other hand, the POST method is not idempotent since if we send the same POST request multiple times, we will receive various results (i.e. a new subordinate will be created each time).

RFC 2616, explains the difference between PUT vs POST as follows.

The fundamental difference between the POST and PUT requests is reflected in the different meaning of the Request-URI. The URI in a POST request identifies the resource that will handle the enclosed entity… In contrast, the URI in a PUT request identifies the entity enclosed with the request.

When you know the URL of the thing you want to create or overwrite, a PUT method should be used. Alternatively, if you only know the URL of the category or sub-section of the thing you want to create something within, use the POST method.
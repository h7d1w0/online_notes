---
title: "SnowFlake Time Travel"
date: 2019-03-04T11:50:23-05:00
draft: true
---

select count(*) from buddy_v02.demo.base_impressions_v2_demo
before(statement => '33d616a3-f85f-40d0-972f-969e3a0c03d5')
where date between '2018-12-01' and '2018-12-31'  
;


insert into  buddy_v02.demo.base_impressions_v2_demo
select * from buddy_v02.demo.base_impressions_v2_demo
before(statement => '33d616a3-f85f-40d0-972f-969e3a0c03d5')
where date between '2018-12-01' and '2018-12-31'  
;


create table buddy_v02.demo.base_impressions_v2_demo2 clone buddy_v02.demo.base_impressions_v2_demo2 
before(statement => '33d616a3-f85f-40d0-972f-969e3a0c03d5');


create table etl_dev.public.MONTHLY_DIGITAL_DASHBOARD_DUMP4 clone sophi.public.MONTHLY_DIGITAL_DASHBOARD 
at (timestamp => to_timestamp_tz('02/28/2019 18:02:03', 'mm/dd/yyyy hh24:mi:ss'))



ALTER SESSION SET sessionParams

ALTER SESSION UNSET <param_name> [ , <param_name> , ... ]

USE_CACHED_RESULT = TRUE | FALSE

ALTER SESSION SET sessionParams USE_CACHED_RESULT = FALSE

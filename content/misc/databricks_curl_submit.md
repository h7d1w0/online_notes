---
title: "DataBricks Curl Submit"
date: 2019-02-22T11:50:23-05:00
draft: true
---

Using Curl to remotely submit a notebook job.
<!--more-->

submit_to_db.sh
```bash
#!/bin/bash

YEAR=$1
MONTH=$2
DAY=$3
NUMBER_OF_DAYS=$4
BATCH=${5:-1}

PROCESSED=0
FIRST_DAY_TO_PROCESS=$(date --date="$1-$2-$3" +"%Y-%m-%d")
CURR_DAY=$FIRST_DAY_TO_PROCESS

while [ $PROCESSED -lt $NUMBER_OF_DAYS ]; do
  YEAR1=$(date --date="$CURR_DAY"  +"%Y")
  MONTH1=$(date --date="$CURR_DAY"  +"%-m")
  DAY1=$(date --date="$CURR_DAY"  +"%-d")
  echo $YEAR1 $MONTH1 $DAY1
  CURR_DAY=$(date --date="$CURR_DAY +$BATCH days"  +"%Y-%m-%d")
  PROCESSED=$(( PROCESSED + BATCH ))
  if [ $PROCESSED -gt $NUMBER_OF_DAYS ]; then
	  BATCH=$(( NUMBER_OF_DAYS - PROCESSED + BATCH )) 
  fi;
  echo $BATCH
  echo submit_to_db_one_day.sh $YEAR1 $MONTH1 $DAY1 $BATCH
done;

```



submit_to_db_one_day.sh
```bash
#!/bin/bash

YEAR=$1
MONTH=$2
DAY=$3
NUMBER_OF_DAYS=$4

curl -n \
-X POST -H 'Content-Type: application/json' \
-d '{
  "run_name": "yeildex",
    "new_cluster": {
    "spark_version": "4.1.x-scala2.11",
    "driver_node_type_id": "m4.xlarge",
    "node_type_id": "c4.4xlarge",
    "num_workers": 4,

      "aws_attributes": {
            "availability": "ON_DEMAND",
          "ebs_volume_count":1,
        "ebs_volume_type": "THROUGHPUT_OPTIMIZED_HDD",
      "ebs_volume_size":500
          }
    },
      "notebook_task": {
          "notebook_path": "/Users/SGao@globeandmail.com/cleon/cleon_historicalProduct_source_s3dev",
      "base_parameters": {
              "YEAR": '"$YEAR"',
              "MONTH": '"$MONTH"',
              "DAY": '"$DAY"',
              "NUMBEROFDAYS": "$NUMBER_OF_DAYS"
          }
    }
}' https://globeandmail-dev.cloud.databricks.com/api/2.0/jobs/runs/submit
```
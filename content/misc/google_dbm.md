---
title: "Google: DoubleClick Bid Manager"
date: 2018-11-27T13:50:23-05:00
draft: true
---

Google DoubleClick Bid Manager Java API is an example on java implementation of REST client API and an example of java implementation of fluent programming style.  This article shows how java API maps to the REST request.
<!--more-->

# Maven coordinate (as of 2018-11-27)

<dependencies>
 <dependency>
    <groupId>com.google.apis</groupId>
    <artifactId>google-api-services-doubleclickbidmanager</artifactId>
    <version>v1-rev48-1.22.0</version>
  </dependency>
  <dependency>
    <groupId>com.google.http-client</groupId>
    <artifactId>google-http-client-jackson2</artifactId>
    <version>${project.http.version}</version>
  </dependency>
  <dependency>
    <groupId>com.google.oauth-client</groupId>
    <artifactId>google-oauth-client-jetty</artifactId>
    <version>${project.oauth.version}</version>
  </dependency>
</dependencies>

# HTTP request

The Bid Manager API allows developers to manage Queries, retrieve Reports and Structured Data Files, and download and upload Line Items.  And below are the http requests that can be made:

Lineitems requests:
POST https://www.googleapis.com/doubleclickbidmanager/v1/lineitems/downloadlineitems
POST https://www.googleapis.com/doubleclickbidmanager/v1/lineitems/uploadlineitems

Queries requests:
POST https://www.googleapis.com/doubleclickbidmanager/v1/query
DELETE https://www.googleapis.com/doubleclickbidmanager/v1/query/queryId
GET https://www.googleapis.com/doubleclickbidmanager/v1/query/queryId
GET https://www.googleapis.com/doubleclickbidmanager/v1/queries
POST https://www.googleapis.com/doubleclickbidmanager/v1/query/queryId

Reports request:
GET https://www.googleapis.com/doubleclickbidmanager/v1/queries/queryId/reports

Sdf request:
POST https://www.googleapis.com/doubleclickbidmanager/v1/sdf/download

# Bid manager service entrance point

com.google.api.services.doubleclickbidmanager.DoubleClickBidManager is the entry point for most of the tasks that need to be done.  Basically it is a client for the DoubleClick Bid Manager API service and it can be constructed through its nested builder class: com.google.api.services.doubleclickbidmanager.DoubleClickBidManager.Builder.  The Builder has many methods (e.g. setApplicationName, setSuppressAllChecks) to configure the building of the DoubleClickBidManager.  All these methods return a Builder instance which allows method chaining and enables the fluent programming style.

When DoubleClickBidManager is created, we kind of get to the point corresponding to the http request: "https://www.googleapis.com/doubleclickbidmanager/v1/".  DoubleClickBidManager has four public methods lineitems(), queries(), reports() and sdf() each of which creates an object of DoubleClickBidManager.Lineitems, DoubleClickBidManager.Queries, DoubleClickBidManager.Reports or DoubleClickBidManager.Sdf respectively.  AFter running one of the methods, we go to the point equavelant to the resource level (the immediate level beneth /v1/, e.g. https://www.googleapis.com/doubleclickbidmanager/v1/lineitems) in terms of http requests.

Now we use the Queries requests as example, saying we run DoubleClickBidManager.queries().  We end up with a DoubleClickBidManager.Queries.  The Queries object has five public methods: createquery(Query content), deletequery(java.lang.Long queryId), getquery(java.lang.Long queryId), listqueries() and runquery(java.lang.Long queryId, RunQueryRequest content), each of which creates an object of DoubleClickBidManager.Queries.Createquery, DoubleClickBidManager.Queries.Deletequery, DoubleClickBidManager.Queries.Getquery, DoubleClickBidManager.Queries.Listqueries, or DoubleClickBidManager.Queries.Runquery.  These five class corresponds to the five queries' http requests.  And actually, they are the subclass of com.google.api.services.doubleclickbidmanager.DoubleClickBidManagerRequest<Query>, which has a method called excute() and excute() will submit the request to the server.

Again, fluent programming style is obvious here and also we can see fluent programming style is not just chaining the methods - the method called in different step returns different type of object.


# Request bodies and the returned value of execute()

Since com.google.api.services.doubleclickbidmanager.DoubleClickBidManagerRequest<T> that is the supper class for all the requests, inherits from com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>, there is no difficulty to say internally the request body for any request should be presented as a json string.  With the Bid Manager API, there is no need to provide a json string as the request body though, the API gracefully provides proper data model for each type of request to build the request body.  The resource body is provided at the same time when the request is being created by DoubleClickBidManager.queries() and the method it calls.  For example, to create a query, DoubleClickBidManager.queries().createquery(Query content) is used, a Query object should be passed as the parameter.  After this call, an object of DoubleClickBidManager.Queries.Createquery is created.  The inheritage tree like this:
ava.lang.Object
java.util.AbstractMap<java.lang.String,java.lang.Object>
com.google.api.client.util.GenericData
com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
com.google.api.services.doubleclickbidmanager.DoubleClickBidManagerRequest<Query>
com.google.api.services.doubleclickbidmanager.DoubleClickBidManager.Queries.Createquery

this is concrete method execute() is defined in com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T> and the return type of this method is T.  Since DoubleClickBidManager.Queries.Createquery extends DoubleClickBidManagerRequest<Query>, when calling execute() on DoubleClickBidManager.Queries.Createquery, a Query will be returned.
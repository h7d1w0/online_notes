---
title: "SnowFlake UDF"
date: 2019-04-18T11:50:23-05:00
draft: true
---

 create or replace function remove_keys_from_array(v variant)
  returns variant
  language javascript
  as ' 
    function transform(input) {
      var returnArray = [];
      if (Array.isArray(input)) {
        var arrayLength = input.length;
        for (var i = 0; i < arrayLength; i++) {
          var obj = input[i];
          for (var key in obj) {
            value = obj[key];
            returnArray[i] = value;
          }
        }
        return returnArray;
      }

    }
   return  transform(V);
  ';

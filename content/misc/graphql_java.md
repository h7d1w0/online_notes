---
title: "GraphQL Java"
date: 2019-07-10T10:11:13-05:00
draft: true
---

type Query {
  inspectParameter(parameter: ParaType): String
}

input ParaType{
  start_date: String
  end_date: String
}

public DataFetcher inspectParameterDataFetcher() {
    return dataFetchingEnvironment -> {
        java.util.LinkedHashMap arg = dataFetchingEnvironment.getArgument("parameter");
        return arg.get("start_date");
    };
}



enum return java.lang.String



    public DataFetcher inspectParameterDataFetcher() {
        return dataFetchingEnvironment -> {
//        	String arg = dataFetchingEnvironment.getArgument("dynamicDateRange");
        	graphql.schema.GraphQLSchema schema = dataFetchingEnvironment.getGraphQLSchema();
        	java.util.List<graphql.schema.GraphQLType> types = schema.getAllTypesAsList();
        	graphql.schema.GraphQLEnumType type = (graphql.schema.GraphQLEnumType) schema.getType("DynamicDateRange");
        	List<GraphQLEnumValueDefinition> children = type.getValues();
        	StringBuilder sb = new StringBuilder();
        	for(GraphQLEnumValueDefinition child: children) {
        		sb.append(child.getName()+"-"+ child.getValue());
        		sb.append(",");
        	}
//        	java.util.Map<java.lang.String,java.lang.Object> args = dataFetchingEnvironment.getArguments();
//        	for(java.util.Map.Entry<java.lang.String,java.lang.Object> entry: args.entrySet()) {
//        		sb.append(entry.getKey());
//        		sb.append("->");
//        		sb.append(entry.getValue().getClass().getName());
//        		sb.append(",");
//        	}
            return sb.toString();
        };
    } 
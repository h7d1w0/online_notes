---
title: "Webservice"
date: 2018-12-05T13:11:13-05:00
draft: true
---

# Web service and XML
A web service is a collection of open protocols and standards used for exchanging data between applications or systems. Software applications written in various programming languages and running on various platforms can use web services to exchange data over computer networks like the Internet in a manner similar to inter-process communication on a single computer. This interoperability (e.g., between Java and Python, or Windows and Linux applications) is due to the use of open standards.

It was possible that Web service is not related with XML.  However, in 2001, A document(Web Services Description Language (WSDL) 1.1 by Ariba, IBM and Microsoft) was submitted to the World Wide Web Consortium (see Submission Request, W3C Staff Comment) as a suggestion for describing services for the W3C XML Activity on XML Protocols.  WSDL is an XML format for describing network services as a set of endpoints operating on messages containing either document-oriented or procedure-oriented information.  Then web service is bounded with XML and then many times the web service is defined as this: 
Web services are XML-based information exchange systems that use the Internet for direct application-to-application interaction. These systems can include programs, objects, messages, or documents.

# WSDL
In WSDL a service puts a set of related ports together. 
A port defines an individual endpoint by specifying a single address for a binding. Basically, it connects a binding with a protocal address.
A binding defines message format and protocol details for operations and messages defined by a particular portType. There may be any number of bindings for a given portType.  In another words, a binding chooses a transport protocol, and for each operation defined in portType, the binding sets up the message format based on the chosen transport protocol.
A port type is a named set of abstract operations and the abstract messages involved. It is a section that gathers the operations with the related messages.
A message is an abstract, typed definition of the data being communicated.
SOAP is one of many protocol that can be used for binding.  In Web Services Description Language (WSDL) 1.1, two bindings are defined: SOAP Binding and HTTP GET & POST Binding.

# WSDL in detail with example
 a WSDL document uses the following elements in the definition of network services:

Typically, a WSDL document describes one service.
Service – a collection of related endpoints, which is used to aggregate a set of related ports.  Service coresponds to a service client class( which extends javax.xml.ws.Service) annoted as @WebServiceClient in JAX-WS afte WSDL being converted to Java when creating Java client side code.
Port – a single endpoint defined as a combination of a binding and a network address.  Port element is under service element.  Port coresponds to a service interface annoted as @WebService and @SOAPBinding in JAX-WS afte WSDL being converted to Java creating Java client side code.

```xml
<service name="HelloService">
    <port name="HelloPort" binding="tns:HelloPortBinding">
        <soap:address location="http://localhost:1212/hello"/>
    </port>
</service>
```
Binding– a concrete protocol and data format specification for a particular port type. It specifies concrete protocol and data format specifications for the operations and messages defined by a particular portType.  Biinding is at the level parallel to service element.
Under binding, there is soap:binding element which set the transport and style, eg. <soap:binding transport="http://schemas.xmlsoap.org/soap/http" style="rpc"/>.  Then all the operations are listed.  Under each of the operation, there are soap:operation, input and output elements.

```xml
<binding name="HelloPortBinding" type="tns:Hello">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" style="rpc"/>
    <operation name="sayBye">
        <soap:operation soapAction=""/>
        <input>
            <soap:body use="literal" namespace="http://web_test.tgam.com/"/>
        </input>
        <output>
            <soap:body use="literal" namespace="http://web_test.tgam.com/"/>
        </output>
    </operation>
    <operation name="sayHello">
        <soap:operation soapAction=""/>
        <input>
            <soap:body use="literal" namespace="http://web_test.tgam.com/"/>
        </input>
        <output>
            <soap:body use="literal" namespace="http://web_test.tgam.com/"/>
        </output>
    </operation>
</binding>
```
PortType– an abstract set of operations supported by one or more endpoints.  It is a set of abstract operations. Each operation refers to an input message and output messages.
Operation– an abstract description of an action supported by the service. Operation elements are under portType element.
```xml
<portType name="Hello">
    <operation name="sayBye">
        <input wsam:Action="http://web_test.tgam.com/Hello/sayByeRequest" message="tns:sayBye"/>
        <output wsam:Action="http://web_test.tgam.com/Hello/sayByeResponse" message="tns:sayByeResponse"/>
    </operation>
    <operation name="sayHello">
        <input wsam:Action="http://web_test.tgam.com/Hello/sayHelloRequest" message="tns:sayHello"/>
        <output wsam:Action="http://web_test.tgam.com/Hello/sayHelloResponse" message="tns:sayHelloResponse"/>
    </operation>
</portType>
```

Message– an abstract, typed definition of the data being communicated.
```xml
<message name="sayBye">
<part name="arg0" type="xsd:string"/>
</message>
<message name="sayByeResponse">
<part name="return" type="xsd:string"/>
</message>
<message name="sayHello">
<part name="arg0" type="xsd:string"/>
</message>
<message name="sayHelloResponse">
<part name="return" type="xsd:string"/>
</message>
```

Types– a container for data type definitions using some type system (such as XSD).


<From https://www.ibm.com/developerworks/library/ws-whichwsdl/>
# Binding styles


A WSDL document describes a web service. A WSDL binding describes how the service is bound to a messaging protocol, particularly the SOAP messaging protocol. A WSDL SOAP binding can be either a Remote Procedure Call (RPC) style binding or a document style binding. A SOAP binding can also have an encoded use or a literal use.
RPC/document style merely dictates how to translate a WSDL binding to a SOAP message and the terms encoded and literal are only meaningful for the WSDL-to-SOAP mapping.

Five binding styles:
RPC/encoded
RPC/literal
Document/encoded: Nobody follows this style. It is not WS-I compliant.
Document/literal
Document/literal wrapped pattern 
The difference among the five styles resides in the WSDL and SOAP message.


# How to choose a style
Document/literal wrapped pattern is very good, but lacks formal specification that defines this style.  In addition, If you have overloaded operations, you cannot use the document/literal wrapped style.

```
public void myMethod(int x, float y);
<strong>public void myMethod(int x);</strong>
```

WSDL allows overloaded operations. But when you add the wrapped pattern to WSDL, you require an element to have the same name as the operation, and you cannot have two elements with the same name in XML. So you must use the document/literal, non-wrapped style or one of the RPC styles.

Since the document/literal, non-wrapped style doesn't provide the operation name, there are cases where you'll need to use one of the RPC styles. 

```
public void myMethod(int x, float y);
public void myMethod(int x);
<strong>public void someOtherMethod(int x, float y);</strong>
```

When server receives the SOAP message below, 
<soap:envelope>
    <soap:body>
        <xElement>5</xElement>
        <yElement>5.0</yElement>
    </soap:body>
</soap:envelope>
All you know for sure is that it's not myMethod(int x) because the message has two parameters and this method requires one. It could be either of the other two methods. With the document/literal style, you have no way to know which one.
RPC/literal contains the method name in the SOAP, so with this message it's fairly easy for a server to decide which method to dispatch to. 


Reasons to use RPC/encoded
The primary reason to prefer the RPC/encoded style is for data graphs.  The standard way to send data graphs is to use the href tag, which is part of the RPC/encoded style.

The RPC/encoded binary tree
<A>
    <name>A</name>
    <left href="12345"/>
    <right href="12345"/>
</A>
<B id="12345">
    <name>B</name>
    <left xsi:nil="true"/>
    <right xsi:nil="true"/>
</B>


The literal binary tree
<A>
    <name>A</name>
    <left>
        <name>B</name>
        <left xsi:nil="true"/>
        <right xsi:nil="true"/>
    </left>
    <right>
        <name>B</name>
        <left xsi:nil="true"/>
        <right xsi:nil="true"/>
    </right>
</A>
There will be two Bs.


# Style detail
RPC/encoded vs. RPC/literal
WSDL is almost the same. The difference is that the use in the binding is encoded vs. literal.
The operation name appears in the message.

In SOAP message, The type encoding info is eliminated for RPC/literal.
RPC/literal is WS-I compliant, while although WSDL of RPC/encoded is legal WSDL, RPC/encoded is not WS-I compliant.

Document/literal
WSDL contains <types>.
There is no type encoding info in SOAP message.  SOAP message body can be verified, but with the operation name eliminated.  WS-I only allows one child of the soap:body in a SOAP message. In Document/literal soap:body could have more than one children.


Document/literal wrapped pattern
SOAP message looks remarkably like the RPC/literal SOAP message, but there's a subtle difference. In the RPC/literal SOAP message, the <myMethod> child of <soap:body> was the name of the operation. In the document/literal wrapped SOAP message, the <myMethod> clause is the name of the wrapper element which the single input message's part refers to. It just so happens that one of the characteristics of the wrapped pattern is that the name of the input element is the same as the name of the operation. This pattern is a sly way of putting the operation name back into the SOAP message.
These are the WSDL basic characteristics of the document/literal wrapped pattern:
The input message has a single part.
The part is an element.
The element has the same name as the operation.
The element's complex type has no attributes.

If you have overloaded operations, you cannot use the document/literal wrapped style.
WSDL allows overloaded operations. But when you add the wrapped pattern to WSDL, you require an element to have the same name as the operation, and you cannot have two elements with the same name in XML. So you must use the document/literal, non-wrapped style(with no operation name, but the number of parts can differentiate which method to depatch to) or one of the RPC styles.


In SOAP message: 
There is no type encoding info.
Everything that appears in the soap:body is defined by the schema, so you can easily validate this message.
Once again, you have the method name in the SOAP message.
Document/literal is WS-I compliant, and the wrapped pattern meets the WS-I restriction that the SOAP message's soap:body has only one child.


## RPC/encoded WSDL and SOAP message for myMethod
<message name="myMethodRequest">
    <part name="x" type="xsd:int"/>
    <part name="y" type="xsd:float"/>
</message>
<message name="empty"/>
 
<portType name="PT">
    <operation name="myMethod">
        <input message="myMethodRequest"/>
        <output message="empty"/>
    </operation>
</portType>

<binding .../>  


<soap:envelope>
    <soap:body>
        <myMethod>
            <x xsi:type="xsd:int">5</x>
            <y xsi:type="xsd:float">5.0</y>
        </myMethod>
    </soap:body>
</soap:envelope>


## RPC/literal WSDL for myMethod
<message name="myMethodRequest">
    <part name="x" type="xsd:int"/>
    <part name="y" type="xsd:float"/>
</message>
<message name="empty"/>
 
<portType name="PT">
    <operation name="myMethod">
        <input message="myMethodRequest"/>
        <output message="empty"/>
    </operation>
</portType>
 
<binding .../>  
<!-- I won't bother with the details, just assume it's RPC/<strong>literal</strong>. -->


<soap:envelope>
    <soap:body>
        <myMethod>
            <x>5</x>
            <y>5.0</y>
        </myMethod>
    </soap:body>
</soap:envelope>


## Document/literal WSDL for myMethod
<strong><types>
    <schema>
        <element name="xElement" type="xsd:int"/>
        <element name="yElement" type="xsd:float"/>
    </schema>
</types></strong>
 
<message name="myMethodRequest">
    <part name="x" <strong>element="xElement"</strong>/>
    <part name="y" <strong>element="yElement"</strong>/>
</message>
<message name="empty"/>
 
<portType name="PT">
    <operation name="myMethod">
        <input message="myMethodRequest"/>
        <output message="empty"/>
    </operation>
</portType>
 
<binding .../>  


<soap:envelope>
    <soap:body>
        <xElement>5</xElement>
        <yElement>5.0</yElement>
    </soap:body>
</soap:envelope>


## Document/literal wrapped
<types>
    <schema>
        <strong><element name="myMethod">
            <complexType>
                <sequence>
                    <element name="x" type="xsd:int"/>
                    <element name="y" type="xsd:float"/>
                </sequence>
            </complexType>
        </element>
        <element name="myMethodResponse">
            <complexType/>
        </element></strong>
    </schema>
</types>
<message name="myMethodRequest">
    <part name="<strong>parameters</strong>" element="<strong>myMethod</strong>"/>
</message>
<strong><message name="empty">
    <part name="parameters" element="myMethodResponse"/>
</message></strong>
 
<portType name="PT">
    <operation name="myMethod">
        <input message="myMethodRequest"/>
        <output message="empty"/>
    </operation>
</portType>
 
<binding .../>  


<soap:envelope>
    <soap:body>
        <myMethod>
            <x>5</x>
            <y>5.0</y>
        </myMethod>
    </soap:body>
</soap:envelope>




----------------------
There are two types of web services:

SOAP stands for Simple Object Access Protocol. SOAP is an XML based industry standard protocol for designing and developing web services. Since it’s XML based, it’s platform and language independent. So our server can be based on JAVA and client can be on .NET, PHP etc. and vice versa.

WSDL stands for Web Service Description Language. WSDL is an XML based document that provides technical details about the web service. Some of the useful information in WSDL document are: method name, port types, service end point, binding, method parameters etc.

UDDI is acronym for Universal Description, Discovery and Integration. UDDI is a directory of web services where client applications can lookup for web services. Web Services can register to the UDDI server and make them available to client applications.

JAX-WS stands for Java API for XML Web Services. JAX-WS is XML based Java API to build web services server and client application. It’s part of standard Java API, so we don’t need to include anything else which working with it.


SOAP Web Services
Restful Web Services



The endpoint is a connection point where HTML files or active server pages are exposed. Endpoints provide information needed to address a Web service endpoint. The endpoint provides a reference or specification that is used to define a group or family of message addressing properties and give end-to-end message characteristics, such as references for the source and destination of endpoints, and the identity of messages to allow for uniform addressing of "independent" messages. The endpoint can be a PC, PDA, or point-of-sale terminal.

This is de "old terminology", use directally the WSDL2 "endepoint" definition (WSDL2 translated "port" to "endpoint").

--------

Similarly, in what way SOAP over HTTP differ from XML over HTTP? After all SOAP is also XML document with SOAP namespace. So what is the difference here?
Why do we need a standard like SOAP? By exchanging XML documents over HTTP, two programs can exchange rich, structured information without the introduction of an additional standard such as SOAP to explicitly describe a message envelope format and a way to encode structured content.

SOAP provides a standard so that developers do not have to invent a custom XML message format for every service they want to make available. Given the signature of the service method to be invoked, the SOAP specification prescribes an unambiguous XML message format. Any developer familiar with the SOAP specification, working in any programming language, can formulate a correct SOAP XML request for a particular service and understand the response from the service by obtaining the following service details.

Service name
Method names implemented by the service
Method signature of each method
Address of the service implementation (expressed as a URI)
Using SOAP streamlines the process for exposing an existing software component as a Web service since the method signature of the service identifies the XML document structure used for both the request and the response.

----------

In WSDL definition, bindings contain operations, here comes style for each operation.

Document : In WSDL file, it specifies types details either having inline Or imports XSD document, which describes the structure(i.e. schema) of the complex data types being exchanged by those service methods which makes loosely coupled. Document style is default.

Advantage:
Using this Document style, we can validate SOAP messages against predefined schema. It supports xml datatypes and patterns.
loosely coupled.
Disadvantage: It is a little bit hard to get understand.
In WSDL types element looks as follows:

<types>
 <xsd:schema>
  <xsd:import schemaLocation="http://localhost:9999/ws/hello?xsd=1" namespace="http://ws.peter.com/"/>
 </xsd:schema>
</types>

The schema is importing from external reference.

RPC :In WSDL file, it does not creates types schema, within message elements it defines name and type attributes which makes tightly coupled.

<types/>  
<message name="getHelloWorldAsString">  
<part name="arg0" type="xsd:string"/>  
</message>  
<message name="getHelloWorldAsStringResponse">  
<part name="return" type="xsd:string"/>  
</message>  
Advantage: Easy to understand.
Disadvantage:
we can not validate SOAP messages.
tightly coupled
RPC : No types in WSDL
Document: Types section would be available in WSDL

-----------


To trace the soap request:
java -Dcom.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump=true  -Dcom.sun.xml.internal.ws.transport.http.HttpAdapter.dump=true wsclient/HelloClient




 left out the details of the binding tags in his examples for RPC-literal and RPC-encoded.
Unfortunately, that is where most of the differences lie.
At any rate, this is a good opportunity to get Eclipse and create some sample WSDL files, so you can see the differences for yourself.


Java JAX

wsimport -d . -p wsclient -keep http://localhost:1212/hello?wsdl
wsimport -d . -p com.tgam.thirdparty.fattail -keep http://adbook01.fattail.com/ABN/ws/adbookconnect.svc?wsdl

The -p arg tells the tool to put the generated classes into a specific package. 
Executing this command will result in generating two classes. The first class:
Service Interface: which is annoted with @WebService and many methods annoted @WebMethod, this is the interface to call the operations.
Service Client Creator Class: annoted with @WebServiceClient and extends javax.xml.ws.Service, this is the place to generate the instance of Service Interface.


Java program example:

To create a web service server:
```java
package com.tgam.web_test;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style=SOAPBinding.Style.RPC)
public class Hello {
    public String sayHello(String name) {
            return "Hello " + name;
    }
    
    public String sayBye(String name) {
            return "Bye " + name;
    }    
}
```

```java
package com.tgam.web_test;

import javax.xml.ws.Endpoint;

public class ServiceStarter {
	public static void main(String[] args) {
		String url1 = "http://localhost:1212/hello";
                String url2 = "http://localhost:1212/aloha";
		Endpoint.publish(url1, new Hello());
                Endpoint.publish(url2, new Aloha());
		System.out.println("Service started @ " + url1);
	}
}
```

java -cp web_test-1.0-SNAPSHOT.jar com.tgam.web_test.ServiceStarter
http://localhost:1212/hello?wsdl
wsimport -d . -p wsclient -keep http://localhost:1212/hello?wsdl
Hello.java and HelloService.java


```java
package wsclient;


public class HelloClient {
	public static void main(String[] args) {
		HelloService service = new HelloService();
		Hello hello = service.getHelloPort();
		String text = hello.sayHello("hany");
		System.out.println(text);
	}
}
```


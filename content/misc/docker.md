---
title: "Docker"
date: 2018-12-05T13:11:13-05:00
draft: true
---

* Kafka

docker run -td --name kafuka 9f38484d220f 
docker run -it  --name kafka -p 127.0.0.1:9092:9092/tcp --network=kafka_network 


* To create kafka container based on centos:7 images
docker network create kafka_network
docker run --name kafka -p 127.0.0.1:9092:9092/tcp --network=kafka_network -it centos:7

yum update

curl -O http://apache.forsale.plus/kafka/2.2.0/kafka_2.12-2.2.0.tgz


* Kylin
docker pull hortonworks/sandbox-hdp-standalone

docker start sandbox-hdp
docker start sandbox-proxy

docker exec -it     /bin/bash



* Node.js
docker pull node:12.6.0-stretch

docker run --name nodejs1 -p 127.0.0.1:3000:3000/tcp  -it node:12.6.0-stretch /bin/bash

cd 
mkdir graphql-server

apt update
apt install vim



* graphql

docker pull tomcat:8.5.42-jdk13-openjdk-oracle
docker run --name graphql -p 127.0.0.1:4000:4000/tcp  -it tomcat:8.5.42-jdk13-openjdk-oracle /bin/bash


docker create --name grapql -p 127.0.0.1:4000:4000/tcp  tomcat:8.5.42-jdk13-openjdk-oracle


/usr/local/tomcat/webapp


docker build -f /Users/sgao/Documents/project/adc/adc_graphql/docker/adc_graphql_dockerfile /Users/sgao/eclipse-workspace/adc_gql/target/


docker create --name adc-graphql -p 127.0.0.1:4000:4000/tcp  1951cfd98f20


docker run -it --entrypoint /bin/bash 1951cfd98f20

docker commit 8b4e57a2f9d8 adc_graphql:0.0.1

docker save -o adc_graphql.tar adc_graphql


/Users/sgao/eclipse-workspace/adc_gql/target/adc_gql-0.0.1-SNAPSHOT.war


docker build -f ./adc_graphql_dockerfile .

* java on ec2 for graphql (v1)
docker build -f ./adc_graphql_dockerfile .


adc_graphql_dockerfile:
FROM openjdk:8-stretch AS adc_graphql
WORKDIR /usr/local
COPY adc_gql-0.0.1-SNAPSHOT.jar /usr/local
EXPOSE 4000/tcp
ENTRYPOINT ["java","-jar","adc_gql-0.0.1-SNAPSHOT.jar"]
docker create --name adc-graphql -p 0.0.0.0:4000:4000/tcp  f030b7622c49


* java on ec2 for graphql (v2)
docker build -f ./adc_graphql_dockerfile .


adc_graphql_dockerfile:
FROM openjdk:8-stretch AS adc_graphql
WORKDIR /usr/local/adc_gql
EXPOSE 4000/tcp
ENTRYPOINT ["java","-jar","adc_gql.jar"]

docker create --name adc-graphql -p 0.0.0.0:4000:4000/tcp --mount type=bind,source=/home/ec2-user/adc_gql/,target=/usr/local/adc_gql 95d1d4aa42cd


docker run -it --entrypoint /bin/bash --name adc-graphql2 -p 0.0.0.0:4000:4000/tcp --mount type=bind,source=/home/ec2-user/adc_gql/,target=/usr/local/adc_gql 95d1d4aa42cd
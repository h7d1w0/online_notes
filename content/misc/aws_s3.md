---
title: "Aws_s3"
date: 2018-11-14T10:50:15-05:00
draft: true
---
AWS object storage solutions like Amazon Simple Storage Service (Amazon S3) and Amazon Glacier,

Object storage helps you break down these silos by providing massively scalable, cost-effective storage to store any type of data in its native format. 
Amazon S3 is object storage

Amazon S3 supports three different forms of encryption. 

QUERY IN PLACE
Amazon S3 allows you to run sophisticated Big Data analytics on your data without moving the data into a separate analytics system. Amazon Athena gives anyone who knows SQL on-demand query access to vast amounts of unstructured data. Amazon Redshift Spectrum lets you run queries spanning both your data warehouse and S3. And only AWS offers Amazon S3 Select, a way to retrieve only the subset of data you need from an S3 object, which can improve the performance of most applications that frequently access data from S3 by up to 400%.

The differences between File, Block, and Object storage

File and Block storage are methods to store data on NAS and SAN storage systems.

On a NAS system, it exposes its storage as a network file system. When devices are attached to a NAS (Network Attached Storage) system a mountable file system is displayed and users can access their files with proper access rights. Because of that a NAS system has to manage user privileges, file locking and other security measures so several users can access files. The access to the NAS is handled via NFS and SMB/CIFS protocols. As with any server or storage solution a file system is responsible for positioning the files in the NAS. This works very well for hundreds of thousands or even millions of files, but not for billions.

Block storage works in a similar way, but unlike file storage where the data is managed on the file level, data is stored in data blocks. Several blocks (for example in a SAN system) build a file. A block consists of an address and the SAN application gets the block, if it makes a SCSI-Request to this address. The storage application decides then were the data blocks are stored inside the system and on what specific disk or storage medium. How the blocks are combined in the end and how to access them decides the storage application. Blocks in a SAN do not have metadata that are related to the storage system or application. In other words: Blocks are data segments without description, association and without an owner to the storage solution. Everything is handled and controlled by the SAN software. Because of that SAN and Block storage is often used for performance hungry applications like data bases or for transactions because the data can be accessed, modified and saved.

Both methods for storing data worked fine for years. So why is there a need for another concept? That is because solutions for both concepts need to implement functionality for user access rights that they can make changes to the data.

What we now see is that much of the data that is being produced is “immured” or unstructured data. Content or material that will never be changed again. And this is where Object storage comes into play:

Objects in Object storage are “bundled data” (aka a file) with corresponding meta data. This object gets a unique ID (identifier), that is calculated out of the file content and the meta data. Applications identify the object via this ID. The many objects inside an object storage system are stored all over the given storage disks. In its pure form object storage can “only” save one version of a file (object). If a user makes a change another version of the same file is stored as a new object. Because of this reason an object storage is a perfect solution for a backup or archive solution. Or, for example, storage that holds vast amounts of video or movies that are only watched but not changed like for example online movie streaming sites or videos on YouTube.

The main difference between the other concepts is that the objects are managed via the application itself that supports Object storage. That means that no real file system is needed here. This layer is obsolete. An application that uses Object storage sends a storage inquiry to the solution where to store the object. The object is then given an address inside the huge storage space and saved there by the application itself.

Because of the much simple management of data – with no real file system in place – Object storage solutions can be scaled up much easier than File storage or Block storage based systems. You just add some disks in the solution and no big management is needed anymore to have more storage space. That´s a main benefit especially in times of exponential data growth.

So Object storage is a perfect solution for huge amounts of data and therefore highly used by big cloud service providers like Amazon, Google and others. But what about data protection and data recovery? The answers to these questions we provide in our second part of this article.



 These include S3 Standard for general-purpose storage of frequently accessed data,
 S3 Standard-Infrequent Access for long-lived, but less frequently accessed data
 S3 One Zone-Infrequent Access for long-lived, but less frequently accessed data
 Amazon Glacier for long-term archive

 Amazon S3 also offers configurable lifecycle policies for managing your data throughout its lifecycle. Once a policy is set, your data will automatically migrate to the most appropriate storage class without any changes to your application.



 Amazon S3 Standard
Amazon S3 Standard offers high durability, availability, and performance object storage for frequently accessed data. Because it delivers low latency and high throughput, S3 Standard is perfect for a wide variety of use cases including cloud applications, dynamic websites, content distribution, mobile and gaming applications, and Big Data analytics. S3 Lifecycle management offers configurable policies to automatically migrate objects to the most appropriate storage class.

Key Features:

Low latency and high throughput performance
Designed for durability of 99.999999999% of objects across multiple Availability Zones
Data is resilient in the event of one entire Availability Zone destruction
Designed for 99.99% availability over a given year
Backed with the Amazon S3 Service Level Agreement for availability
Supports SSL for data in transit and encryption of data at rest
Lifecycle management for automatic migration of objects

Amazon S3 Standard-Infrequent Access
S3 Standard-IA

Designed for 99.9% availability over a given year


Amazon S3 One Zone-Infrequent Access
S3 One Zone-IA

Designed for durability of 99.999999999% of objects in a single Availability Zone, but data will be lost in the event of Availability Zone destruction
Designed for 99.5% availability over a given year


Amazon Glacier


Amazon Glacier provides three options for access to archives, from a few minutes to several hours. 
Expedited retrievals typically return data in 1-5 minutes, and are great for Active Archive use cases. 
Standard retrievals typically complete between 3-5 hours work, and work well for less time-sensitive needs like backup data, media editing, or long-term analytics. 
Bulk retrievals are the lowest-cost retrieval option, returning large amounts of data within 5-12 hours.



The letter change on the URI scheme makes a big difference because it causes different software to be used to interface to S3. Somewhat like the difference between http and https - it's only a one-letter change, but it triggers a big difference in behavior.

The difference between s3 and s3n/s3a is that s3 is a block-based overlay on top of Amazon S3, while s3n/s3a are not (they are object-based).

The difference between s3n and s3a is that s3n supports objects up to 5GB in size, while s3a supports objects up to 5TB and has higher performance (both are because it uses multi-part upload). s3a is the successor to s3n.

If you're here because you want to understand which S3 file system you should use with Amazon EMR, then read this article from Amazon (only available on wayback machine). The net is: use s3:// because s3:// and s3n:// are functionally interchangeable in the context of EMR, while s3a:// is not compatible with EMR.



The EMR File System (EMRFS) is an implementation of HDFS that all Amazon EMR clusters use for reading and writing regular files from Amazon EMR directly to Amazon S3. EMRFS provides the convenience of storing persistent data in Amazon S3 for use with Hadoop while also providing features like consistent view and data encryption.


Work with Storage and File Systems
Amazon EMR and Hadoop provide a variety of file systems that you can use when processing cluster steps. You specify which file system to use by the prefix of the URI used to access the data. For example, s3://myawsbucket/path references an Amazon S3 bucket using EMRFS. The following table lists the available file systems, with recommendations about when it's best to use each one.

Amazon EMR and Hadoop typically use two or more of the following file systems when processing a cluster. HDFS and EMRFS are the two main file systems used with Amazon EMR.


HDFS	hdfs:// (or no prefix)	
HDFS is a distributed, scalable, and portable file system for Hadoop. An advantage of HDFS is data awareness between the Hadoop cluster nodes managing the clusters and the Hadoop cluster nodes managing the individual steps. For more information, see	Hadoop documentation.

HDFS is used by the master and core nodes. One advantage is that it's fast; a disadvantage is that it's ephemeral storage which is reclaimed when the cluster ends. It's best used for caching the results produced by intermediate job-flow steps.


EMRFS	s3://	
EMRFS is an implementation of the Hadoop file system used for reading and writing regular files from Amazon EMR directly to Amazon S3. EMRFS provides the convenience of storing persistent data in Amazon S3 for use with Hadoop while also providing features like Amazon S3 server-side encryption, read-after-write consistency, and list consistency.

Note

Previously, Amazon EMR used the S3 Native FileSystem with the URI scheme, s3n. While this still works, we recommend that you use the s3 URI scheme for the best performance, security, and reliability.

local file system		
The local file system refers to a locally connected disk. When a Hadoop cluster is created, each node is created from an EC2 instance that comes with a preconfigured block of preattached disk storage called an instance store. Data on instance store volumes persists only during the life of its EC2 instance. Instance store volumes are ideal for storing temporary data that is continually changing, such as buffers, caches, scratch data, and other temporary content. For more information, see Amazon EC2 Instance Storage.

(Legacy) Amazon S3 block file system	s3bfs://	
The Amazon S3 block file system is a legacy file storage system. We strongly discourage the use of this system.

Important

We recommend that you do not use this file system because it can trigger a race condition that might cause your cluster to fail. However, it might be required by legacy applications.

The s3a protocol is not supported. We suggest you use s3 in place of s3a.

S3 Native FileSystem (URI scheme: s3n) A native filesystem for reading and writing regular files on S3. The advantage of this filesystem is that you can access files on S3 that were written with other tools. Conversely, other tools can access files written using Hadoop. The disadvantage is the 5GB limit on file size imposed by S3.

S3A (URI scheme: s3a) A successor to the S3 Native, s3n fs, the S3a: system uses Amazon's libraries to interact with S3. This allows S3a to support larger files (no more 5GB limit), higher performance operations and more. The filesystem is intended to be a replacement for/successor to S3 Native: all objects accessible from s3n:// URLs should also be accessible from s3a simply by replacing the URL schema.

S3 Block FileSystem (URI scheme: s3) A block-based filesystem backed by S3. Files are stored as blocks, just like they are in HDFS. This permits efficient implementation of renames. This filesystem requires you to dedicate a bucket for the filesystem - you should not use an existing bucket containing files, or write other files to the same bucket. The files stored by this filesystem can be larger than 5GB, but they are not interoperable with other S3 tools.
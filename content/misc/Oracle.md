---
title: "Oracle"
date: 2019-02-28T13:50:23-05:00
draft: true
---

SELECT * FROM v$version

select standard_hash('foo', 'MD5') from dual; --Oracle 12g

select DBMS_CRYPTO.HASH(rawtohex('foo') ,2) from dual;

select DBMS_OBFUSCATION_TOOLKIT.md5 (input => UTL_RAW.cast_to_raw('foo')) from dual;  --deprecated